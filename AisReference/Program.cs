using AisReference.Configs;
using AisReference.Controllers;
using AisReference.Domain;
using AisReference.Interfaces;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

// Add services to the container.

builder.Services.AddEntityFrameworkNpgsql().AddDbContext<AppDbContext>(opt =>
    opt.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnectionString")));

builder.Services.AddHangfire(h => h.UsePostgreSqlStorage(builder.Configuration.GetConnectionString("DefaultConnectionString")));
builder.Services.AddHangfireServer();
builder.Services.AddControllers();
builder.Services.AddScoped<ISyncInterface, EleedReaderController>();
builder.Services.AddScoped<ISyncInterface, EkmReaderController>();
builder.Services.AddScoped<ISyncInterface, RegistryReaderController>();
builder.Services.AddScoped<ISyncInterface, SiteWriterController>();
builder.Services.AddScoped<ISyncInterface, EkmWriterController>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure<DbCon>(configuration.GetSection("ConnectionStrings"));
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseHangfireDashboard("/dashboard");

app.MapControllers();

app.Run();