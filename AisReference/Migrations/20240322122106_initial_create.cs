﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace AisReference.Migrations
{
    public partial class initial_create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "permit_form_received_blank_number",
                columns: table => new
                {
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    country_id = table.Column<long>(type: "bigint", nullable: false),
                    permit_form_received_id = table.Column<Guid>(type: "uuid", nullable: false),
                    start_number = table.Column<int>(type: "integer", nullable: false),
                    end_number = table.Column<int>(type: "integer", nullable: false),
                    quantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_permit_form_received_blank_number", x => x.guid);
                });

            migrationBuilder.CreateTable(
                name: "ref_application_status",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_application_status", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_attachment_types",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    is_individual = table.Column<bool>(type: "boolean", nullable: false),
                    is_legal = table.Column<bool>(type: "boolean", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_attachment_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_carrier_categories",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_carrier_categories", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_carrier_states",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_carrier_states", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_countries",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    code2 = table.Column<string>(type: "text", nullable: true),
                    code3 = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_countries", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_covered_ters",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    code = table.Column<string>(type: "text", nullable: true),
                    name_rus = table.Column<string>(type: "text", nullable: true),
                    name_kyr = table.Column<string>(type: "text", nullable: true),
                    in_valid = table.Column<bool>(type: "boolean", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_covered_ters", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_euro_limit",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name_rus = table.Column<string>(type: "text", nullable: true),
                    name_kyr = table.Column<string>(type: "text", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_euro_limit", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_form_request_type",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name_rus = table.Column<string>(type: "text", nullable: true),
                    name_kyr = table.Column<string>(type: "text", nullable: true),
                    name_short = table.Column<string>(type: "text", nullable: true),
                    in_valid = table.Column<bool>(type: "boolean", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_form_request_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_fuel_types",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_fuel_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_license_states",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    status = table.Column<bool>(type: "boolean", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_license_states", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_lot_status",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_lot_status", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_model_types",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    model_name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    front_api_url = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    update_with_front = table.Column<bool>(type: "boolean", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_model_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_org_structures",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name_full_ru = table.Column<string>(type: "text", nullable: true),
                    name_full_ky = table.Column<string>(type: "text", nullable: true),
                    is_individual = table.Column<bool>(type: "boolean", nullable: true),
                    min_just_code = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_org_structures", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_ownership_types",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    class_code = table.Column<string>(type: "text", nullable: true),
                    idcode = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_ownership_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_permit_status",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    is_valid = table.Column<bool>(type: "boolean", nullable: true),
                    action_name = table.Column<string>(type: "text", nullable: true),
                    color = table.Column<string>(type: "text", nullable: true),
                    send_to_tunduk = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_permit_status", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_position",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    in_valid = table.Column<bool>(type: "boolean", nullable: true),
                    weight = table.Column<int>(type: "integer", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_position", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_reg_authorities",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    address = table.Column<string>(type: "text", nullable: true),
                    code = table.Column<string>(type: "text", nullable: true),
                    contacts = table.Column<string>(type: "text", nullable: true),
                    name_full_ru = table.Column<string>(type: "text", nullable: true),
                    name_full_ky = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_reg_authorities", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_reg_types",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_reg_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_regions",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    code = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_regions", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_route_types",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_route_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_specialist_positions",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_specialist_positions", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_technical_base_categories",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    parent_category_id = table.Column<long>(type: "bigint", nullable: true),
                    is_rated = table.Column<bool>(type: "boolean", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_technical_base_categories", x => x.id);
                    table.ForeignKey(
                        name: "fk_ref_technical_base_categories_ref_technical_base_categories~",
                        column: x => x.parent_category_id,
                        principalTable: "ref_technical_base_categories",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "ref_usage_basis",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_usage_basis", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_vehicle_brands",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    code = table.Column<string>(type: "text", nullable: true),
                    is_water_transport = table.Column<bool>(type: "boolean", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_vehicle_brands", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_vehicle_types",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    descriptions = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_vehicle_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "registry_specialists",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    date_of_birth = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    driving_experience = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    inn = table.Column<string>(type: "text", nullable: true),
                    middle_name = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    surname = table.Column<string>(type: "text", nullable: true),
                    org_pattern = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registry_specialists", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user_group",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    uid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_group", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user_in_group",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    user_group_id = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_in_group", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "user_role",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_role", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    uid = table.Column<Guid>(type: "uuid", nullable: false),
                    user_name = table.Column<string>(type: "text", nullable: true),
                    password = table.Column<string>(type: "text", nullable: true),
                    first_name = table.Column<string>(type: "text", nullable: true),
                    last_name = table.Column<string>(type: "text", nullable: true),
                    middle_name = table.Column<string>(type: "text", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: false),
                    last_login_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ref_activity_types",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    abbreviation = table.Column<string>(type: "text", nullable: true),
                    code = table.Column<string>(type: "text", nullable: true),
                    covered_ter_id = table.Column<long>(type: "bigint", nullable: true),
                    name_rus = table.Column<string>(type: "text", nullable: true),
                    name_kyr = table.Column<string>(type: "text", nullable: true),
                    lic_name_rus = table.Column<string>(type: "text", nullable: true),
                    lic_name_kyr = table.Column<string>(type: "text", nullable: true),
                    type = table.Column<long>(type: "bigint", nullable: true),
                    is_water_transport = table.Column<bool>(type: "boolean", nullable: true),
                    in_valid = table.Column<bool>(type: "boolean", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_activity_types", x => x.id);
                    table.ForeignKey(
                        name: "fk_ref_activity_types_ref_covered_ters_covered_ter_id",
                        column: x => x.covered_ter_id,
                        principalTable: "ref_covered_ters",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "ref_departments",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    address = table.Column<string>(type: "text", nullable: true),
                    phone = table.Column<string>(type: "text", nullable: true),
                    email = table.Column<string>(type: "text", nullable: true),
                    inn = table.Column<string>(type: "text", nullable: true),
                    code_okpo = table.Column<string>(type: "text", nullable: true),
                    code = table.Column<string>(type: "text", nullable: true),
                    geolocation = table.Column<string>(type: "text", nullable: true),
                    region_id = table.Column<long>(type: "bigint", nullable: true),
                    schedule = table.Column<string>(type: "text", nullable: true),
                    chief_id = table.Column<long>(type: "bigint", nullable: true),
                    chief = table.Column<string>(type: "text", nullable: true),
                    head_of_department_id = table.Column<long>(type: "bigint", nullable: true),
                    head_of_department = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_departments", x => x.id);
                    table.ForeignKey(
                        name: "fk_ref_departments_ref_regions_region_id",
                        column: x => x.region_id,
                        principalTable: "ref_regions",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "ref_vehicle_kinds",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    code = table.Column<string>(type: "text", nullable: true),
                    is_trailer = table.Column<bool>(type: "boolean", nullable: true),
                    is_water_transport = table.Column<bool>(type: "boolean", nullable: true),
                    vehicle_type_id = table.Column<long>(type: "bigint", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_vehicle_kinds", x => x.id);
                    table.ForeignKey(
                        name: "fk_ref_vehicle_kinds_ref_vehicle_types_vehicle_type_id",
                        column: x => x.vehicle_type_id,
                        principalTable: "ref_vehicle_types",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "user_token",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    token = table.Column<string>(type: "text", nullable: true),
                    jwt_token = table.Column<string>(type: "text", nullable: true),
                    expires = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    replaced_by_token = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    revoked_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by_ip = table.Column<string>(type: "text", nullable: true),
                    revoked_by_ip = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_token", x => x.id);
                    table.ForeignKey(
                        name: "fk_user_token_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user_user_role",
                columns: table => new
                {
                    user_id = table.Column<long>(type: "bigint", nullable: false),
                    role_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_user_role", x => new { x.user_id, x.role_id });
                    table.ForeignKey(
                        name: "fk_user_user_role_user_role_role_id",
                        column: x => x.role_id,
                        principalTable: "user_role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_user_user_role_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ref_checkpoint",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name_rus = table.Column<string>(type: "text", nullable: true),
                    name_kyr = table.Column<string>(type: "text", nullable: true),
                    email = table.Column<string>(type: "text", nullable: true),
                    phone = table.Column<string>(type: "text", nullable: true),
                    number = table.Column<string>(type: "text", nullable: true),
                    fax = table.Column<string>(type: "text", nullable: true),
                    geolocation = table.Column<string>(type: "text", nullable: true),
                    location = table.Column<string>(type: "text", nullable: true),
                    country_id = table.Column<long>(type: "bigint", nullable: true),
                    departure_country_id = table.Column<long>(type: "bigint", nullable: true),
                    destination_country_id = table.Column<long>(type: "bigint", nullable: true),
                    department_id = table.Column<long>(type: "bigint", nullable: true),
                    external_checkpoint = table.Column<bool>(type: "boolean", nullable: true),
                    image_log_service = table.Column<string>(type: "text", nullable: true),
                    kgborder_checkpoint = table.Column<bool>(type: "boolean", nullable: true),
                    violation_checkpoint = table.Column<bool>(type: "boolean", nullable: true),
                    road_type = table.Column<long>(type: "bigint", nullable: true),
                    post_id = table.Column<long>(type: "bigint", nullable: true),
                    user_id = table.Column<long>(type: "bigint", nullable: true),
                    board_ip1 = table.Column<string>(type: "text", nullable: true),
                    board_ip2 = table.Column<string>(type: "text", nullable: true),
                    camera_ip1 = table.Column<string>(type: "text", nullable: true),
                    camera_ip2 = table.Column<string>(type: "text", nullable: true),
                    modified_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    creation_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    in_valid = table.Column<bool>(type: "boolean", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ref_checkpoint", x => x.id);
                    table.ForeignKey(
                        name: "fk_ref_checkpoint_ref_countries_country_id",
                        column: x => x.country_id,
                        principalTable: "ref_countries",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_ref_checkpoint_ref_countries_departure_country_id",
                        column: x => x.departure_country_id,
                        principalTable: "ref_countries",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_ref_checkpoint_ref_countries_destination_country_id",
                        column: x => x.destination_country_id,
                        principalTable: "ref_countries",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_ref_checkpoint_ref_departments_department_id",
                        column: x => x.department_id,
                        principalTable: "ref_departments",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "registry_carriers",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    base_address = table.Column<string>(type: "text", nullable: true),
                    brand_name = table.Column<string>(type: "text", nullable: true),
                    base_region_id = table.Column<long>(type: "bigint", nullable: true),
                    carrier_category_id = table.Column<long>(type: "bigint", nullable: true),
                    carrier_state_id = table.Column<long>(type: "bigint", nullable: true),
                    comment = table.Column<string>(type: "text", nullable: true),
                    department_id = table.Column<long>(type: "bigint", nullable: true),
                    education = table.Column<string>(type: "text", nullable: true),
                    email = table.Column<string>(type: "text", nullable: true),
                    experience = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    head_name = table.Column<string>(type: "text", nullable: true),
                    head_position = table.Column<string>(type: "text", nullable: true),
                    inn = table.Column<string>(type: "text", nullable: true),
                    legal_address = table.Column<string>(type: "text", nullable: true),
                    legal_region_id = table.Column<long>(type: "bigint", nullable: true),
                    lessor = table.Column<string>(type: "text", nullable: true),
                    login = table.Column<string>(type: "text", nullable: true),
                    middle_name = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_legal = table.Column<string>(type: "text", nullable: true),
                    org_structure_id = table.Column<long>(type: "bigint", nullable: true),
                    ownership_type_id = table.Column<long>(type: "bigint", nullable: true),
                    passport_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    passport_end = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    passport_issued_by = table.Column<string>(type: "text", nullable: true),
                    passport_num = table.Column<string>(type: "text", nullable: true),
                    password = table.Column<string>(type: "text", nullable: true),
                    password_hashed = table.Column<bool>(type: "boolean", nullable: true),
                    phones = table.Column<string>(type: "text", nullable: true),
                    real_address = table.Column<string>(type: "text", nullable: true),
                    real_region_id = table.Column<long>(type: "bigint", nullable: true),
                    reg_authorities_id = table.Column<long>(type: "bigint", nullable: true),
                    reg_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    reg_expiration_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    region_id = table.Column<long>(type: "bigint", nullable: true),
                    reg_number = table.Column<string>(type: "text", nullable: true),
                    reg_series = table.Column<string>(type: "text", nullable: true),
                    reg_type_id = table.Column<long>(type: "bigint", nullable: true),
                    regсar_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    short_name = table.Column<string>(type: "text", nullable: true),
                    subject_id = table.Column<long>(type: "bigint", nullable: true),
                    surname = table.Column<string>(type: "text", nullable: true),
                    usage_basis_id = table.Column<long>(type: "bigint", nullable: true),
                    usage_expire_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    experience_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registry_carriers", x => x.id);
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_carrier_categories_carrier_category_id",
                        column: x => x.carrier_category_id,
                        principalTable: "ref_carrier_categories",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_carrier_states_carrier_state_id",
                        column: x => x.carrier_state_id,
                        principalTable: "ref_carrier_states",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_departments_department_id",
                        column: x => x.department_id,
                        principalTable: "ref_departments",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_org_structures_org_structure_id",
                        column: x => x.org_structure_id,
                        principalTable: "ref_org_structures",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_ownership_types_ownership_type_id",
                        column: x => x.ownership_type_id,
                        principalTable: "ref_ownership_types",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_reg_authorities_reg_authorities_id",
                        column: x => x.reg_authorities_id,
                        principalTable: "ref_reg_authorities",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_reg_types_reg_type_id",
                        column: x => x.reg_type_id,
                        principalTable: "ref_reg_types",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_regions_base_region_id",
                        column: x => x.base_region_id,
                        principalTable: "ref_regions",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_regions_legal_region_id",
                        column: x => x.legal_region_id,
                        principalTable: "ref_regions",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_regions_real_region_id",
                        column: x => x.real_region_id,
                        principalTable: "ref_regions",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_regions_region_id",
                        column: x => x.region_id,
                        principalTable: "ref_regions",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carriers_ref_usage_basis_usage_basis_id",
                        column: x => x.usage_basis_id,
                        principalTable: "ref_usage_basis",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "domestic_regular_routes",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    back_trip_duration = table.Column<int>(type: "integer", nullable: false),
                    department_id = table.Column<long>(type: "bigint", nullable: false),
                    departure_id = table.Column<long>(type: "bigint", nullable: true),
                    departure_name = table.Column<string>(type: "text", nullable: true),
                    destination_id = table.Column<long>(type: "bigint", nullable: true),
                    destination_name = table.Column<string>(type: "text", nullable: true),
                    interval = table.Column<int>(type: "integer", nullable: false),
                    length = table.Column<decimal>(type: "numeric", nullable: false),
                    name_route = table.Column<string>(type: "text", nullable: false),
                    number_route = table.Column<string>(type: "text", nullable: false),
                    reg_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    route_type_id = table.Column<long>(type: "bigint", nullable: true),
                    trip_duration = table.Column<int>(type: "integer", nullable: false),
                    trips_qty = table.Column<int>(type: "integer", nullable: false),
                    vehicle_kind_id = table.Column<long>(type: "bigint", nullable: true),
                    vehicles_qty = table.Column<int>(type: "integer", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    name_ky = table.Column<string>(type: "text", nullable: true),
                    name_ru = table.Column<string>(type: "text", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    send_to_front_office = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_domestic_regular_routes", x => x.id);
                    table.ForeignKey(
                        name: "fk_domestic_regular_routes_ref_departments_department_id",
                        column: x => x.department_id,
                        principalTable: "ref_departments",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_domestic_regular_routes_ref_regions_departure_id",
                        column: x => x.departure_id,
                        principalTable: "ref_regions",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_domestic_regular_routes_ref_regions_destination_id",
                        column: x => x.destination_id,
                        principalTable: "ref_regions",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_domestic_regular_routes_ref_route_types_route_type_id",
                        column: x => x.route_type_id,
                        principalTable: "ref_route_types",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_domestic_regular_routes_ref_vehicle_kinds_vehicle_kind_id",
                        column: x => x.vehicle_kind_id,
                        principalTable: "ref_vehicle_kinds",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "registry_vehicles",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    state = table.Column<int>(type: "integer", nullable: true),
                    vehicle_brand_id = table.Column<long>(type: "bigint", nullable: true),
                    brand_spelling = table.Column<Guid>(type: "uuid", nullable: true),
                    is_trailer = table.Column<bool>(type: "boolean", nullable: true),
                    model = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: true),
                    registered_country_id = table.Column<long>(type: "bigint", nullable: true),
                    scan = table.Column<long>(type: "bigint", nullable: true),
                    state_number = table.Column<string>(type: "text", nullable: true),
                    vehicle_kind_id = table.Column<long>(type: "bigint", nullable: true),
                    vin = table.Column<string>(type: "text", nullable: true),
                    water_transport = table.Column<bool>(type: "boolean", nullable: true),
                    year_release = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registry_vehicles", x => x.id);
                    table.ForeignKey(
                        name: "fk_registry_vehicles_ref_countries_registered_country_id",
                        column: x => x.registered_country_id,
                        principalTable: "ref_countries",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_vehicles_ref_vehicle_brands_vehicle_brand_id",
                        column: x => x.vehicle_brand_id,
                        principalTable: "ref_vehicle_brands",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_vehicles_ref_vehicle_kinds_vehicle_kind_id",
                        column: x => x.vehicle_kind_id,
                        principalTable: "ref_vehicle_kinds",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "reg_employee",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    full_name = table.Column<string>(type: "text", nullable: true),
                    email = table.Column<string>(type: "text", nullable: true),
                    cell_phone = table.Column<string>(type: "text", nullable: true),
                    phone = table.Column<string>(type: "text", nullable: true),
                    physical_address = table.Column<string>(type: "text", nullable: true),
                    post_address = table.Column<string>(type: "text", nullable: true),
                    skype = table.Column<string>(type: "text", nullable: true),
                    icq = table.Column<string>(type: "text", nullable: true),
                    badge_number = table.Column<string>(type: "text", nullable: true),
                    login = table.Column<string>(type: "text", nullable: true),
                    password = table.Column<string>(type: "text", nullable: true),
                    force_pwd_change = table.Column<bool>(type: "boolean", nullable: true),
                    parent_id = table.Column<long>(type: "bigint", nullable: true),
                    role = table.Column<string>(type: "text", nullable: true),
                    user_id = table.Column<long>(type: "bigint", nullable: true),
                    user_guid = table.Column<Guid>(type: "uuid", nullable: true),
                    checkpoint_id = table.Column<long>(type: "bigint", nullable: true),
                    photo_id = table.Column<long>(type: "bigint", nullable: true),
                    department_id = table.Column<long>(type: "bigint", nullable: true),
                    position_id = table.Column<long>(type: "bigint", nullable: true),
                    in_valid = table.Column<bool>(type: "boolean", nullable: true),
                    modified_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    creation_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_reg_employee", x => x.id);
                    table.ForeignKey(
                        name: "fk_reg_employee_ref_checkpoint_checkpoint_id",
                        column: x => x.checkpoint_id,
                        principalTable: "ref_checkpoint",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_reg_employee_ref_departments_department_id",
                        column: x => x.department_id,
                        principalTable: "ref_departments",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_reg_employee_ref_position_position_id",
                        column: x => x.position_id,
                        principalTable: "ref_position",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_reg_employee_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "registry_carrier_specialists",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    parent_id = table.Column<long>(type: "bigint", nullable: true),
                    application_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    driving_experience = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    position_id = table.Column<long>(type: "bigint", nullable: true),
                    responsible_for_license = table.Column<bool>(type: "boolean", nullable: true),
                    specialist_id = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registry_carrier_specialists", x => x.id);
                    table.ForeignKey(
                        name: "fk_registry_carrier_specialists_ref_specialist_positions_posit~",
                        column: x => x.position_id,
                        principalTable: "ref_specialist_positions",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carrier_specialists_registry_carriers_parent_id",
                        column: x => x.parent_id,
                        principalTable: "registry_carriers",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carrier_specialists_registry_specialists_specialist~",
                        column: x => x.specialist_id,
                        principalTable: "registry_specialists",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "registry_licenses",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    license = table.Column<string>(type: "text", nullable: true),
                    lic_justification = table.Column<string>(type: "text", nullable: true),
                    lic_issued_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    lic_validity_end = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    lic_validity_start = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    suspend_date_end = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    num_spec = table.Column<int>(type: "integer", nullable: true),
                    old_license = table.Column<string>(type: "text", nullable: true),
                    covered_ter_id = table.Column<long>(type: "bigint", nullable: true),
                    department_id = table.Column<long>(type: "bigint", nullable: true),
                    doc_reason_id = table.Column<long>(type: "bigint", nullable: true),
                    carrier_id = table.Column<long>(type: "bigint", nullable: true),
                    parent_id = table.Column<long>(type: "bigint", nullable: true),
                    license_state_id = table.Column<long>(type: "bigint", nullable: true),
                    license_type_id = table.Column<long>(type: "bigint", nullable: true),
                    is_deleted = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registry_licenses", x => x.id);
                    table.ForeignKey(
                        name: "fk_registry_licenses_ref_activity_types_license_type_id",
                        column: x => x.license_type_id,
                        principalTable: "ref_activity_types",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_licenses_ref_covered_ters_covered_ter_id",
                        column: x => x.covered_ter_id,
                        principalTable: "ref_covered_ters",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_licenses_ref_departments_department_id",
                        column: x => x.department_id,
                        principalTable: "ref_departments",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_licenses_ref_license_states_license_state_id",
                        column: x => x.license_state_id,
                        principalTable: "ref_license_states",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_licenses_registry_carriers_carrier_id",
                        column: x => x.carrier_id,
                        principalTable: "registry_carriers",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_licenses_registry_licenses_parent_id",
                        column: x => x.parent_id,
                        principalTable: "registry_licenses",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "registry_carrier_vehicles",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    parent_id = table.Column<long>(type: "bigint", nullable: true),
                    application_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    usage_basis_id = table.Column<long>(type: "bigint", nullable: true),
                    vehicle_id = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registry_carrier_vehicles", x => x.id);
                    table.ForeignKey(
                        name: "fk_registry_carrier_vehicles_ref_usage_basis_usage_basis_id",
                        column: x => x.usage_basis_id,
                        principalTable: "ref_usage_basis",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carrier_vehicles_registry_carriers_parent_id",
                        column: x => x.parent_id,
                        principalTable: "registry_carriers",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_carrier_vehicles_registry_vehicles_vehicle_id",
                        column: x => x.vehicle_id,
                        principalTable: "registry_vehicles",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "registry_permit_forms",
                columns: table => new
                {
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    id = table.Column<long>(type: "bigint", nullable: false),
                    permit_form_received_guid = table.Column<Guid>(type: "uuid", nullable: true),
                    status_id = table.Column<int>(type: "integer", nullable: true),
                    archive_status_id = table.Column<long>(type: "bigint", nullable: true),
                    country_id = table.Column<long>(type: "bigint", nullable: true),
                    department_id = table.Column<long>(type: "bigint", nullable: true),
                    carrier_id = table.Column<long>(type: "bigint", nullable: true),
                    vehicle_id = table.Column<long>(type: "bigint", nullable: true),
                    form_type_id = table.Column<long>(type: "bigint", nullable: true),
                    restriction_id = table.Column<long>(type: "bigint", nullable: true),
                    invoice_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    expiry_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    issue_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    receipt_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    return_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    return_deadline = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    transfered_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    number = table.Column<string>(type: "text", nullable: true),
                    number_int = table.Column<int>(type: "integer", nullable: true),
                    series = table.Column<string>(type: "text", nullable: true),
                    created_by_id = table.Column<long>(type: "bigint", nullable: true),
                    updated_by_id = table.Column<long>(type: "bigint", nullable: true),
                    created_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    deleted_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registry_permit_forms", x => x.guid);
                    table.ForeignKey(
                        name: "fk_registry_permit_forms_ref_countries_country_id",
                        column: x => x.country_id,
                        principalTable: "ref_countries",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_permit_forms_ref_departments_department_id",
                        column: x => x.department_id,
                        principalTable: "ref_departments",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_permit_forms_ref_euro_limit_restriction_id",
                        column: x => x.restriction_id,
                        principalTable: "ref_euro_limit",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_permit_forms_ref_form_request_type_form_type_id",
                        column: x => x.form_type_id,
                        principalTable: "ref_form_request_type",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_permit_forms_ref_permit_status_status_id",
                        column: x => x.status_id,
                        principalTable: "ref_permit_status",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_permit_forms_registry_carriers_carrier_id",
                        column: x => x.carrier_id,
                        principalTable: "registry_carriers",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_permit_forms_registry_vehicles_vehicle_id",
                        column: x => x.vehicle_id,
                        principalTable: "registry_vehicles",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_permit_forms_users_created_by_id",
                        column: x => x.created_by_id,
                        principalTable: "users",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_permit_forms_users_updated_by_id",
                        column: x => x.updated_by_id,
                        principalTable: "users",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "registry_license_blanks",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    guid = table.Column<Guid>(type: "uuid", nullable: false),
                    doc_id = table.Column<long>(type: "bigint", nullable: true),
                    form_validity_end = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    form_validity_start = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_cancelled = table.Column<bool>(type: "boolean", nullable: true),
                    license_id = table.Column<long>(type: "bigint", nullable: true),
                    lic_form_issued_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    lic_form_lic_number = table.Column<string>(type: "text", nullable: true),
                    lic_form_number = table.Column<string>(type: "text", nullable: true),
                    main_form = table.Column<bool>(type: "boolean", nullable: true),
                    receipt = table.Column<string>(type: "text", nullable: true),
                    receipt_balance = table.Column<decimal>(type: "numeric", nullable: true),
                    sum_to_pay = table.Column<decimal>(type: "numeric", nullable: true),
                    department_id = table.Column<long>(type: "bigint", nullable: true),
                    trailer_id = table.Column<long>(type: "bigint", nullable: true),
                    vehicle_id = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registry_license_blanks", x => x.id);
                    table.ForeignKey(
                        name: "fk_registry_license_blanks_ref_departments_department_id",
                        column: x => x.department_id,
                        principalTable: "ref_departments",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_license_blanks_registry_licenses_license_id",
                        column: x => x.license_id,
                        principalTable: "registry_licenses",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_license_blanks_registry_vehicles_trailer_id",
                        column: x => x.trailer_id,
                        principalTable: "registry_vehicles",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_registry_license_blanks_registry_vehicles_vehicle_id",
                        column: x => x.vehicle_id,
                        principalTable: "registry_vehicles",
                        principalColumn: "id");
                });

            migrationBuilder.InsertData(
                table: "ref_fuel_types",
                columns: new[] { "id", "created_date", "deleted_date", "is_active", "name", "name_ky", "name_ru", "send_to_front_office", "updated_date" },
                values: new object[,]
                {
                    { 1, new DateTime(2024, 3, 22, 18, 21, 6, 606, DateTimeKind.Local).AddTicks(2788), null, true, null, "Дизель", "Дизель", true, new DateTime(2024, 3, 22, 18, 21, 6, 606, DateTimeKind.Local).AddTicks(2801) },
                    { 2, new DateTime(2024, 3, 22, 18, 21, 6, 606, DateTimeKind.Local).AddTicks(2805), null, true, null, "Газ", "Газ", true, new DateTime(2024, 3, 22, 18, 21, 6, 606, DateTimeKind.Local).AddTicks(2806) },
                    { 3, new DateTime(2024, 3, 22, 18, 21, 6, 606, DateTimeKind.Local).AddTicks(2806), null, true, null, "Электро", "Электро", true, new DateTime(2024, 3, 22, 18, 21, 6, 606, DateTimeKind.Local).AddTicks(2807) },
                    { 4, new DateTime(2024, 3, 22, 18, 21, 6, 606, DateTimeKind.Local).AddTicks(2808), null, true, null, "Бензин", "Бензин", true, new DateTime(2024, 3, 22, 18, 21, 6, 606, DateTimeKind.Local).AddTicks(2808) }
                });

            migrationBuilder.CreateIndex(
                name: "ix_domestic_regular_routes_department_id",
                table: "domestic_regular_routes",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_domestic_regular_routes_departure_id",
                table: "domestic_regular_routes",
                column: "departure_id");

            migrationBuilder.CreateIndex(
                name: "ix_domestic_regular_routes_destination_id",
                table: "domestic_regular_routes",
                column: "destination_id");

            migrationBuilder.CreateIndex(
                name: "ix_domestic_regular_routes_route_type_id",
                table: "domestic_regular_routes",
                column: "route_type_id");

            migrationBuilder.CreateIndex(
                name: "ix_domestic_regular_routes_vehicle_kind_id",
                table: "domestic_regular_routes",
                column: "vehicle_kind_id");

            migrationBuilder.CreateIndex(
                name: "ix_ref_activity_types_covered_ter_id",
                table: "ref_activity_types",
                column: "covered_ter_id");

            migrationBuilder.CreateIndex(
                name: "ix_ref_checkpoint_country_id",
                table: "ref_checkpoint",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "ix_ref_checkpoint_department_id",
                table: "ref_checkpoint",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_ref_checkpoint_departure_country_id",
                table: "ref_checkpoint",
                column: "departure_country_id");

            migrationBuilder.CreateIndex(
                name: "ix_ref_checkpoint_destination_country_id",
                table: "ref_checkpoint",
                column: "destination_country_id");

            migrationBuilder.CreateIndex(
                name: "ix_ref_departments_region_id",
                table: "ref_departments",
                column: "region_id");

            migrationBuilder.CreateIndex(
                name: "ix_ref_technical_base_categories_parent_category_id",
                table: "ref_technical_base_categories",
                column: "parent_category_id");

            migrationBuilder.CreateIndex(
                name: "ix_ref_vehicle_kinds_vehicle_type_id",
                table: "ref_vehicle_kinds",
                column: "vehicle_type_id");

            migrationBuilder.CreateIndex(
                name: "ix_reg_employee_checkpoint_id",
                table: "reg_employee",
                column: "checkpoint_id");

            migrationBuilder.CreateIndex(
                name: "ix_reg_employee_department_id",
                table: "reg_employee",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_reg_employee_position_id",
                table: "reg_employee",
                column: "position_id");

            migrationBuilder.CreateIndex(
                name: "ix_reg_employee_user_id",
                table: "reg_employee",
                column: "user_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_registry_carrier_specialists_parent_id",
                table: "registry_carrier_specialists",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carrier_specialists_position_id",
                table: "registry_carrier_specialists",
                column: "position_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carrier_specialists_specialist_id",
                table: "registry_carrier_specialists",
                column: "specialist_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carrier_vehicles_parent_id",
                table: "registry_carrier_vehicles",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carrier_vehicles_usage_basis_id",
                table: "registry_carrier_vehicles",
                column: "usage_basis_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carrier_vehicles_vehicle_id",
                table: "registry_carrier_vehicles",
                column: "vehicle_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_base_region_id",
                table: "registry_carriers",
                column: "base_region_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_carrier_category_id",
                table: "registry_carriers",
                column: "carrier_category_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_carrier_state_id",
                table: "registry_carriers",
                column: "carrier_state_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_department_id",
                table: "registry_carriers",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_legal_region_id",
                table: "registry_carriers",
                column: "legal_region_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_org_structure_id",
                table: "registry_carriers",
                column: "org_structure_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_ownership_type_id",
                table: "registry_carriers",
                column: "ownership_type_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_real_region_id",
                table: "registry_carriers",
                column: "real_region_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_reg_authorities_id",
                table: "registry_carriers",
                column: "reg_authorities_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_reg_type_id",
                table: "registry_carriers",
                column: "reg_type_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_region_id",
                table: "registry_carriers",
                column: "region_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_carriers_usage_basis_id",
                table: "registry_carriers",
                column: "usage_basis_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_license_blanks_department_id",
                table: "registry_license_blanks",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_license_blanks_license_id",
                table: "registry_license_blanks",
                column: "license_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_license_blanks_trailer_id",
                table: "registry_license_blanks",
                column: "trailer_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_license_blanks_vehicle_id",
                table: "registry_license_blanks",
                column: "vehicle_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_licenses_carrier_id",
                table: "registry_licenses",
                column: "carrier_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_licenses_covered_ter_id",
                table: "registry_licenses",
                column: "covered_ter_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_licenses_department_id",
                table: "registry_licenses",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_licenses_license_state_id",
                table: "registry_licenses",
                column: "license_state_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_licenses_license_type_id",
                table: "registry_licenses",
                column: "license_type_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_licenses_parent_id",
                table: "registry_licenses",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_permit_forms_carrier_id",
                table: "registry_permit_forms",
                column: "carrier_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_permit_forms_country_id",
                table: "registry_permit_forms",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_permit_forms_created_by_id",
                table: "registry_permit_forms",
                column: "created_by_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_permit_forms_department_id",
                table: "registry_permit_forms",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_permit_forms_form_type_id",
                table: "registry_permit_forms",
                column: "form_type_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_permit_forms_restriction_id",
                table: "registry_permit_forms",
                column: "restriction_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_permit_forms_status_id",
                table: "registry_permit_forms",
                column: "status_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_permit_forms_updated_by_id",
                table: "registry_permit_forms",
                column: "updated_by_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_permit_forms_vehicle_id",
                table: "registry_permit_forms",
                column: "vehicle_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_vehicles_registered_country_id",
                table: "registry_vehicles",
                column: "registered_country_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_vehicles_vehicle_brand_id",
                table: "registry_vehicles",
                column: "vehicle_brand_id");

            migrationBuilder.CreateIndex(
                name: "ix_registry_vehicles_vehicle_kind_id",
                table: "registry_vehicles",
                column: "vehicle_kind_id");

            migrationBuilder.CreateIndex(
                name: "ix_user_token_user_id",
                table: "user_token",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "ix_user_user_role_role_id",
                table: "user_user_role",
                column: "role_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "domestic_regular_routes");

            migrationBuilder.DropTable(
                name: "permit_form_received_blank_number");

            migrationBuilder.DropTable(
                name: "ref_application_status");

            migrationBuilder.DropTable(
                name: "ref_attachment_types");

            migrationBuilder.DropTable(
                name: "ref_fuel_types");

            migrationBuilder.DropTable(
                name: "ref_lot_status");

            migrationBuilder.DropTable(
                name: "ref_model_types");

            migrationBuilder.DropTable(
                name: "ref_technical_base_categories");

            migrationBuilder.DropTable(
                name: "reg_employee");

            migrationBuilder.DropTable(
                name: "registry_carrier_specialists");

            migrationBuilder.DropTable(
                name: "registry_carrier_vehicles");

            migrationBuilder.DropTable(
                name: "registry_license_blanks");

            migrationBuilder.DropTable(
                name: "registry_permit_forms");

            migrationBuilder.DropTable(
                name: "user_group");

            migrationBuilder.DropTable(
                name: "user_in_group");

            migrationBuilder.DropTable(
                name: "user_token");

            migrationBuilder.DropTable(
                name: "user_user_role");

            migrationBuilder.DropTable(
                name: "ref_route_types");

            migrationBuilder.DropTable(
                name: "ref_checkpoint");

            migrationBuilder.DropTable(
                name: "ref_position");

            migrationBuilder.DropTable(
                name: "ref_specialist_positions");

            migrationBuilder.DropTable(
                name: "registry_specialists");

            migrationBuilder.DropTable(
                name: "registry_licenses");

            migrationBuilder.DropTable(
                name: "ref_euro_limit");

            migrationBuilder.DropTable(
                name: "ref_form_request_type");

            migrationBuilder.DropTable(
                name: "ref_permit_status");

            migrationBuilder.DropTable(
                name: "registry_vehicles");

            migrationBuilder.DropTable(
                name: "user_role");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "ref_activity_types");

            migrationBuilder.DropTable(
                name: "ref_license_states");

            migrationBuilder.DropTable(
                name: "registry_carriers");

            migrationBuilder.DropTable(
                name: "ref_countries");

            migrationBuilder.DropTable(
                name: "ref_vehicle_brands");

            migrationBuilder.DropTable(
                name: "ref_vehicle_kinds");

            migrationBuilder.DropTable(
                name: "ref_covered_ters");

            migrationBuilder.DropTable(
                name: "ref_carrier_categories");

            migrationBuilder.DropTable(
                name: "ref_carrier_states");

            migrationBuilder.DropTable(
                name: "ref_departments");

            migrationBuilder.DropTable(
                name: "ref_org_structures");

            migrationBuilder.DropTable(
                name: "ref_ownership_types");

            migrationBuilder.DropTable(
                name: "ref_reg_authorities");

            migrationBuilder.DropTable(
                name: "ref_reg_types");

            migrationBuilder.DropTable(
                name: "ref_usage_basis");

            migrationBuilder.DropTable(
                name: "ref_vehicle_types");

            migrationBuilder.DropTable(
                name: "ref_regions");
        }
    }
}
