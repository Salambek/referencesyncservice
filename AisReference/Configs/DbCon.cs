namespace AisReference.Configs;

public class DbCon
{
    public  string DefaultConnectionString { get; set; }
    public  string EleedConnectionString { get; set; }
    public  string EkmConnectionString { get; set; }
    public  string SiteConnectionString { get; set; }
}