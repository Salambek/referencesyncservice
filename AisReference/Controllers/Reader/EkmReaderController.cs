
using AisReference.Configs;
using AisReference.Domain;
using AisReference.Interfaces;
using AisReference.Services;
using AisReference.Services.EKM;
using AisReference.Services.Eleed;
using AisReference.Services.Registry;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AisReference.Controllers;

[ApiController]
[Route("[controller]")]
public class EkmReaderController : ControllerBase, ISyncInterface
{
    private readonly RefAttachmentTypeSyncService _refAttachmentTypeSyncService;
    private readonly RefModelTypeSyncService _refModelTypeSyncService;
    private readonly RefTechnicalBaseCategorySyncServise _refTechnicalBaseCategorySyncServise;
    private readonly RefApplicationStatusSyncService _refApplicationStatusSyncService;
    private readonly RefLotStatusSyncService _refLotStatusSyncService;
    private readonly RefFuelTypeSyncService _refFuelTypeSyncService;
    private readonly RefRegTypeSyncService _refRegTypeSyncService;

    public EkmReaderController(IOptions<DbCon> dbCon, AppDbContext context)
    {
        _refAttachmentTypeSyncService = new RefAttachmentTypeSyncService(dbCon, context);
        _refModelTypeSyncService = new RefModelTypeSyncService(dbCon, context);
        _refTechnicalBaseCategorySyncServise = new RefTechnicalBaseCategorySyncServise(dbCon, context);
        _refApplicationStatusSyncService = new RefApplicationStatusSyncService(dbCon, context);
        _refLotStatusSyncService = new RefLotStatusSyncService(dbCon, context);
        _refFuelTypeSyncService = new RefFuelTypeSyncService(dbCon, context);
        _refRegTypeSyncService = new RefRegTypeSyncService(dbCon, context);
    }

    [HttpGet("startSync")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult SyncTables()
    {
        try
        {
            _refAttachmentTypeSyncService.ReadSyncTable();
            _refModelTypeSyncService.ReadSyncTable();
            _refTechnicalBaseCategorySyncServise.ReadFromEkmSyncTable();
            _refApplicationStatusSyncService.ReadFromEkmSyncTable();
            _refLotStatusSyncService.ReadFromEkmSyncTable();
            _refFuelTypeSyncService.ReadFromEkmSyncTable();
            _refRegTypeSyncService.ReadFromEkmSyncTable();

        }   
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }

        return Ok("Tables successfully synchronized.");
    }

}