using AisReference.Configs;
using AisReference.Domain;
using AisReference.Interfaces;
using AisReference.Services.EKM;
using AisReference.Services.Eleed;
using AisReference.Services.Registry;
using AisReference.Services.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AisReference.Controllers;

[ApiController]
[Route("[controller]")]
public class EleedReaderController : ControllerBase, ISyncInterface
{


    //User
    //Employee
    //Reference

    
    private readonly DomesticRegularRouteSyncService _domesticRegularRouteSyncService;
    private readonly RefRouteTypeSyncService _refRouteTypeSyncService;
    private readonly RefDepartmentSyncService _refDepartmentSyncService;
    private readonly RefSpecialistPositionsSyncService _refSpecialistPositionsSyncService;
    private readonly RefRegionSyncService _refRegionSyncService;
    private readonly RefVehicleTypeSyncService _refVehicleTypeSyncService;
    private readonly RefVehicleKindSyncService _refVehicleKindSyncService;
    private readonly RefVehicleBrandSyncService _refVehicleBrandSyncService;
    private readonly UsageBasisSyncService _usageBasisSyncService;
    private readonly RefCarrierCategorySyncService _refCarrierCategorySyncService;
    private readonly RefActivityTypeSyncService _refActivityTypeSyncService;
    private readonly RefCarrierStateSyncService _refCarrierStateSyncService;
    private readonly RefCheckpointSyncService _refCheckpointSyncService;
    private readonly RefCountrySyncService _refCountrySyncService;
    private readonly RefCoveredTerSyncService _refCoveredTerSyncService;
    private readonly RefLicenseStateSyncService _refLicenseStateSyncService;
    private readonly RefOrgStructureSyncService _refOrgStructureSyncService;
    private readonly RefOwnershipTypeSyncServices _refOwnershipTypeSyncServices;
    private readonly RefPositionSyncService _refPositionSyncQueries;
    private readonly RefRegAuthoritiesSyncService _refRegAuthoritiesSyncService;
    private readonly UserSyncService _userSyncService;
    private readonly UserGroupSyncService _userGroupSyncService;
    private readonly UserInGroupSyncService _userInGroupSyncService;
    private readonly RegEmployeeSyncService _regEmployeeSyncService;
    private readonly RefEuroLimitSyncService _refEuroLimitSyncService;
    private readonly RefFormRequestTypeSyncService _refFormRequestTypeSyncService;
    private readonly RegistryPermitFormSyncService _registryPermitFormSyncService;

    public EleedReaderController(IOptions<DbCon> dbCon, AppDbContext context)
    {
        _registryPermitFormSyncService = new RegistryPermitFormSyncService(dbCon, context);
        _domesticRegularRouteSyncService = new DomesticRegularRouteSyncService(dbCon, context);
        _refRouteTypeSyncService = new RefRouteTypeSyncService(dbCon, context);
        _refDepartmentSyncService = new RefDepartmentSyncService(dbCon, context);
        _refSpecialistPositionsSyncService = new RefSpecialistPositionsSyncService(dbCon, context);
        _refRegionSyncService = new RefRegionSyncService(dbCon, context);
        _refVehicleTypeSyncService = new RefVehicleTypeSyncService(dbCon, context);
        _refVehicleKindSyncService = new RefVehicleKindSyncService(dbCon, context);
        _refVehicleBrandSyncService = new RefVehicleBrandSyncService(dbCon, context);
        _usageBasisSyncService = new UsageBasisSyncService(dbCon, context);
        _refCarrierCategorySyncService = new RefCarrierCategorySyncService(dbCon, context);
        _refActivityTypeSyncService = new RefActivityTypeSyncService(dbCon, context);
        _refCarrierStateSyncService = new RefCarrierStateSyncService(dbCon, context);
        _refCheckpointSyncService = new RefCheckpointSyncService(dbCon, context);
        _refCountrySyncService = new RefCountrySyncService(dbCon, context);
        _refCoveredTerSyncService = new RefCoveredTerSyncService(dbCon, context);
        _refLicenseStateSyncService = new RefLicenseStateSyncService(dbCon, context);
        _refOrgStructureSyncService = new RefOrgStructureSyncService(dbCon, context);
        _refOwnershipTypeSyncServices = new RefOwnershipTypeSyncServices(dbCon, context);
        _refPositionSyncQueries = new RefPositionSyncService(dbCon, context);
        _refRegAuthoritiesSyncService = new RefRegAuthoritiesSyncService(dbCon, context);
        _userSyncService = new UserSyncService(dbCon, context);
        _userGroupSyncService = new UserGroupSyncService(dbCon, context);
        _userInGroupSyncService = new UserInGroupSyncService(dbCon, context);
        _regEmployeeSyncService = new RegEmployeeSyncService(dbCon, context);
        _refEuroLimitSyncService = new RefEuroLimitSyncService(dbCon, context);
        _refFormRequestTypeSyncService = new RefFormRequestTypeSyncService(dbCon, context);
    }

    [HttpGet("startSync")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult SyncTables()
    {
        try
        {

            _registryPermitFormSyncService.ReadSyncTable();
            //_domesticRegularRouteSyncService.ReadSyncTable();
            //_refRouteTypeSyncService.ReadSyncTable();
            //_refDepartmentSyncService.ReadSyncTable();
            //_refSpecialistPositionsSyncService.ReadSyncTable();
            //_refRegionSyncService.ReadSyncTable();
            //_refVehicleTypeSyncService.ReadSyncTable();
            //_refVehicleKindSyncService.ReadSyncTable();
            //_refVehicleBrandSyncService.ReadSyncTable();
            //_usageBasisSyncService.ReadSyncTable();
            //_refCarrierCategorySyncService.ReadSyncTable();
            //_refActivityTypeSyncService.ReadSyncTable();
            //_refCarrierStateSyncService.ReadSyncTable();
            //_refCheckpointSyncService.ReadSyncTable();
            //_refCountrySyncService.ReadSyncTable();
            //_refCoveredTerSyncService.ReadSyncTable();
            //_refLicenseStateSyncService.ReadSyncTable();
            //_refOrgStructureSyncService.ReadSyncTable();
            //_refOwnershipTypeSyncServices.ReadSyncTable();
            //_refPositionSyncQueries.ReadSyncTable();
            //_refRegAuthoritiesSyncService.ReadSyncTable();
            //_userSyncService.ReadSyncTable();
            //_userGroupSyncService.ReadSyncTable();
            //_userInGroupSyncService.ReadSyncTable();
            //_regEmployeeSyncService.ReadSyncTable();
            //_refEuroLimitSyncService.ReadSyncTable();
            //_refFormRequestTypeSyncService.ReadSyncTable();

        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }

        return Ok("Tables successfully synchronized.");
    }
}