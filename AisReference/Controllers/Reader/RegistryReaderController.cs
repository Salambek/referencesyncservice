using AisReference.Configs;
using AisReference.Domain;
using AisReference.Interfaces;
using AisReference.Services.Eleed;
using AisReference.Services.Registry;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AisReference.Controllers;

[ApiController]
[Route("[controller]")]
public class RegistryReaderController : ControllerBase, ISyncInterface
{

    //Carrier
    //Vehicle
    //License
    //LicenseBlank
    //PermitForm

    private readonly RegisterLicenseSyncService _registerLicenseSyncService;
    private readonly RegistryCarrierSyncService _registryCarrierSyncService;
    private readonly RegistryVehicleSyncService _registryVehicleSyncService;
    private readonly RegistryCarrierSpecialistSyncService _registryCarrierSpecialistSyncService;
    private readonly RegistryCarrierVehicleSyncService _registryCarrierVehicleSyncService;
    private readonly RegistrySpecialistSyncService _registrySpecialistSyncService;
    private readonly RegistryPermitFormSyncService _registryPermitFormSyncService;
    private readonly RegistryLicenseBlankSyncService _registryLicenseBlankSyncService;

    public RegistryReaderController(IOptions<DbCon> dbCon, AppDbContext context)
    {
        _registerLicenseSyncService = new RegisterLicenseSyncService(dbCon, context);
        _registryCarrierSyncService = new RegistryCarrierSyncService(dbCon, context);
        _registryVehicleSyncService = new RegistryVehicleSyncService(dbCon, context);
        _registryCarrierSpecialistSyncService = new RegistryCarrierSpecialistSyncService(dbCon, context);
        _registryCarrierVehicleSyncService = new RegistryCarrierVehicleSyncService(dbCon, context);
        _registrySpecialistSyncService = new RegistrySpecialistSyncService(dbCon, context);
        _registryPermitFormSyncService = new RegistryPermitFormSyncService(dbCon, context);
        _registryLicenseBlankSyncService = new RegistryLicenseBlankSyncService(dbCon, context);
    }

    [HttpGet("startSync")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult SyncTables()
    {
        try
        {
            //_registerLicenseSyncService.ReadSyncTable();
            //_registryCarrierSyncService.ReadSyncTable();
            //_registryVehicleSyncService.ReadSyncTable();
            //_registryCarrierSpecialistSyncService.ReadSyncTable();
            //_registryCarrierVehicleSuncService.ReadSyncTable();
            //_registrySpecialistSyncService.ReadSyncTable();
            //_registryPermitFormSyncService.ReadSyncTable();
            _registryLicenseBlankSyncService.ReadSyncTable();
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }

        return Ok("Tables successfully synchronized.");
    }
}