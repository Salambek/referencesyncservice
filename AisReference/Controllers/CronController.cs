﻿using AisReference.Interfaces;
using Hangfire;
using Microsoft.AspNetCore.Mvc;

namespace AisReference.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CronController : ControllerBase
    {
        private readonly ISyncInterface _eleedReaderController;
        private readonly ISyncInterface _ekmReaderController;
        private readonly ISyncInterface _registryReaderController;
        private readonly ISyncInterface _siteWriterController;
        private readonly ISyncInterface _ekmWriterController;

        public CronController(ISyncInterface eleedReaderController, ISyncInterface ekmReaderController, ISyncInterface registryReaderController, ISyncInterface siteWriterController, ISyncInterface ekmWriterController)
        {
            _eleedReaderController = eleedReaderController;
            _ekmReaderController = ekmReaderController;
            _registryReaderController = registryReaderController;
            _siteWriterController = siteWriterController;
            _ekmWriterController = ekmWriterController;
        }

        [HttpPost("startSyncCron")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult StartSyncCron()
        {
            try
            {
                RecurringJob.AddOrUpdate(() => SyncTables(), Cron.Daily(18, 0));
                
            }

            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        [HttpPost("SyncTables")]
        public IActionResult SyncTables()
        {
            _eleedReaderController.SyncTables();
            _ekmReaderController.SyncTables();
            _registryReaderController.SyncTables();
            _siteWriterController.SyncTables();
            _ekmWriterController.SyncTables();

            return Ok();
        }
    }
}
