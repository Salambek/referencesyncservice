
using AisReference.Configs;
using AisReference.Domain;
using AisReference.Interfaces;
using AisReference.Services;
using AisReference.Services.EKM;
using AisReference.Services.Eleed;
using AisReference.Services.Registry;
using AisReference.Services.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AisReference.Controllers;

[ApiController]
[Route("[controller]")]
public class SiteWriterController : ControllerBase, ISyncInterface
{
    private readonly RegistryCarrierSyncService _registryCarrierSyncService;
    private readonly RegistryVehicleSyncService _registryVehicleSyncService;
    private readonly RegisterLicenseSyncService _registerLicenseSyncService;
    private readonly RegistryCarrierSpecialistSyncService _registryCarrierSpecialistSyncService;
    private readonly RegistryCarrierVehicleSyncService _registryCarrierVehicleSuncService;
    private readonly RegistrySpecialistSyncService _registrySpecialistSyncService;
    private readonly RefAttachmentTypeSyncService _refAttachmentTypeSyncService;
    private readonly RegistryPermitFormSyncService _registryPermitFormSyncService;
    private readonly RegistryLicenseBlankSyncService _registryLicenseBlankSyncService;
    private readonly RefModelTypeSyncService _refModelTypeSyncService;
    private readonly DomesticRegularRouteSyncService _domesticRegularRouteSyncService;
    private readonly RefRouteTypeSyncService _refRouteTypeSyncService;
    private readonly RefDepartmentSyncService _refDepartmentSyncService;
    private readonly RefSpecialistPositionsSyncService _refSpecialistPositionsSyncService;
    private readonly RefRegionSyncService _refRegionSyncService;
    private readonly RefVehicleTypeSyncService _refVehicleTypeSyncService;
    private readonly RefVehicleKindSyncService _refVehicleKindSyncService;
    private readonly RefVehicleBrandSyncService _refVehicleBrandSyncService;
    private readonly UsageBasisSyncService _usageBasisSyncService;
    private readonly RefTechnicalBaseCategorySyncServise _refTechnicalBaseCategorySyncServise;
    private readonly RefApplicationStatusSyncService _refApplicationStatusSyncService;
    private readonly RefCarrierCategorySyncService _refCarrierCategorySyncService;
    private readonly RefLotStatusSyncService _refLotStatusSyncService;
    private readonly RefFuelTypeSyncService _refFuelTypeSyncService;
    private readonly RefRegTypeSyncService _refRegTypeSyncService;
    private readonly RefActivityTypeSyncService _refActivityTypeSyncService;
    private readonly RefCarrierStateSyncService _refCarrierStateSyncService;
    private readonly RefCountrySyncService _refCountrySyncService;
    private readonly RefCoveredTerSyncService _refCoveredTerSyncService;
    private readonly RefLicenseStateSyncService _refLicenseStateSyncService;
    private readonly RefOrgStructureSyncService _refOrgStructureSyncService;
    private readonly RefOwnershipTypeSyncServices _refOwnershipTypeSyncServices;
    private readonly RefRegAuthoritiesSyncService _refRegAuthoritiesSyncService;
    private readonly RefEuroLimitSyncService _refEuroLimitSyncService;
    private readonly RefFormRequestTypeSyncService _refFormRequestTypeSyncService;


    public SiteWriterController(IOptions<DbCon> dbCon, AppDbContext context)
    {
        _registerLicenseSyncService = new RegisterLicenseSyncService(dbCon, context);
        _registryCarrierSyncService = new RegistryCarrierSyncService(dbCon, context);
        _registryVehicleSyncService = new RegistryVehicleSyncService(dbCon, context);
        _registryPermitFormSyncService = new RegistryPermitFormSyncService(dbCon, context);
        _registryLicenseBlankSyncService = new RegistryLicenseBlankSyncService(dbCon, context);
        _registryCarrierSpecialistSyncService = new RegistryCarrierSpecialistSyncService(dbCon, context);
        _registryCarrierVehicleSuncService = new RegistryCarrierVehicleSyncService(dbCon, context);
        _registrySpecialistSyncService = new RegistrySpecialistSyncService(dbCon, context);
        _refAttachmentTypeSyncService = new RefAttachmentTypeSyncService(dbCon, context);
        _refModelTypeSyncService = new RefModelTypeSyncService(dbCon, context);
        _domesticRegularRouteSyncService = new DomesticRegularRouteSyncService(dbCon, context);
        _refRouteTypeSyncService = new RefRouteTypeSyncService(dbCon, context);
        _refDepartmentSyncService = new RefDepartmentSyncService(dbCon, context);
        _refSpecialistPositionsSyncService = new RefSpecialistPositionsSyncService(dbCon, context);
        _refRegionSyncService = new RefRegionSyncService(dbCon, context);
        _refVehicleTypeSyncService = new RefVehicleTypeSyncService(dbCon, context);
        _refVehicleKindSyncService = new RefVehicleKindSyncService(dbCon, context);
        _refVehicleBrandSyncService = new RefVehicleBrandSyncService(dbCon, context);
        _usageBasisSyncService = new UsageBasisSyncService(dbCon, context);
        _refTechnicalBaseCategorySyncServise = new RefTechnicalBaseCategorySyncServise(dbCon, context);
        _refApplicationStatusSyncService = new RefApplicationStatusSyncService(dbCon, context);
        _refCarrierCategorySyncService = new RefCarrierCategorySyncService(dbCon, context);
        _refLotStatusSyncService = new RefLotStatusSyncService(dbCon, context);
        _refFuelTypeSyncService = new RefFuelTypeSyncService(dbCon, context);
        _refRegTypeSyncService = new RefRegTypeSyncService(dbCon, context);
        _refActivityTypeSyncService = new RefActivityTypeSyncService(dbCon, context);
        _refCarrierStateSyncService = new RefCarrierStateSyncService(dbCon, context);
        _refCountrySyncService = new RefCountrySyncService(dbCon, context);
        _refCoveredTerSyncService = new RefCoveredTerSyncService(dbCon, context);
        _refLicenseStateSyncService = new RefLicenseStateSyncService(dbCon, context);
        _refOrgStructureSyncService = new RefOrgStructureSyncService(dbCon, context);
        _refOwnershipTypeSyncServices = new RefOwnershipTypeSyncServices(dbCon, context);
        _refRegAuthoritiesSyncService = new RefRegAuthoritiesSyncService(dbCon, context);
        _refEuroLimitSyncService = new RefEuroLimitSyncService(dbCon, context);
        _refFormRequestTypeSyncService = new RefFormRequestTypeSyncService(dbCon, context);

    }

    [HttpGet("startWriteSync")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult SyncTables()
    {
        try
        {
            //_registerLicenseSyncService.WriteToSiteSyncTable();
            //_registryCarrierSyncService.WriteToSiteSyncTable();
            //_registryVehicleSyncService.WriteToSiteSyncTable();
            //_registryCarrierSpecialistSyncService.WriteToSiteSyncTable();
            //_registryCarrierVehicleSuncService.WriteToSiteSyncTable();
            //_registrySpecialistSyncService.WriteToSiteSyncTable();
            //_registryPermitFormSyncService.WriteToSiteSyncTable();
            //_registryLicenseBlankSyncService.WriteToSiteSyncTable();
            _refAttachmentTypeSyncService.WriteToSiteSyncTable();
            _refModelTypeSyncService.WriteToSiteSyncTable();
            //_domesticRegularRouteSyncService.WriteToSiteSyncTable();
            //_refRouteTypeSyncService.WriteToSiteSyncTable();
            //_refDepartmentSyncService.WriteToSiteSyncTable();
            //_refSpecialistPositionsSyncService.WriteToSiteSyncTable();
            //_refRegionSyncService.WriteToSiteSyncTable();
            //_refVehicleTypeSyncService.WriteToSiteSyncTable();
            //_refVehicleKindSyncService.WriteToSiteSyncTable();
            //_refVehicleBrandSyncService.WriteToSiteSyncTable();
            //_usageBasisSyncService.WriteToSiteSyncTable();
            _refTechnicalBaseCategorySyncServise.WriteToSiteSyncTable();
            _refApplicationStatusSyncService.WriteToSiteSyncTable();
            //_refCarrierCategorySyncService.WriteToSiteSyncTable();
            _refLotStatusSyncService.WriteToSiteSyncTable();
            _refFuelTypeSyncService.WriteToSiteSyncTable();
            _refRegTypeSyncService.WriteToSiteSyncTable();
            //_refActivityTypeSyncService.WriteToSiteSyncTable();
            //_refCarrierStateSyncService.WriteToSiteSyncTable();
            //_refCountrySyncService.WriteToSiteSyncTable();
            //_refCoveredTerSyncService.WriteToSiteSyncTable();
            //_refLicenseStateSyncService.WriteToSiteSyncTable();
            //_refOrgStructureSyncService.WriteToSiteSyncTable();
            //_refOwnershipTypeSyncServices.WriteToSiteSyncTable();
            //_refRegAuthoritiesSyncService.WriteToSiteSyncTable();
            //_refEuroLimitSyncService.WriteToSiteSyncTable();
            //_refFormRequestTypeSyncService.WriteToSiteSyncTable();

        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }

        return Ok("Tables successfully synchronized.");
    }

}