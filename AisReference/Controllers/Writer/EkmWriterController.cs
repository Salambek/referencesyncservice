
using AisReference.Configs;
using AisReference.Domain;
using AisReference.Interfaces;
using AisReference.Services.EKM;
using AisReference.Services.Eleed;
using AisReference.Services.Registry;
using AisReference.Services.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AisReference.Controllers;

[ApiController]
[Route("[controller]")]
public class EkmWriterController : ControllerBase, ISyncInterface
{
    private readonly RegisterLicenseSyncService _registerLicenseSyncService;
    private readonly RegistryCarrierSyncService _registryCarrierSyncService;
    private readonly RegistryVehicleSyncService _registryVehicleSyncService;
    private readonly RegistryCarrierSpecialistSyncService _registryCarrierSpecialistSyncService;
    private readonly RegistryCarrierVehicleSyncService _registryCarrierVehicleSuncService;
    private readonly RegistrySpecialistSyncService _registrySpecialistSyncService;
    private readonly DomesticRegularRouteSyncService _domesticRegularRouteSyncService;
    private readonly RefRouteTypeSyncService _refRouteTypeSyncService;
    private readonly RefDepartmentSyncService _refDepartmentSyncService;
    private readonly RefSpecialistPositionsSyncService _refSpecialistPositionsSyncService;
    private readonly RefRegionSyncService _refRegionSyncService;
    private readonly RefVehicleTypeSyncService _refVehicleTypeSyncService;
    private readonly RefVehicleKindSyncService _refVehicleKindSyncService;
    private readonly RefVehicleBrandSyncService _refVehicleBrandSyncService;
    private readonly UsageBasisSyncService _usageBasisSyncService;
    private readonly RefApplicationStatusSyncService _refApplicationStatusSyncService;
    private readonly RefCarrierCategorySyncService _refCarrierCategorySyncService;
    private readonly RefCarrierStateSyncService _refCarrierStateSyncService;
    private readonly RefCheckpointSyncService _refCheckpointSyncService;
    private readonly RefCountrySyncService _refCountrySyncService;
    private readonly RefOrgStructureSyncService _refOrgStructureSyncService;
    private readonly RefOwnershipTypeSyncServices _refOwnershipTypeSyncServices;
    private readonly RefPositionSyncService _refPositionSyncQueries;
    private readonly RefRegAuthoritiesSyncService _refRegAuthoritiesSyncService;
    private readonly UserSyncService _userSyncService;
    private readonly UserGroupSyncService _userGroupSyncService;
    private readonly UserInGroupSyncService _userInGroupSyncService;
    private readonly RegEmployeeSyncService _regEmployeeSyncService;


    public EkmWriterController(IOptions<DbCon> dbCon, AppDbContext context)
    {
        _registerLicenseSyncService = new RegisterLicenseSyncService(dbCon, context);
        _registryCarrierSyncService = new RegistryCarrierSyncService(dbCon, context);
        _registryVehicleSyncService = new RegistryVehicleSyncService(dbCon, context);
        _registryCarrierSpecialistSyncService = new RegistryCarrierSpecialistSyncService(dbCon, context);
        _registryCarrierVehicleSuncService = new RegistryCarrierVehicleSyncService(dbCon, context);
        _registrySpecialistSyncService = new RegistrySpecialistSyncService(dbCon, context);
        _domesticRegularRouteSyncService = new DomesticRegularRouteSyncService(dbCon, context);
        _refRouteTypeSyncService = new RefRouteTypeSyncService(dbCon, context);
        _refDepartmentSyncService = new RefDepartmentSyncService(dbCon, context);
        _refSpecialistPositionsSyncService = new RefSpecialistPositionsSyncService(dbCon, context);
        _refRegionSyncService = new RefRegionSyncService(dbCon, context);
        _refVehicleTypeSyncService = new RefVehicleTypeSyncService(dbCon, context);
        _refVehicleKindSyncService = new RefVehicleKindSyncService(dbCon, context);
        _refVehicleBrandSyncService = new RefVehicleBrandSyncService(dbCon, context);
        _usageBasisSyncService = new UsageBasisSyncService(dbCon, context);
        _refApplicationStatusSyncService = new RefApplicationStatusSyncService(dbCon, context);
        _refCarrierCategorySyncService = new RefCarrierCategorySyncService(dbCon, context);
        _refCarrierStateSyncService = new RefCarrierStateSyncService(dbCon, context);
        _refCheckpointSyncService = new RefCheckpointSyncService(dbCon, context);
        _refCountrySyncService = new RefCountrySyncService(dbCon, context);
        _refOrgStructureSyncService = new RefOrgStructureSyncService(dbCon, context);
        _refOwnershipTypeSyncServices = new RefOwnershipTypeSyncServices(dbCon, context);
        _refPositionSyncQueries = new RefPositionSyncService(dbCon, context);
        _refRegAuthoritiesSyncService = new RefRegAuthoritiesSyncService(dbCon, context);
        _userSyncService = new UserSyncService(dbCon, context);
        _userGroupSyncService = new UserGroupSyncService(dbCon, context);
        _userInGroupSyncService = new UserInGroupSyncService(dbCon, context);
        _regEmployeeSyncService = new RegEmployeeSyncService(dbCon, context);

    }

    [HttpGet("startWriteSync")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public IActionResult SyncTables()
    {
        try
        {
            //_registerLicenseSyncService.WriteToSiteSyncTable();
            //_registryCarrierSyncService.WriteToEkmSyncTable();
            //_registryVehicleSyncService.WriteToEkmSyncTable();
            //_registryCarrierSpecialistSyncService.WriteToEkmSyncTable();
            //_registryCarrierVehicleSuncService.WriteToEkmSyncTable();
            //_registrySpecialistSyncService.WriteToEkmSyncTable();
            //_domesticRegularRouteSyncService.WriteToEkmSyncTable();
            //_refRouteTypeSyncService.WriteToEkmSyncTable();
            //_refDepartmentSyncService.WriteToEkmSyncTable();
            //_refSpecialistPositionsSyncService.WriteToEkmSyncTable();
            //_refRegionSyncService.WriteToEkmSyncTable();
            //_refVehicleTypeSyncService.WriteToEkmSyncTable();
            //_refVehicleKindSyncService.WriteToEkmSyncTable();
            //_refVehicleBrandSyncService.WriteToEkmSyncTable();
            //_usageBasisSyncService.WriteToEkmSyncTable();
            //_refCarrierCategorySyncService.WriteToEkmSyncTable();
            //_refCarrierStateSyncService.WriteToEkmSyncTable();
            //_refCheckpointSyncService.WriteToEkmSyncTable();
            //_refCountrySyncService.WriteToEkmSyncTable();
            //_refOrgStructureSyncService.WriteToEkmSyncTable();
            //_refOwnershipTypeSyncServices.WriteToEkmSyncTable();
            //_refPositionSyncQueries.WriteToEkmSyncTable();
            //_refRegAuthoritiesSyncService.WriteToEkmSyncTable();
            //_userSyncService.WriteToEkmSyncTable();
            //_userGroupSyncService.WriteToEkmSyncTable();
            //_userInGroupSyncService.WriteToEkmSyncTable();
            //_regEmployeeSyncService.WriteToEkmSyncTable();

        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }

        return Ok("Tables successfully synchronized.");
    }

}