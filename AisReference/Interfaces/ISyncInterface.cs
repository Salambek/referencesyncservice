﻿using Microsoft.AspNetCore.Mvc;

namespace AisReference.Interfaces
{
    public interface ISyncInterface
    {
        public IActionResult SyncTables();
    }
}
