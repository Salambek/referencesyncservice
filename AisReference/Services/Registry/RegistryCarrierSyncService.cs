﻿using AisReference.Configs;
using AisReference.Domain;
using AisReference.Domain.Registry;
using AisReference.Queries.Registry;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Registry
{
    public class RegistryCarrierSyncService : SyncService<RegistryCarrier>
    {
        public RegistryCarrierSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RegistryCarrierQueries queries = new RegistryCarrierQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CarrierEleedSelectQuery,
                queries.CarrierSelectQuery,
                queries.CarrierCreateQuery,
                queries.CarrierUpdateQuery,
                queries.CarrierDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            RegistryCarrierQueries queries = new RegistryCarrierQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CarrierSelectQuery,
                queries.CarrierSelectQuery,
                queries.CarrierCreateQuery,
                queries.CarrierUpdateQuery,
                queries.CarrierDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RegistryCarrierQueries queries = new RegistryCarrierQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CarrierDefaultToSiteSelectQuery,
                queries.CarrierSiteSelectQuery,
                queries.CarrierSiteCreateQuery,
                queries.CarrierSiteUpdateQuery,
                queries.CarrierDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}
