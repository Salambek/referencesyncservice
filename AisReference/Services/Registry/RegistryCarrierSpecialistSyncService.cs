﻿using AisReference.Configs;
using AisReference.Domain.Registry;
using AisReference.Domain;
using AisReference.Queries.Registry;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Registry
{
    public class RegistryCarrierSpecialistSyncService : SyncService<RegistryCarrierSpecialist>
    {
        public RegistryCarrierSpecialistSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RegistryCarrierSpecialistQueries queries = new RegistryCarrierSpecialistQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryCarrierSpecialistEleedSelectQuery,
                queries.RegistryCarrierSpecialistSelectQuery,
                queries.RegistryCarrierSpecialistCreateQuery,
                queries.RegistryCarrierSpecialistUpdateQuery,
                queries.RegistryCarrierSpecialistDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            RegistryCarrierSpecialistQueries queries = new RegistryCarrierSpecialistQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryCarrierSpecialistSelectQuery,
                queries.RegistryCarrierSpecialistSelectQuery,
                queries.RegistryCarrierSpecialistCreateQuery,
                queries.RegistryCarrierSpecialistUpdateQuery,
                queries.RegistryCarrierSpecialistDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RegistryCarrierSpecialistQueries queries = new RegistryCarrierSpecialistQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryCarrierSpecialistSelectQuery,
                queries.RegistryCarrierSpecialistSiteSelectQuery,
                queries.RegistryCarrierSpecialistSiteCreateQuery,
                queries.RegistryCarrierSpecialistSiteUpdateQuery,
                queries.RegistryCarrierSpecialistDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}