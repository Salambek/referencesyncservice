﻿using AisReference.Configs;
using AisReference.Domain;
using AisReference.Domain.Registry;
using AisReference.Queries.Registry;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Registry;

public class RegisterLicenseSyncService : SyncService<RegistryLicense>
{
    public RegisterLicenseSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
    {
    }

    public void ReadSyncTable()
    {
        LicenseQueries queries = new LicenseQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.RegisterLicenseEleedSelectQuery,
            queries.RegisterLicenseDefaultSelectQuery,
            queries.RegisterLicenseCreateQuery,
            queries.RegisterLicenseUpdateQuery,
            queries.RegisterLicenseDeleteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
    }

    public void WriteToEkmSyncTable()
    {
        LicenseQueries queries = new LicenseQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.RegisterLicenseDefaultSelectQuery,
            queries.RegisterLicenseEkmSelectQuery,
            queries.RegisterLicenseCreateQuery,
            queries.RegisterLicenseUpdateQuery,
            queries.RegisterLicenseDeleteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
    }

    public void WriteToSiteSyncTable()
    {
        LicenseQueries queries = new LicenseQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.RegisterLicenseDefaultSelectQuery,
            queries.RegisterLicenseDefaultToSiteSelectQuery,
            queries.RegisterLicenseCreateOnSiteQuery,
            queries.RegisterLicenseUpdateOnSiteQuery,
            queries.RegisterLicenseDeleteOnSiteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
    }
}