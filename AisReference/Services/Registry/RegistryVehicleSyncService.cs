﻿using AisReference.Configs;
using AisReference.Domain;
using AisReference.Domain.Registry;
using AisReference.Queries.Registry;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Registry
{
    public class RegistryVehicleSyncService : SyncService<RegistryVehicle>
    {
        public RegistryVehicleSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RegistryVehicleQueries queries = new RegistryVehicleQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryVehicleEleedSelectQuery,
                queries.RegistryVehicleSelectQuery,
                queries.RegistryVehicleCreateQuery,
                queries.RegistryVehicleUpdateQuery,
                queries.RegistryVehicleDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            RegistryVehicleQueries queries = new RegistryVehicleQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryVehicleSelectQuery,
                queries.RegistryVehicleSelectQuery,
                queries.RegistryVehicleCreateQuery,
                queries.RegistryVehicleUpdateQuery,
                queries.RegistryVehicleDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RegistryVehicleQueries queries = new RegistryVehicleQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryVehicleSelectQuery,
                queries.RegistryVehicleSiteSelectQuery,
                queries.RegistryVehicleSiteCreateQuery,
                queries.RegistryVehicleSiteUpdateQuery,
                queries.RegistryVehicleSiteDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}
