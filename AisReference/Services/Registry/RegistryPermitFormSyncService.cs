﻿using AisReference.Configs;
using AisReference.Domain.Registry;
using AisReference.Domain;
using AisReference.Queries.Registry;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Registry
{
    public class RegistryPermitFormSyncService : SyncService<RegistryPermitForm>
    {
        public RegistryPermitFormSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RegistryPermitFormQueries queries = new RegistryPermitFormQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryPermitFormEleedSelectQuery,
                queries.RegistryPermitFormEleedToDefaultSelectQuery,
                queries.RegistryPermitFormDefaultCreateQuery,
                queries.RegistryPermitFormUpdateQuery,
                queries.RegistryPermitFormDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToSiteSyncTable()
        {
            RegistryPermitFormQueries queries = new RegistryPermitFormQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryPermitFormDefaultSelectQuery, 
                queries.RegistryPermitFormSiteSelectQuery,
                queries.RegistryPermitFormSiteCreateQuery,
                queries.RegistryPermitFormSiteUpdateQuery,
                queries.RegistryPermitFormSiteDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}