﻿using AisReference.Configs;
using AisReference.Domain.Registry;
using AisReference.Domain;
using AisReference.Queries.Registry;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;
using MtPermitsBackend.Domain.Registry;

namespace AisReference.Services.Registry
{
    public class RegistryLicenseBlankSyncService : SyncService<RegistryLicenseBlank>
    {
        public RegistryLicenseBlankSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RegistryLicenseBlankQueries queries = new RegistryLicenseBlankQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegisterLicenseBlankEleedSelectQuery,
                queries.RegisterLicenseBlankEleedToDefaultSelectQuery,
                queries.RegisterLicenseBlankCreateQuery,
                queries.RegisterLicenseBlankUpdateQuery,
                queries.RegisterLicenseBlankDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        //public void WriteToEkmSyncTable()
        //{
        //    LicenseQueries queries = new LicenseQueries();
        //    SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
        //        queries.RegisterLicenseDefaultSelectQuery,
        //        queries.RegisterLicenseEkmSelectQuery,
        //        queries.RegisterLicenseCreateQuery,
        //        queries.RegisterLicenseUpdateQuery,
        //        queries.RegisterLicenseDeleteQuery
        //    );

        //    ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        //}

        public void WriteToSiteSyncTable()
        {
            RegistryLicenseBlankQueries queries = new RegistryLicenseBlankQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegisterLicenseBlankEleedToDefaultSelectQuery,
                queries.RegisterLicenseBlankEleedToDefaultSelectQuery,
                queries.RegisterLicenseBlankCreateQuery,
                queries.RegisterLicenseBlankUpdateQuery,
                queries.RegisterLicenseBlankDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}