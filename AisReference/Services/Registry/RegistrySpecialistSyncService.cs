﻿using AisReference.Configs;
using AisReference.Domain.Registry;
using AisReference.Domain;
using AisReference.Queries.Registry;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Registry
{
    public class RegistrySpecialistSyncService : SyncService<RegistrySpecialist>
    {
        public RegistrySpecialistSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RegistrySpecialistQueries queries = new RegistrySpecialistQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistrySpecialistEleedSelectQuery,
                queries.RegistrySpecialistSelectQuery,
                queries.RegistrySpecialistCreateQuery,
                queries.RegistrySpecialistUpdateQuery,
                queries.RegistrySpecialistDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void ReadFromEkmSyncTable()
        {
            RegistrySpecialistQueries queries = new RegistrySpecialistQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistrySpecialistSelectQuery,
                queries.RegistrySpecialistSelectQuery,
                queries.RegistrySpecialistCreateQuery,
                queries.RegistrySpecialistUpdateQuery,
                queries.RegistrySpecialistDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EkmDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            RegistrySpecialistQueries queries = new RegistrySpecialistQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistrySpecialistSelectQuery,
                queries.RegistrySpecialistSelectQuery,
                queries.RegistrySpecialistCreateQuery,
                queries.RegistrySpecialistUpdateQuery,
                queries.RegistrySpecialistDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RegistrySpecialistQueries queries = new RegistrySpecialistQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistrySpecialistSelectQuery,
                queries.RegistrySpecialistSiteSelectQuery,
                queries.RegistrySpecialistSiteCreateQuery,
                queries.RegistrySpecialistSiteUpdateQuery,
                queries.RegistrySpecialistDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}