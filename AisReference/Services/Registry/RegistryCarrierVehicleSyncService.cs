﻿using AisReference.Configs;
using AisReference.Domain.Registry;
using AisReference.Domain;
using AisReference.Queries.Registry;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Registry
{
    public class RegistryCarrierVehicleSyncService : SyncService<RegistryCarrierVehicle>
    {
        public RegistryCarrierVehicleSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RegistryCarrierVehicleQueries queries = new RegistryCarrierVehicleQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryCarrierVehicleEleedSelectQuery,
                queries.RegistryCarrierVehicleSelectQuery,
                queries.RegistryCarrierVehicleCreateQuery,
                queries.RegistryCarrierVehicleUpdateQuery,
                queries.RegistryCarrierVehicleDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void ReadFromEkmSyncTable()
        {
            RegistryCarrierVehicleQueries queries = new RegistryCarrierVehicleQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryCarrierVehicleSelectQuery,
                queries.RegistryCarrierVehicleSelectQuery,
                queries.RegistryCarrierVehicleCreateQuery,
                queries.RegistryCarrierVehicleUpdateQuery,
                queries.RegistryCarrierVehicleDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EkmDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            RegistryCarrierVehicleQueries queries = new RegistryCarrierVehicleQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryCarrierVehicleSelectQuery,
                queries.RegistryCarrierVehicleSelectQuery,
                queries.RegistryCarrierVehicleCreateQuery,
                queries.RegistryCarrierVehicleUpdateQuery,
                queries.RegistryCarrierVehicleDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RegistryCarrierVehicleQueries queries = new RegistryCarrierVehicleQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegistryCarrierVehicleSelectQuery,
                queries.RegistryCarrierVehicleSiteSelectQuery,
                queries.RegistryCarrierVehicleSiteCreateQuery,
                queries.RegistryCarrierVehicleSiteUpdateQuery,
                queries.RegistryCarrierVehicleDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}