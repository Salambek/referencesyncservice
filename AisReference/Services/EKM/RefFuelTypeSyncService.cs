﻿using AisReference.Configs;
using AisReference.Domain.Ekm;
using AisReference.Domain;
using AisReference.Queries;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;
using AisReference.Queries.Ekm;

namespace AisReference.Services.EKM
{
    public class RefFuelTypeSyncService : SyncService<RefFuelType>
    {
        public RefFuelTypeSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadFromEkmSyncTable()
        {
            RefFuelTypeQueries queries = new RefFuelTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefFuelTypeSelectQuery,
                queries.RefFuelTypeSelectQuery,
                queries.RefFuelTypeCreateQuery,
                queries.RefFuelTypeUpdateQuery,
                queries.RefFuelTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EkmDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToSiteSyncTable()
        {
            RefFuelTypeQueries queries = new RefFuelTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefFuelTypeSelectQuery,
                queries.RefFuelTypeSelectQuery,
                queries.RefFuelTypeCreateQuery,
                queries.RefFuelTypeUpdateQuery,
                queries.RefFuelTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}