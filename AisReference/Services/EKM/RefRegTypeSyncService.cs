﻿using AisReference.Configs;
using AisReference.Domain.Ekm;
using AisReference.Domain;
using AisReference.Queries.Ekm;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.EKM
{
    public class RefRegTypeSyncService : SyncService<RefRegType>
    {
        public RefRegTypeSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadFromEkmSyncTable()
        {
            RefRegTypeQueries queries = new RefRegTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefRegTypeSelectQuery,
                queries.RefRegTypeSelectQuery,
                queries.RefRegTypeCreateQuery,
                queries.RefRegTypeUpdateQuery,
                queries.RefRegTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EkmDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToSiteSyncTable()
        {
            RefRegTypeQueries queries = new RefRegTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefRegTypeSelectQuery,
                queries.RefRegTypeSelectQuery,
                queries.RefRegTypeCreateQuery,
                queries.RefRegTypeUpdateQuery,
                queries.RefRegTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}