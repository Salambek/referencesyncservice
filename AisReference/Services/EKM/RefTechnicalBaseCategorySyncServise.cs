﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;
using AisReference.Domain.Ekm;
using AisReference.Queries.Ekm;

namespace AisReference.Services.EKM
{
    public class RefTechnicalBaseCategorySyncServise : SyncService<RefTechnicalBaseCategory>
    {
        public RefTechnicalBaseCategorySyncServise(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadFromEkmSyncTable()
        {
            RefTechnicalBaseCategoryQueries queries = new RefTechnicalBaseCategoryQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefTechnicalBaseCategorySelectQuery,
                queries.RefTechnicalBaseCategorySelectQuery,
                queries.RefTechnicalBaseCategoryCreateQuery,
                queries.RefTechnicalBaseCategoryUpdateQuery,
                queries.RefTechnicalBaseCategoryDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EkmDbCon, DbConType.DefaultDbCon);
        }

            
        public void WriteToSiteSyncTable()
        {
            RefTechnicalBaseCategoryQueries queries = new RefTechnicalBaseCategoryQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefTechnicalBaseCategorySiteSelectQuery,
                queries.RefTechnicalBaseCategorySiteSelectQuery,
                queries.RefTechnicalBaseCategorySiteCreateQuery,
                queries.RefTechnicalBaseCategorySiteUpdateQuery,
                queries.RefTechnicalBaseCategoryDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}