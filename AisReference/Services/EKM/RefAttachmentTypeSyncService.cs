﻿using AisReference.Configs;
using AisReference.Domain;
using AisReference.Domain.Ekm;
using AisReference.Domain.Registry;
using AisReference.Queries;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services;

public class RefAttachmentTypeSyncService : SyncService<RefAttachmentType>
{
    public RefAttachmentTypeSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
    {
    }

    public void ReadSyncTable()
    {
        RefAttachmentTypeQueries queries = new RefAttachmentTypeQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.RefAttachmentTypeEkmSelectQuery,
            queries.RefAttachmentTypeSelectQuery,
            queries.RefAttachmentTypeCreateQuery, 
            queries.RefAttachmentTypeUpdateQuery, 
            queries.RefAttachmentTypeDeleteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EkmDbCon, DbConType.DefaultDbCon);
    }

    
    public void WriteToSiteSyncTable()
    {
        RefAttachmentTypeQueries queries = new RefAttachmentTypeQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.RefAttachmentTypeSelectQuery,
            queries.RefAttachmentTypeSelectQuery,
            queries.RefAttachmentTypeCreateQuery,
            queries.RefAttachmentTypeUpdateQuery,
            queries.RefAttachmentTypeDeleteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
    }
}