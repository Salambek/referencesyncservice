﻿using AisReference.Configs;
using AisReference.Domain.Ekm;
using AisReference.Domain;
using AisReference.Queries;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;
using AisReference.Queries.Ekm;

namespace AisReference.Services.EKM
{
    public class RefApplicationStatusSyncService : SyncService<RefApplicationStatus>
    {
        public RefApplicationStatusSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadFromEkmSyncTable()
        {
            RefApplicationStatusQueries queries = new RefApplicationStatusQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefApplicationStatusSelectQuery,
                queries.RefApplicationStatusSelectQuery,
                queries.RefApplicationStatusCreateQuery,
                queries.RefApplicationStatusUpdateQuery,
                queries.RefApplicationStatusDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EkmDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToSiteSyncTable()
        {
            RefApplicationStatusQueries queries = new RefApplicationStatusQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefApplicationStatusSelectQuery,
                queries.RefApplicationStatusSelectQuery,
                queries.RefApplicationStatusCreateQuery,
                queries.RefApplicationStatusUpdateQuery,
                queries.RefApplicationStatusDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}