﻿using AisReference.Configs;
using AisReference.Domain;
using AisReference.Domain.Ekm;
using AisReference.Queries;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services;

public class RefModelTypeSyncService : SyncService<RefModelType>
{
    public RefModelTypeSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
    {
    }

    public void ReadSyncTable()
    {
        RefModelTypeQueries queries = new RefModelTypeQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.RefModelTypeSelectQuery,
            queries.RefModelTypeSelectQuery,
            queries.RefModelTypeCreateQuery, 
            queries.RefModelTypeUpdateQuery, 
            queries.RefModelTypeDeleteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EkmDbCon, DbConType.DefaultDbCon);
    }

    
    public void WriteToSiteSyncTable()
    {
        RefModelTypeQueries queries = new RefModelTypeQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.RefModelTypeSelectQuery,
            queries.RefModelTypeSiteSelectQuery,
            queries.RefModelTypeSiteCreateQuery,
            queries.RefModelTypeSiteUpdateQuery,
            queries.RefModelTypeDeleteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
    }
}