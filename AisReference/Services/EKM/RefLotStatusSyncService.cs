﻿using AisReference.Configs;
using AisReference.Domain.Ekm;
using AisReference.Domain;
using AisReference.Queries.Ekm;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.EKM
{
    public class RefLotStatusSyncService : SyncService<RefLotStatus>
    {
        public RefLotStatusSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadFromEkmSyncTable()
        {
            RefLotStatusQueries queries = new RefLotStatusQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefLotStatusSelectQuery,
                queries.RefLotStatusSelectQuery,
                queries.RefLotStatusCreateQuery,
                queries.RefLotStatusUpdateQuery,
                queries.RefLotStatusDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EkmDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToSiteSyncTable()
        {
            RefLotStatusQueries queries = new RefLotStatusQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefLotStatusSelectQuery,
                queries.RefLotStatusSelectQuery,
                queries.RefLotStatusCreateQuery,
                queries.RefLotStatusUpdateQuery,
                queries.RefLotStatusDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}