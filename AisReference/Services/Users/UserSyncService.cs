﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Domain.Users;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;
using AisReference.Queries;

namespace AisReference.Services.Users
{
    public class UserSyncService : SyncService<User>
    {
        public UserSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            UserQueries queries = new UserQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.UserEleedSelectQuery,
                queries.UserDefaultSelectQuery,
                queries.UserCreateQuery,
                queries.UserUpdateQuery,
                queries.UserDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            UserQueries queries = new UserQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.UserDefaultSelectQuery,
                queries.UserDefaultSelectQuery,
                queries.UserCreateQuery,
                queries.UserUpdateQuery,
                queries.UserDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }
    }
}