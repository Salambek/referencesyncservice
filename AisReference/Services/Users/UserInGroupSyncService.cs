﻿using AisReference.Configs;
using AisReference.Domain.Users;
using AisReference.Domain;
using AisReference.Queries;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Users
{
    public class UserInGroupSyncService : SyncService<UserInGroup>
    {
        public UserInGroupSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            UserQueries queries = new UserQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.UserInGroupELeedSelectQuery,
                queries.UserInGroupSelectQuery,
                queries.UserInGroupCreateQuery,
                queries.UserInGroupUpdateQuery,
                queries.UserInGroupDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            UserQueries queries = new UserQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.UserInGroupSelectQuery,
                queries.UserInGroupSelectQuery,
                queries.UserInGroupCreateQuery,
                queries.UserInGroupUpdateQuery,
                queries.UserInGroupDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }
    }
}