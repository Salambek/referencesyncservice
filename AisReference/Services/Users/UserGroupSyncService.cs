﻿using AisReference.Configs;
using AisReference.Domain.Users;
using AisReference.Domain;
using AisReference.Queries;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Users
{
    public class UserGroupSyncService : SyncService<UserGroup>
    {
        public UserGroupSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            UserQueries queries = new UserQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.UserGroupEleedSelectQuery,
                queries.UserGroupSelectQuery,
                queries.UserGroupCreateQuery,
                queries.UserGroupUpdateQuery,
                queries.UserGroupDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            UserQueries queries = new UserQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.UserGroupSelectQuery,
                queries.UserGroupSelectQuery,
                queries.UserGroupCreateQuery,
                queries.UserGroupUpdateQuery,
                queries.UserGroupDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }
    }
}