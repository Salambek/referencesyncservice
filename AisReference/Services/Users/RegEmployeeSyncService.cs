﻿using AisReference.Configs;
using AisReference.Domain.Users;
using AisReference.Domain;
using AisReference.Queries;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Users
{
    public class RegEmployeeSyncService : SyncService<RegEmployee>
    {
        public RegEmployeeSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            UserQueries queries = new UserQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegEmployeeELeedSelectQuery,
                queries.RegEmployeeDefaultSelectQuery,
                queries.RegEmployeeDefaultCreateQuery,
                queries.RegEmployeeDefaultUpdateQuery,
                queries.RegEmployeeDefaultDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            UserQueries queries = new UserQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegEmployeeDefaultSelectQuery,
                queries.RegEmployeeDefaultSelectQuery,
                queries.RegEmployeeDefaultCreateQuery,
                queries.RegEmployeeDefaultUpdateQuery,
                queries.RegEmployeeDefaultDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }
    }
}