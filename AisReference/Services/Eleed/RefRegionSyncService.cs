﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefRegionSyncService : SyncService<RefRegion>
    {
        public RefRegionSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefRegionQueries queries = new RefRegionQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegionEleedSelectQuery,
                queries.RegionEleedToDefaultSelectQuery,
                queries.RegionCreateQuery,
                queries.RegionUpdateQuery,
                queries.RegionDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefRegionQueries queries = new RefRegionQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegionSelectQuery,
                queries.RegionSelectQuery,
                queries.RegionCreateQuery,
                queries.RegionUpdateQuery,
                queries.RegionDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefRegionQueries queries = new RefRegionQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegionSelectQuery,
                queries.RegionSelectQuery,
                queries.RegionCreateQuery,
                queries.RegionUpdateQuery,
                queries.RegionDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}