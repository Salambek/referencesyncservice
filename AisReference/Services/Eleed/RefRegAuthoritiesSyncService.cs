﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefRegAuthoritiesSyncService : SyncService<RefRegAuthorities>
    {
        public RefRegAuthoritiesSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefRegAuthoritiesQueries queries = new RefRegAuthoritiesQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegAuthoritiesEleedSelectQuery,
                queries.RegAuthoritiesEleedToSiteSelectQuery,
                queries.RegAuthoritiesDefaultCreateQuery,
                queries.RegAuthoritiesDefaultUpdateQuery,
                queries.RegAuthoritiesDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            RefRegAuthoritiesQueries queries = new RefRegAuthoritiesQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegAuthoritiesSelectQuery,
                queries.RegAuthoritiesSelectQuery,
                queries.RegAuthoritiesCreateQuery,
                queries.RegAuthoritiesUpdateQuery,
                queries.RegAuthoritiesDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefRegAuthoritiesQueries queries = new RefRegAuthoritiesQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RegAuthoritiesSelectQuery,
                queries.RegAuthoritiesSelectQuery,
                queries.RegAuthoritiesCreateQuery,
                queries.RegAuthoritiesUpdateQuery,
                queries.RegAuthoritiesDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}