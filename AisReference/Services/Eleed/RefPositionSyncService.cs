﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefPositionSyncService : SyncService<RefPosition>
    {
        public RefPositionSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefPositionQueries queries = new RefPositionQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.PositionEleedSelectQuery,
                queries.PositionSelectQuery,
                queries.PositionCreateQuery,
                queries.PositionUpdateQuery,
                queries.PositionDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToEkmSyncTable()
        {
            RefPositionQueries queries = new RefPositionQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.PositionSelectQuery,
                queries.PositionSelectQuery,
                queries.PositionCreateQuery,
                queries.PositionUpdateQuery,
                queries.PositionDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }
    }
}