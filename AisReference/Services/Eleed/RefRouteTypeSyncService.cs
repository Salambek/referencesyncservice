﻿using AisReference.Configs;
using AisReference.Domain;
using AisReference.Domain.Eleed;
using AisReference.Queries;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefRouteTypeSyncService : SyncService<RefRouteType>
    {
        public RefRouteTypeSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefRouteTypeQueries queries = new RefRouteTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RouteTypeEleedSelectQuery,
                queries.RouteTypeDefaultToEleedSelectQuery,
                queries.RouteTypeCreateQuery,
                queries.RouteTypeUpdateQuery,
                queries.RouteTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefRouteTypeQueries queries = new RefRouteTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RouteTypeSelectQuery,
                queries.RouteTypeSelectQuery,
                queries.RouteTypeCreateQuery,
                queries.RouteTypeUpdateQuery,
                queries.RouteTypeEkmDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefRouteTypeQueries queries = new RefRouteTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RouteTypeSelectQuery,
                queries.RouteTypeSelectQuery,
                queries.RouteTypeSiteCreateQuery,
                queries.RouteTypeUpdateQuery,
                queries.RouteTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}