﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefSpecialistPositionsSyncService : SyncService<RefSpecialistPosition>
    {
        public RefSpecialistPositionsSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefSpecialistPositionsQueries queries = new RefSpecialistPositionsQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.SpecialistPositionEleedSelectQuery,
                queries.SpecialistPositionDefaultToEleedSelectQuery,
                queries.SpecialistPositionCreateQuery,
                queries.SpecialistPositionUpdateQuery,
                queries.SpecialistPositionDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefSpecialistPositionsQueries queries = new RefSpecialistPositionsQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.SpecialistPositionSelectQuery,
                queries.SpecialistPositionSelectQuery,
                queries.SpecialistPositionCreateQuery,
                queries.SpecialistPositionUpdateQuery,
                queries.SpecialistPositionDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefSpecialistPositionsQueries queries = new RefSpecialistPositionsQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.SpecialistPositionSelectQuery,
                queries.SpecialistPositionSelectQuery,
                queries.SpecialistPositionCreateQuery,
                queries.SpecialistPositionUpdateQuery,
                queries.SpecialistPositionDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}