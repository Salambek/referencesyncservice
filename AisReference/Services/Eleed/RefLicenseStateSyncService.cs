﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefLicenseStateSyncService : SyncService<RefLicenseState>
    {
        public RefLicenseStateSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefLicenseStateQueries queries = new RefLicenseStateQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.LicenseStateEleedSelectQuery,
                queries.LicenseStateEleedToDefaultSelectQuery,
                queries.LicenseStateDefaultCreateQuery,
                queries.LicenseStateDefaultUpdateQuery,
                queries.LicenseStateDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefLicenseStateQueries queries = new RefLicenseStateQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.LicenseStateSiteSelectQuery,
                queries.LicenseStateSiteSelectQuery,
                queries.LicenseStateSiteCreateQuery,
                queries.LicenseStateSiteUpdateQuery,
                queries.LicenseStateDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}