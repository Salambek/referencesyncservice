﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefEuroLimitSyncService : SyncService<RefEuroLimit>
    {
        public RefEuroLimitSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefEuroLimitQueries queries = new RefEuroLimitQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.EuroLimitEleedSelectQuery,
                queries.EuroLimitEleedToDefaultSelectQuery,
                queries.EuroLimitDefaultCreateQuery,
                queries.EuroLimitDefaultUpdateQuery,
                queries.EuroLimitDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }



        public void WriteToSiteSyncTable()
        {
            RefEuroLimitQueries queries = new RefEuroLimitQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.EuroLimitDefaultSelectQuery,
                queries.EuroLimitSiteSelectQuery,
                queries.EuroLimitSiteCreateQuery,
                queries.EuroLimitSiteUpdateQuery,
                queries.EuroLimitSiteDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}