﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefVehicleBrandSyncService : SyncService<RefVehicleBrand>
    {
        public RefVehicleBrandSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefVehicleBrandQueries queries = new RefVehicleBrandQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.VehicleBrandEleedSelectQuery,
                queries.VehicleBrandEleedToDefaultSelectQuery,
                queries.VehicleBrandDefaultCreateQuery,
                queries.VehicleBrandDefaultpdateQuery,
                queries.VehicleBrandDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefVehicleBrandQueries queries = new RefVehicleBrandQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.VehicleBrandSelectQuery,
                queries.VehicleBrandSelectQuery,
                queries.VehicleBrandCreateQuery,
                queries.VehicleBrandUpdateQuery,
                queries.VehicleBrandDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefVehicleBrandQueries queries = new RefVehicleBrandQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.VehicleBrandSelectQuery,
                queries.VehicleBrandSelectQuery,
                queries.VehicleBrandCreateQuery,
                queries.VehicleBrandUpdateQuery,
                queries.VehicleBrandDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}