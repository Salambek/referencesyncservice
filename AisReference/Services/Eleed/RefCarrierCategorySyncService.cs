﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefCarrierCategorySyncService : SyncService<RefCarrierCategory>
    {
        public RefCarrierCategorySyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefCarrierCategoryQueries queries = new RefCarrierCategoryQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CarrierCategoryEleedSelectQuery,
                queries.CarrierCategoryEleedToDefaultSelectQuery,
                queries.CarrierCategoryCreateQuery,
                queries.CarrierCategoryUpdateQuery,
                queries.CarrierCategoryDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefCarrierCategoryQueries queries = new RefCarrierCategoryQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CarrierCategorySelectQuery,
                queries.CarrierCategorySelectQuery,
                queries.CarrierCategoryCreateQuery,
                queries.CarrierCategoryUpdateQuery,
                queries.CarrierCategoryDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefCarrierCategoryQueries queries = new RefCarrierCategoryQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CarrierCategorySiteSelectQuery,
                queries.CarrierCategorySiteSelectQuery,
                queries.CarrierCategorySiteCreateQuery,
                queries.CarrierCategorySiteUpdateQuery,
                queries.CarrierCategoryDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}