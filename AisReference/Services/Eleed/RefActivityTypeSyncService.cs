﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefActivityTypeSyncService : SyncService<RefActivityType>
    {
        public RefActivityTypeSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefActivityTypeQueries queries = new RefActivityTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.ActivityTypeEleedSelectQuery,
                queries.ActivityTypeEleedToDefaultSelectQuery,
                queries.ActivityTypeCreateQuery,
                queries.ActivityTypeUpdateQuery,
                queries.ActivityTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }



        public void WriteToSiteSyncTable()
        {
            RefActivityTypeQueries queries = new RefActivityTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.ActivityTypeDefaultSelectQuery,
                queries.ActivityTypeSiteSelectQuery,
                queries.ActivityTypeSiteCreateQuery,
                queries.ActivityTypeDefaultUpdateQuery,
                queries.ActivityTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}