﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class UsageBasisSyncService : SyncService<RefUsageBasis>
    {
        public UsageBasisSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            UsageBasisQueries queries = new UsageBasisQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.UsageBasisEleedSelectQuery,
                queries.UsageBasisEleedToDefaultSelectQuery,
                queries.UsageBasisDefaultCreateQuery,
                queries.UsageBasisDefaultUpdateQuery,
                queries.UsageBasisDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            UsageBasisQueries queries = new UsageBasisQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.UsageBasisSelectQuery,
                queries.UsageBasisSelectQuery,
                queries.UsageBasisCreateQuery,
                queries.UsageBasisUpdateQuery,
                queries.UsageBasisDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            UsageBasisQueries queries = new UsageBasisQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.UsageBasisSelectQuery,
                queries.UsageBasisSelectQuery,
                queries.UsageBasisCreateQuery,
                queries.UsageBasisUpdateQuery,
                queries.UsageBasisDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}