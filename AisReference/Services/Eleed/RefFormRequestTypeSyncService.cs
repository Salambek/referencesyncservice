﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefFormRequestTypeSyncService : SyncService<RefFormRequestType>
    {
        public RefFormRequestTypeSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefFormRequestTypeQueries queries = new RefFormRequestTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.FormRequestTypeEleedSelectQuery,
                queries.FormRequestTypeEleedToDefaultSelectQuery,
                queries.FormRequestTypeCreateQuery,
                queries.FormRequestTypeUpdateQuery,
                queries.FormRequestTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }



        public void WriteToSiteSyncTable()
        {
            RefFormRequestTypeQueries queries = new RefFormRequestTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.FormRequestTypeEleedToDefaultSelectQuery,
                queries.FormRequestTypeSiteSelectQuery,
                queries.FormRequestTypeSiteCreateQuery,
                queries.FormRequestTypeSiteUpdateQuery,
                queries.FormRequestTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}