﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefCarrierStateSyncService : SyncService<RefCarrierState>
    {
        public RefCarrierStateSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefCarrierStateQueries queries = new RefCarrierStateQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CarrierStateEleedSelectQuery,
                queries.CarrierStateEleedToDefaultSelectQuery,
                queries.CarrierStateDefaultCreateQuery,
                queries.CarrierStateDefaultUpdateQuery,
                queries.CarrierStateDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefCarrierStateQueries queries = new RefCarrierStateQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CarrierStateSelectQuery,
                queries.CarrierStateSelectQuery,
                queries.CarrierStateCreateQuery,
                queries.CarrierStateUpdateQuery,
                queries.CarrierStateDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefCarrierStateQueries queries = new RefCarrierStateQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CarrierStateSelectQuery,
                queries.CarrierStateSelectQuery,
                queries.CarrierStateCreateQuery,
                queries.CarrierStateUpdateQuery,
                queries.CarrierStateDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}