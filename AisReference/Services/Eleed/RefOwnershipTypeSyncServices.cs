﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefOwnershipTypeSyncServices : SyncService<RefOwnershipType>
    {
        public RefOwnershipTypeSyncServices(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefOwnershipTypeQueries queries = new RefOwnershipTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.OwnershipTypeEleedSelectQuery,
                queries.OwnershipTypeEleedToDefaultpSelectQuery,
                queries.OwnershipTypeDefaultCreateQuery,
                queries.OwnershipTypeDefaultUpdateQuery,
                queries.OwnershipTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefOwnershipTypeQueries queries = new RefOwnershipTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.OwnershipTypepSelectQuery,
                queries.OwnershipTypepSelectQuery,
                queries.OwnershipTypeCreateQuery,
                queries.OwnershipTypeUpdateQuery,
                queries.OwnershipTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }


        public void WriteToSiteSyncTable()
        {
            RefOwnershipTypeQueries queries = new RefOwnershipTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.OwnershipTypepSelectQuery,
                queries.OwnershipTypeSitepSelectQuery,
                queries.OwnershipTypeSiteCreateQuery,
                queries.OwnershipTypeSiteUpdateQuery,
                queries.OwnershipTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}