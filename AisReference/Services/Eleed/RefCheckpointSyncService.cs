﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefCheckpointSyncService : SyncService<RefCheckpoint>
    {
        public RefCheckpointSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefCheckpointQueries queries = new RefCheckpointQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CheckpointEleedSelectQuery,
                queries.CheckpointSelectQuery,
                queries.CheckpointCreateQuery,
                queries.CheckpointUpdateQuery,
                queries.CheckpointDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefCheckpointQueries queries = new RefCheckpointQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CheckpointSelectQuery,
                queries.CheckpointSelectQuery,
                queries.CheckpointCreateQuery,
                queries.CheckpointUpdateQuery,
                queries.CheckpointDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }
    }
}