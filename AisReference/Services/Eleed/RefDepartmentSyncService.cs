﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefDepartmentSyncService : SyncService<RefDepartment>
    {
        public RefDepartmentSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefDepartmentQueries queries = new RefDepartmentQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefDepartmentEleedSelectQuery,
                queries.RefDepartmentDefaultToEleedSelectQuery,
                queries.RefDepartmentDefaultCreateQuery,
                queries.RefDepartmentDefalutUpdateQuery,
                queries.RefDepartmentDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefDepartmentQueries queries = new RefDepartmentQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefDepartmentSelectQuery,
                queries.RefDepartmentSelectQuery,
                queries.RefDepartmentCreateQuery,
                queries.RefDepartmentUpdateQuery,
                queries.RefDepartmentEkmDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefDepartmentQueries queries = new RefDepartmentQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.RefDepartmentSiteSelectQuery,
                queries.RefDepartmentSiteSelectQuery,
                queries.RefDepartmentSiteCreateQuery,
                queries.RefDepartmentSiteUpdateQuery,
                queries.RefDepartmentDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}