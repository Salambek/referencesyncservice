﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefVehicleTypeSyncService : SyncService<RefVehicleType>
    {
        public RefVehicleTypeSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefVehicleTypeQueries queries = new RefVehicleTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.VehicleTypeEleedSelectQuery,
                queries.VehicleTypeELeedToDefaultSelectQuery,
                queries.VehicleTypeCreateQuery,
                queries.VehicleTypeUpdateQuery,
                queries.VehicleTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefVehicleTypeQueries queries = new RefVehicleTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.VehicleTypeSelectQuery,
                queries.VehicleTypeSelectQuery,
                queries.VehicleTypeCreateQuery,
                queries.VehicleTypeUpdateQuery,
                queries.VehicleTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefVehicleTypeQueries queries = new RefVehicleTypeQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.VehicleTypeSelectQuery,
                queries.VehicleTypeSiteSelectQuery,
                queries.VehicleTypeSiteCreateQuery,
                queries.VehicleTypeSiteUpdateQuery,
                queries.VehicleTypeDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}