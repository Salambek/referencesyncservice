﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefCountrySyncService : SyncService<RefCountry>
    {
        public RefCountrySyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefCountryQueries queries = new RefCountryQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CountryEleedSelectQuery,
                queries.CountryEleedToDefaultSelectQuery,
                queries.CountryDefaultCreateQuery,
                queries.CountryDefaultUpdateQuery,
                queries.CountryDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefCountryQueries queries = new RefCountryQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CountrySelectQuery,
                queries.CountrySelectQuery,
                queries.CountryCreateQuery,
                queries.CountryUpdateQuery,
                queries.CountryDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefCountryQueries queries = new RefCountryQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CountrySelectQuery,
                queries.CountrySiteSelectQuery,
                queries.CountrySiteCreateQuery,
                queries.CountrySiteUpdateQuery,
                queries.CountryDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}