﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefCoveredTerSyncService : SyncService<RefCoveredTer>
    {
        public RefCoveredTerSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefCoveredTerQueries queries = new RefCoveredTerQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CoveredTerEleedSelectQuery,
                queries.CoveredTerEleedToDefaultSelectQuery,
                queries.CoveredTerDefaultCreateQuery,
                queries.CoveredTerDefaultUpdateQuery,
                queries.CoveredTerDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefCoveredTerQueries queries = new RefCoveredTerQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.CoveredTerSelectQuery,
                queries.CoveredTerSiteSelectQuery,
                queries.CoveredTerSiteCreateQuery,
                queries.CoveredTerSiteUpdateQuery,
                queries.CoveredTerDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}