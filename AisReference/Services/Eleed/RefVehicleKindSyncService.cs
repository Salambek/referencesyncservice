﻿using AisReference.Configs;
using AisReference.Domain;
using AisReference.Domain.Eleed;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefVehicleKindSyncService : SyncService<RefVehicleKind>
    {
        public RefVehicleKindSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefVehicleKindQueries queries = new RefVehicleKindQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.VehicleKindEleedSelectQuery,
                queries.VehicleKindEleedToDefaultSelectQuery,
                queries.VehicleKindCreateQuery,
                queries.VehicleKindUpdateQuery,
                queries.VehicleKindDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefVehicleKindQueries queries = new RefVehicleKindQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.VehicleKindSelectQuery,
                queries.VehicleKindSelectQuery,
                queries.VehicleKindCreateQuery,
                queries.VehicleKindUpdateQuery,
                queries.VehicleKindDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }

        public void WriteToSiteSyncTable()
        {
            RefVehicleKindQueries queries = new RefVehicleKindQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.VehicleKindSelectQuery,
                queries.VehicleKindSelectQuery,
                queries.VehicleKindCreateQuery,
                queries.VehicleKindUpdateQuery,
                queries.VehicleKindDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}