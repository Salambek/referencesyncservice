﻿using AisReference.Configs;
using AisReference.Domain.Eleed;
using AisReference.Domain;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed
{
    public class RefOrgStructureSyncService : SyncService<RefOrgStructure>
    {
        public RefOrgStructureSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
        {
        }

        public void ReadSyncTable()
        {
            RefOrgStructureQueries queries = new RefOrgStructureQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.OrgStructureEleedSelectQuery,
                queries.OrgStructureEleedToDefaultSelectQuery,
                queries.OrgStructureDefaultCreateQuery,
                queries.OrgStructureDefaultUpdateQuery,
                queries.OrgStructureDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
        }


        public void WriteToEkmSyncTable()
        {
            RefOrgStructureQueries queries = new RefOrgStructureQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.OrgStructureSelectQuery,
                queries.OrgStructureSelectQuery,
                queries.OrgStructureDefaultCreateQuery,
                queries.OrgStructureDefaultUpdateQuery,
                queries.OrgStructureDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
        }


        public void WriteToSiteSyncTable()
        {
            RefOrgStructureQueries queries = new RefOrgStructureQueries();
            SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
                queries.OrgStructureSelectQuery,
                queries.OrgStructureSelectQuery,
                queries.OrgStructureDefaultCreateQuery,
                queries.OrgStructureDefaultUpdateQuery,
                queries.OrgStructureDeleteQuery
            );

            ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
        }
    }
}