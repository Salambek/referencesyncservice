﻿using AisReference.Configs;
using AisReference.Domain;
using AisReference.Domain.Ekm;
using AisReference.Domain.Eleed;
using AisReference.Domain.Registry;
using AisReference.Queries;
using AisReference.Queries.Eleed;
using AisReference.Services.Sync;
using Microsoft.Extensions.Options;

namespace AisReference.Services.Eleed;

public class DomesticRegularRouteSyncService : SyncService<DomesticRegularRoute>
{
    public DomesticRegularRouteSyncService(IOptions<DbCon> dbCon, AppDbContext context) : base(dbCon, context)
    {
    }

    public void ReadSyncTable()
    {
        DomesticRegularRouteQueries queries = new DomesticRegularRouteQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.DomesticRegularRouteEleedSelectQuery,
            queries.DomesticRegularRouteSelectQuery,
            queries.DomesticRegularRouteCreateQuery, 
            queries.DomesticRegularRouteUpdateQuery, 
            queries.DomesticRegularRouteDeleteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.EleedDbCon, DbConType.DefaultDbCon);
    }

    
    public void WriteToEkmSyncTable()
    {
        DomesticRegularRouteQueries queries = new DomesticRegularRouteQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.DomesticRegularRouteSelectQuery,
            queries.DomesticRegularRouteSelectQuery,
            queries.DomesticRegularRouteCreateQuery,
            queries.DomesticRegularRouteUpdateQuery,
            queries.DomesticRegularRouteDeleteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.EkmDbCon);
    }

    public void WriteToSiteSyncTable()
    {
        DomesticRegularRouteQueries queries = new DomesticRegularRouteQueries();
        SyncTableQueryDto syncTableQueryDto = new SyncTableQueryDto(
            queries.DomesticRegularRouteSelectQuery,
            queries.DomesticRegularRouteSelectQuery,
            queries.DomesticRegularRouteCreateQuery,
            queries.DomesticRegularRouteUpdateQuery,
            queries.DomesticRegularRouteDeleteQuery
        );

        ComparedTablesDataUpdate(syncTableQueryDto, DbConType.DefaultDbCon, DbConType.SiteDbCon);
    }
}