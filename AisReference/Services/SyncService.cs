﻿using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using AisReference.Configs;
using AisReference.Domain;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Npgsql;

namespace AisReference.Services.Sync;

public class SyncService<T> where T : class, new()
{
    private readonly string _connectionStringToDefaultDb;
    private readonly string _connectionStringToEleedDb;
    private readonly string _connectionStringToEkmDb;
    private readonly string _connectionStringToSiteDb;
    private readonly AppDbContext _context;


    protected SyncService(IOptions<DbCon> dbCon, AppDbContext context)
    {
        _context = context;
        _connectionStringToDefaultDb = dbCon.Value.DefaultConnectionString ?? throw new ArgumentException(nameof(dbCon));
        _connectionStringToEleedDb = dbCon.Value.EleedConnectionString ?? throw new ArgumentException(nameof(dbCon));
        _connectionStringToEkmDb = dbCon.Value.EkmConnectionString ?? throw new ArgumentException(nameof(dbCon));
        _connectionStringToSiteDb = dbCon.Value.SiteConnectionString ?? throw new ArgumentException(nameof(dbCon));
    }

    private IDbConnection ConnectionToDefaultDb
    {
        get { return new NpgsqlConnection(_connectionStringToDefaultDb); }
    }

    private IDbConnection ConnectionToEkmDb
    {
        get { return new NpgsqlConnection(_connectionStringToEkmDb); }
    }

    private IDbConnection ConnectionToSiteDb
    {
        get { return new NpgsqlConnection(_connectionStringToSiteDb); }
    }

    private SqlConnection ConnectionToEleedDb
    {
        get { return new SqlConnection(_connectionStringToEleedDb); }
    }

    private IDbConnection SourceDbConnection;
    private IDbConnection DestDbConnection;

    private void InitDbConn(SyncTableQueryDto syncTableQueryDto, DbConType DbConSource, DbConType DbConDest)     
    {
        switch (DbConSource)
        {
            case DbConType.EleedDbCon:
                SourceDbConnection = ConnectionToEleedDb;
                break;
            case DbConType.EkmDbCon:
                SourceDbConnection = ConnectionToEkmDb;
                break;
            case DbConType.SiteDbCon:
                SourceDbConnection = ConnectionToSiteDb;
                break;
            default:
                //case DbConType.DefaultDbCon:
                SourceDbConnection = ConnectionToDefaultDb;
                break;
        }

        switch (DbConDest)
        {
            case DbConType.EleedDbCon:
                DestDbConnection = ConnectionToEleedDb;
                break;
            case DbConType.EkmDbCon:
                DestDbConnection = ConnectionToEkmDb;
                break;
            case DbConType.SiteDbCon:
                DestDbConnection = ConnectionToSiteDb;
                break;
            default:
                //case DbConType.DefaultDbCon:
                DestDbConnection = ConnectionToDefaultDb;
                break;
        }

    }

    private List<SyncTablesResultDto> CompareTables(SyncTableQueryDto syncTableQueryDto, DbConType DbConSource, DbConType DbConDest)
    {
        InitDbConn(syncTableQueryDto, DbConSource, DbConDest);
        var compareResults = new List<SyncTablesResultDto>();

        if (SourceDbConnection == null && DestDbConnection == null)
            throw new Exception("Объекты подключения к ДБ не инициализированы");

        using (IDbConnection SourceDbConn = SourceDbConnection)
        {
            using (var DestDbConn = DestDbConnection)
            {
                SourceDbConn.Open();
                DestDbConn.Open();

                var modelsFromSource = SourceDbConn.Query<T>(syncTableQueryDto.SourceSelectQuery).ToList();
                var modelsFromDest = DestDbConn.Query<T>(syncTableQueryDto.DestinationSelectQuery).ToList();
                var modelsCompare = modelsFromSource.Except(modelsFromDest, (IEqualityComparer<T>?)new T()).ToList();
                var modelsCompareDelete = modelsFromDest.Except(modelsFromSource, (IEqualityComparer<T>?)new T()).ToList();


                foreach (var model in modelsCompareDelete)
                {
                    var prop = model.GetType().GetProperty("Action", BindingFlags.Instance | BindingFlags.Public);
                    var val = prop.GetValue(model, null);
                    if (val == ActionType.Create || val == ActionType.Update)
                    {
                        prop.SetValue(model, ActionType.Update);
                    }
                }

                modelsCompare.AddRange(modelsCompareDelete);
                if (modelsCompare.Count != 0) compareResults.Add(new SyncTablesResultDto(modelsCompare));

                SourceDbConn.Close();
                DestDbConn.Close();
            }
        }

        return compareResults;
    }

    protected void ComparedTablesDataUpdate(SyncTableQueryDto syncTableQueryDto, DbConType DbConSource, DbConType DbConDest)
    {
        var comparedTables = CompareTables(syncTableQueryDto, DbConSource, DbConDest);

        InitDbConn(syncTableQueryDto, DbConSource, DbConDest);
        if (SourceDbConnection == null && DestDbConnection == null)
            throw new Exception("Объекты подключения к ДБ не инициализированы");

        var entityType = _context.Model.FindEntityType(typeof(T));
        var tableName = entityType.GetTableName();


        syncTableQueryDto.EnableTriggerQuery = $"ALTER TABLE {tableName} ENABLE TRIGGER ALL;";
        syncTableQueryDto.DisableTriggerQuery = $"ALTER TABLE {tableName} DISABLE TRIGGER ALL;";

        using (var DestDbbConn = DestDbConnection)
        {
            DestDbbConn.Open();
            foreach (var table in comparedTables)
            {
                var models = ((IEnumerable)table.SyncTableData).Cast<T>().ToList();
                var modelsCreate = new List<T>();
                var modelsUpdate = new List<T>();
                var modelsDelete = new List<T>();

                foreach (var model in models)
                {
                    var prop = model.GetType().GetProperty("Action", BindingFlags.Instance | BindingFlags.Public);
                    var val = prop.GetValue(model, null);
                    if (val == ActionType.Create)
                    {
                        modelsCreate.Add(model);
                    }
                }

                foreach (var model in models)
                {
                    var prop = model.GetType().GetProperty("Action", BindingFlags.Instance | BindingFlags.Public);
                    var val = prop.GetValue(model, null);
                    if (val == ActionType.Update)
                    {
                        modelsUpdate.Add(model);
                    }
                }

                foreach (var model in models)
                {
                    PropertyInfo prop =
                        model.GetType().GetProperty("Action", BindingFlags.Instance | BindingFlags.Public);
                    object val = prop.GetValue(model, null);
                    if (val == ActionType.Delete)
                    {
                        modelsDelete.Add(model);
                    }
                }


                // Disable trigger
                //ChangeTrigger(syncTableQueryDto.DisableTriggerQuery, DestDbbConn);

                if (modelsCreate.Count != 0)
                {
                    IDbTransaction transCreate = DestDbbConn.BeginTransaction();

                    DestDbbConn.Execute(syncTableQueryDto.CreateQuery, modelsCreate, transaction: transCreate);
                    transCreate.Commit();
                }

                if (modelsUpdate.Count != 0)
                {
                    IDbTransaction transUpdate = DestDbbConn.BeginTransaction();
                    DestDbbConn.Execute(syncTableQueryDto.UpdateQuery, modelsUpdate, transaction: transUpdate);
                    transUpdate.Commit();
                }

                if (modelsDelete.Count != 0)
                {
                    IDbTransaction transDelete = DestDbbConn.BeginTransaction();
                    DestDbbConn.Execute(syncTableQueryDto.DeleteQuery, modelsDelete, transaction: transDelete);
                    transDelete.Commit();
                }

                // Enable trigger
                //ChangeTrigger(syncTableQueryDto.EnableTriggerQuery, DestDbbConn);
            }

            DestDbbConn.Close();
        }
    }

    private static void ChangeTrigger(string query, IDbConnection DbbConn)
    {
        var trigger = DbbConn.BeginTransaction();
        DbbConn.Execute(query, transaction: trigger);
        trigger.Commit();
    }

    public virtual void SyncTable()
    {
    }
}