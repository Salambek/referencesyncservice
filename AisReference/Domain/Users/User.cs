using AisReference.Domain;
using AisReference.Domain.Eleed;
using AisReference.Domain.Users;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AisReference.Domain.Users;

public class User : IEqualityComparer<User>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public virtual long Id { get; set; }
    public virtual Guid Uid { get; set; }
    public virtual string? UserName { get; set; }
    public virtual string? Password { get; set; }
    public virtual string? FirstName { get; set; }
    public virtual string? LastName { get; set; }
    public virtual string? MiddleName { get; set; }
    public virtual bool IsDeleted { get; set; }
    public virtual RegEmployee? Employee { get; set; }
    //public string? PasswordHash { get; set; }
    //public int HashType { get; set; }
    public virtual DateTime? LastLoginDate { get; set; }
    [NotMapped]
    public virtual ICollection<UserRole> Roles { get; set; } = new List<UserRole>();
    public virtual IList<UserToken> UserTokens { get; set; }
    [NotMapped]
    public virtual ICollection<UserGroup> UserGroups { get; set; }
    [NotMapped]
    public virtual string Action { get; set; }

    public User()
    {
        Roles = new List<UserRole>();
        UserTokens = new List<UserToken>();
        Action = ActionType.Create;
        IsDeleted = false;
    }

    public virtual int GetHashCode(User user)
    {
        return user.Id.GetHashCode();
    }

    public bool IsInRole(string role)
    {
        return UserName.ToLower() == role.ToLower();
    }

    public virtual bool Equals(User user1, User user2)
    {
        if (user1.Id == user2.Id && user1.UserName == user2.UserName && user1.Password == user2.Password &&
            user1.FirstName == user2.FirstName &&
            user1.MiddleName == user2.MiddleName && user1.LastName == user2.LastName && user1.Id == user2.Id)
        {
            return true;
        }


        if (user1.Id == user2.Id && (user1.UserName != user2.UserName | user1.Password != user2.Password |
                                     user1.FirstName != user2.FirstName |
                                     user1.MiddleName != user2.MiddleName | user1.LastName != user2.LastName |
                                     user1.Id != user2.Id))
        {
            user2.Action = ActionType.Update;
        }

        return false;
    }
}