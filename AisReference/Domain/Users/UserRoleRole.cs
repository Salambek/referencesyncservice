﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Users
{
    public class UserUserRole
    {
        [Display(AutoGenerateField = false)]
        public long UserId { get; set; }

        [Display(AutoGenerateField = false)]
        public int RoleId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; } = null!;

        [ForeignKey("RoleId")]
        public virtual UserRole Role { get; set; } = null!;

    }
}
