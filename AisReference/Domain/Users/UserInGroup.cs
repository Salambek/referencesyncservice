﻿using AisReference.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Users
{
    public class UserInGroup : IEqualityComparer<UserInGroup>
    {
        [Key]
        public long Id { get; set; }

        public Guid UserUid { get; set; }
        public long UserGroupId { get; set; }


        [NotMapped]
        public string Action { get; set; }


        public UserInGroup()
        {
            this.Action = ActionType.Create;
        }

        public int GetHashCode(UserInGroup userInGroup)
        {
            return userInGroup.Id.GetHashCode();
        }


        public bool Equals(UserInGroup userInGroup1, UserInGroup userInGroup2)
        {
            if (userInGroup1.Id == userInGroup2.Id && userInGroup1.UserGroupId == userInGroup2.UserGroupId && userInGroup1.UserUid == userInGroup2.UserUid)
                return true;

            if (userInGroup1.Id == userInGroup2.Id && (userInGroup1.UserGroupId != userInGroup2.UserGroupId | userInGroup1.UserUid != userInGroup2.UserUid))
                userInGroup2.Action = ActionType.Update;

            return false;
        }

    }
}
