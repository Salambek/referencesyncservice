﻿using AisReference.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Users
{
    public class UserGroup : IEqualityComparer<UserGroup>
    {
        public const long Admin = 20013; //Admins
        public const long PermitForms = 20015; //Бланки разрешений
        public const long License = 20008; //Лицензирование и Регулирование

        [Key]
        public long Id { get; set; }
        public Guid Uid { get; set; }
        public string Name { get; set; }

        [NotMapped]
        public string Action { get; set; }


        public UserGroup()
        {
            Action = ActionType.Create;
        }

        public int GetHashCode(UserGroup userGroup)
        {
            return userGroup.Id.GetHashCode();
        }
        public bool Equals(UserGroup userGroup1, UserGroup userGroup2)
        {
            if (userGroup1.Id == userGroup2.Id && userGroup1.Name == userGroup2.Name && userGroup1.Uid == userGroup2.Uid)
            {
                return true;
            }

            if (userGroup1.Id == userGroup2.Id && (userGroup1.Name != userGroup2.Name | userGroup1.Uid != userGroup2.Uid))
            {
                userGroup2.Action = ActionType.Update;
            }

            return false;
        }

    }
}
