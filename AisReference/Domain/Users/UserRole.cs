﻿using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Users
{
    public class UserRole
    {
        public const int SuperAdmin = 1; //SuperAdmin
        public const int Admin = 2; //Admin
        public const int SpecPermit = 3; //Спец разрешения
        public const int License = 4; //Лицензирование
        public const int PermitForm = 5; //Бланки разрешений

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }


        public override string ToString()
        {
            return Name;
        }

    }
}
