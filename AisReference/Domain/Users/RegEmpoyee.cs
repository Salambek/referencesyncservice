﻿using AisReference.Domain;
using AisReference.Domain.Ekm;
using AisReference.Domain.Eleed;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Users
{// A613
    public class RegEmployee : IEqualityComparer<RegEmployee>
    {
        [Key]
        public long Id { get; set; }
        public Guid Guid { get; set; }
        public string? Name { get; set; } //F60065
        public string? FullName { get; set; } //F60064
        public string? Email { get; set; } //F60071
        public string? CellPhone { get; set; } //F60070
        public string? Phone { get; set; } //F60069
        public string? PhysicalAddress { get; set; } //F60068
        public string? PostAddress { get; set; } //F60067
        public string? Skype { get; set; } //F60072
        public string? ICQ { get; set; } //F60073
        public string? BadgeNumber { get; set; } //F20205
        public string? Login { get; set; } //F60078
        public string? Password { get; set; } //F80700
        public bool? ForcePwdChange { get; set; }
        public long? ParentId { get; set; }
        public string? Role { get; set; } //ссылка

        [ForeignKey("UserId")]
        public long? UserId { get; set; } //bigint
        [ForeignKey("UserGuid")]
        public Guid? UserGuid { get; set; } //ссылка
        
        public virtual User? User { get; set; }


        public long? CheckpointId { get; set; } //F20204 ссылка
        [ForeignKey("CheckpointId")]
        public virtual RefCheckpoint? Checkpoint { get; set; }


        [ForeignKey("PhotoId")]
        public long? PhotoId { get; set; }


        public long? DepartmentId { get; set; } //F20202
        [ForeignKey("DepartmentId")]
        public virtual RefDepartment? Department { get; set; }


        public long? PositionId { get; set; } //F20203
        [ForeignKey("PositionId")]
        public virtual RefPosition? Position { get; set; }


        public bool? InValid { get; set; } //F81030

        public DateTime? ModifiedDate { get; set; } //F80702
        public DateTime? CreationDate { get; set; } //F80701

        public virtual bool? IsDeleted { get; set; }


        [NotMapped]
        public virtual ICollection<UserRole> Roles { get; set; } = new List<UserRole>();


        [NotMapped]
        public DateTime? LastLoginDate
        {
            get
            {
                if (User != null)
                {
                    return User.LastLoginDate;
                }
                return null;
            }
        }


        [NotMapped]
        public virtual string Action { get; set; } = ActionType.Create;

        public virtual int GetHashCode(RegEmployee employee)
        {
            return employee.Id.GetHashCode();
        }

        public virtual bool Equals(RegEmployee employee1, RegEmployee employee2)
        {
            if (employee1.Id == employee2.Id && employee1.Guid == employee2.Guid &&
                employee1.UserGuid == employee2.UserGuid && employee1.DepartmentId == employee2.DepartmentId &&
                employee1.PositionId == employee2.PositionId && employee1.CheckpointId == employee2.CheckpointId &&
                employee1.PhotoId == employee2.PhotoId && employee1.Name == employee2.Name &&
                employee1.FullName == employee2.FullName && employee1.Email == employee2.Email &&
                employee1.CellPhone == employee2.CellPhone && employee1.Phone == employee2.Phone &&
                employee1.PhysicalAddress == employee2.PhysicalAddress &&
                employee1.PostAddress == employee2.PostAddress && employee1.Skype == employee2.Skype &&
                employee1.ICQ == employee2.ICQ && employee1.BadgeNumber == employee2.BadgeNumber &&
                employee1.Login == employee2.Login && employee1.Password == employee2.Password &&
                employee1.ForcePwdChange == employee2.ForcePwdChange && employee1.ParentId == employee2.ParentId &&
                employee1.Role == employee2.Role && employee1.UserId == employee2.UserId &&
                employee1.InValid == employee2.InValid && employee1.CreationDate == employee2.CreationDate)
            {
                return true;
            }


            if (employee1.Id == employee2.Id | employee1.Guid != employee2.Guid | employee1.UserGuid != employee2.UserGuid |
                employee1.DepartmentId != employee2.DepartmentId | employee1.PositionId != employee2.PositionId |
                employee1.CheckpointId != employee2.CheckpointId | employee1.PhotoId != employee2.PhotoId |
                employee1.Name != employee2.Name | employee1.FullName != employee2.FullName |
                employee1.Email != employee2.Email | employee1.CellPhone != employee2.CellPhone |
                employee1.Phone != employee2.Phone | employee1.PhysicalAddress != employee2.PhysicalAddress |
                employee1.PostAddress != employee2.PostAddress | employee1.Skype != employee2.Skype |
                employee1.ICQ != employee2.ICQ | employee1.BadgeNumber != employee2.BadgeNumber |
                employee1.Login != employee2.Login | employee1.Password != employee2.Password |
                employee1.ForcePwdChange != employee2.ForcePwdChange | employee1.ParentId != employee2.ParentId |
                employee1.Role != employee2.Role | employee1.UserId != employee2.UserId |
                employee1.InValid != employee2.InValid | employee1.CreationDate != employee2.CreationDate)
            {
                employee2.Action = ActionType.Update;
            }

            return false;
        }
    }
}
