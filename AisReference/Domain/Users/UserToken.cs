namespace AisReference.Domain.Users;

public class UserToken
{
    public int Id { get; set; }
    public long UserId { get; set; }
    public string? Token { get; set; }
    public string? JwtToken { get; set; }
    public DateTime Expires { get; set; }
    public bool IsExpired => DateTime.UtcNow >= Expires;
    public string? ReplacedByToken { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime? RevokedDate { get; set; }
    public string? CreatedByIp { get; set; }
    public string? RevokedByIp { get; set; }
    public bool? IsActive => RevokedDate == null && !IsExpired;
}