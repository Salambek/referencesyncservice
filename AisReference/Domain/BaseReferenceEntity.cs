using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace AisReference.Domain;

public class BaseReferenceEntity
{
    [Display(Name = "������������")]
    public string? Name { get; set; }
    [Display(Name = "������������ �� ����������")]
    public string? NameKy { get; set; }
    [Display(Name = "������������ �� �������")]
    public string? NameRu { get; set; }
    
    [DefaultValue("CURRENT_TIMESTAMP")]
    [Display(Name = "���� ��������")]
    public DateTime? CreatedDate { get; set; } = DateTime.Now;
    
    [DefaultValue("CURRENT_TIMESTAMP")]
    [Display(Name = "���� ���������")]
    public DateTime? UpdatedDate { get; set; } = DateTime.Now;
    
    [DefaultValue("true")]
    [Display(Name = "��������")]
    public bool? IsActive { get; set; } = true;
    [Display(Name = "���� ��������")]
    public DateTime? DeletedDate { get; set; } = null;
    
    [JsonIgnore]
    [Display(Name = "��������� � ����� ����")]
    public bool? SendToFrontOffice { get; set; } = true;

}