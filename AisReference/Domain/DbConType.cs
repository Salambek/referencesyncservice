namespace AisReference.Domain;

public enum DbConType
{
    DefaultDbCon,
    EleedDbCon,
    EkmDbCon,
    SiteDbCon
}