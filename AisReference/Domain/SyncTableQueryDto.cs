namespace AisReference.Domain;

public class SyncTableQueryDto
{
    public string SourceSelectQuery { get; set; }
    public string DestinationSelectQuery { get; set; }
    public string CreateQuery { get; set; }
    public string UpdateQuery { get; set; }
    public string DeleteQuery { get; set; }
    public string EnableTriggerQuery { get; set; }
    public string DisableTriggerQuery { get; set; }
    
    public SyncTableQueryDto(string sourceSelectQuery, string destinationSelectQuery, string createQuery,
        string updateQuery, string deleteQuery)
    {
        SourceSelectQuery = sourceSelectQuery;
        DestinationSelectQuery = destinationSelectQuery;
        CreateQuery = createQuery;
        UpdateQuery = updateQuery;
        DeleteQuery = deleteQuery;
    }
}