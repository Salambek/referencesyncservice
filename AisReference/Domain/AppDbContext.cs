﻿using AisReference.Domain.Ekm;
using AisReference.Domain.Eleed;
using AisReference.Domain.Registry;
using Microsoft.EntityFrameworkCore;
using AisReference.Extensions;
using AisReference.Domain.Users;
using MtPermitsBackend.Domain.Registry;

namespace AisReference.Domain
{
    public class AppDbContext : DbContext
    {
        protected readonly IConfiguration Configuration;


        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                //Replace table names
                entity.SetTableName(entity.GetTableName().ToSnakeCase());

                //Replace column names            
                foreach (var property in entity.GetProperties())
                {
                    property.SetColumnName(property.GetColumnBaseName().ToSnakeCase());
                }

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(key.GetName().ToSnakeCase());
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.SetConstraintName(key.GetConstraintName().ToSnakeCase());
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.SetDatabaseName(index.GetDatabaseName().ToSnakeCase());
                }
            }

            //modelBuilder.Entity<Lot>()
            //.HasMany(e => e.Routes)
            //.WithOne(e => e.Lot)
            //.HasForeignKey(e => e.LotId)
            //.IsRequired();

            //modelBuilder.Entity<Lot>()
            //    .HasMany(e => e.Attachments)
            //    .WithOne(e => e.Lot)
            //    .HasForeignKey(e => e.LotId)
            //    .IsRequired();

            modelBuilder.Entity<UserUserRole>()
                    .HasKey(e => new { e.UserId, e.RoleId });

            modelBuilder.Entity<RefFuelType>().HasData(new RefFuelType[]
            {
            new RefFuelType{Id=1, NameKy= "Дизель", NameRu="Дизель"},
            new RefFuelType{Id=2, NameKy= "Газ", NameRu="Газ"},
            new RefFuelType{Id=3, NameKy= "Электро", NameRu="Электро"},
            new RefFuelType{Id=4, NameKy= "Бензин", NameRu="Бензин"},
            });
        }

        #region Users
        public DbSet<User> Users { get; set; }
        public DbSet<UserToken> UserToken { get; set; }
        public DbSet<RegEmployee> RegEmployee { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<UserUserRole> UserUserRole { get; set; }
        public DbSet<UserGroup> UserGroup { get; set; }
        public DbSet<UserInGroup> UserInGroup { get; set; }
        #endregion

        #region References
        public DbSet<RefModelType> RefModelTypes { get; set; }
        public DbSet<RefApplicationStatus> RefApplicationStatus { get; set; }
        public DbSet<RefCarrierState> RefCarrierStates { get; set; }
        public DbSet<RefCarrierCategory> RefCarrierCategories { get; set; }
        public DbSet<RefCountry> RefCountries { get; set; }
        public DbSet<RefDepartment> RefDepartments { get; set; }
        public DbSet<RefLotStatus> RefLotStatus { get; set; }
        public DbSet<RefOwnershipType> RefOwnershipTypes { get; set; }
        public DbSet<RefOrgStructure> RefOrgStructures { get; set; }
        public DbSet<RefRegion> RefRegions { get; set; }
        public DbSet<RefRouteType> RefRouteTypes { get; set; }
        public DbSet<RefSpecialistPosition> RefSpecialistPositions { get; set; }
        public DbSet<RefVehicleBrand> RefVehicleBrands { get; set; }
        public DbSet<RefVehicleKind> RefVehicleKinds { get; set; }
        public DbSet<RefVehicleType> RefVehicleTypes { get; set; }
        public DbSet<RefFuelType> RefFuelTypes { get; set; }
        public DbSet<RefUsageBasis> RefUsageBasis { get; set; }
        public DbSet<RefRegType> RefRegTypes { get; set; }
        #endregion


        #region Eleed references
        public DbSet<RefCoveredTer> RefCoveredTers { get; set; }
        public DbSet<RefLicenseState> RefLicenseStates { get; set; }
        public DbSet<RefActivityType> RefActivityTypes { get; set; }
        public DbSet<RefEuroLimit> RefEuroLimit { get; set; }
        public DbSet<RefFormRequestType> RefFormRequestType { get; set; }
        public DbSet<RefPermitStatus> RefPermitStatus { get; set; }
        public DbSet<PermitFormReceivedBlankNumber> PermitFormReceivedBlankNumber { get; set; }
        #endregion

        #region Ekm references
        public DbSet<RefAttachmentType> RefAttachmentTypes { get; set; }
        public DbSet<DomesticRegularRoute> DomesticRegularRoutes { get; set; }
        public DbSet<RefTechnicalBaseCategory> RefTechnicalBaseCategories { get; set; }

        #endregion


        #region Registry
        public DbSet<RegistryCarrier> RegistryCarriers { get; set; }
        public DbSet<RegistryCarrierSpecialist> RegistryCarrierSpecialists { get; set; }
        public DbSet<RegistryCarrierVehicle> RegistryCarrierVehicles { get; set; }
        public DbSet<RegistrySpecialist> RegistrySpecialists { get; set; }
        public DbSet<RegistryVehicle> RegistryVehicles { get; set; }
        public DbSet<RegistryLicense> RegistryLicenses { get; set; }
        public DbSet<RegistryLicenseBlank> RegistryLicenseBlanks { get; set; }
        public DbSet<RegistryPermitForm> RegistryPermitForms { get; set; }

        #endregion
    }
}
