using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

public class RefRouteType : BaseReferenceEntity, IEqualityComparer<RefRouteType>
{
    public const int International = 1; //Международный
    public const int Intercity = 2; //Междугородный
    public const int Suburban = 3; //Пригородный
    public const int Urban = 4; //Городской
    
    //782027645255681 Международные
    //782027645255682 Внутриобластной междугородний
    //782027645255683 Внутриобластной пригородный
    //782027645255684 Внутриообластной городской
    //782027645255685 Межобластной междугородний
    //782027645255686 Городской (Бишкек или Ош)
    //782027645255687 Международный пригородный
    
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public virtual long Id { get; set; }
    
    [NotMapped]
    public virtual string Action { get; set; } = ActionType.Create;
    
    public virtual int GetHashCode(RefRouteType routeType)
    {
        return routeType.Id.GetHashCode();
    }
    
    public virtual bool Equals(RefRouteType routeType1, RefRouteType routeType2)
    {
        if (routeType1.Id == routeType2.Id && 
            routeType1.Name == routeType2.Name)
        {
            return true;
        }

        if (routeType1.Id == routeType2.Id | routeType1 != routeType2)
        {
            routeType2.Action = ActionType.Update;
        }

        return false;
    }
}