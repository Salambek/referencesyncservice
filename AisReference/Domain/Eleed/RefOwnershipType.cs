using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

public class RefOwnershipType : BaseReferenceEntity, IEqualityComparer<RefOwnershipType>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "�������������")]
    public long Id { get; set; }
    [Display(Name = "����������������� ���")]
    public string? ClassCode { get; set; } //F80705
    [Display(Name = "��� ���. �������")]
    public string? IDCode { get; set; } //F80706

    [NotMapped] public string Action { get; set; } = ActionType.Create;

    public int GetHashCode(RefOwnershipType model)
    {
        return model.Id.GetHashCode();
    }

    public bool Equals(RefOwnershipType model1, RefOwnershipType model2)
    {
        if (model1.Id == model2.Id && model1.Name == model2.Name && model1.NameRu == model2.NameRu &&
            model1.NameKy == model2.NameKy && model1.ClassCode == model2.ClassCode && model1.IDCode == model2.IDCode)
        {
            return true;
        }

        if (model1.Id == model2.Id && model1.Name != model2.Name | model1.NameRu != model2.NameRu |
            model1.NameKy != model2.NameKy |
            model1.ClassCode != model2.ClassCode | model1.IDCode != model2.IDCode)
        {
            model2.Action = ActionType.Update;
        }

        return false;
    }
}