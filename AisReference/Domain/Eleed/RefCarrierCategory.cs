﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AisReference.Domain.Eleed;

//Категория автоперевозчика
//A839
public class RefCarrierCategory : BaseReferenceEntity, IEqualityComparer<RefCarrierCategory>
{
    public const long YrLico = 230622563926016;
    public const long FizLico = 230622563926017;
    public const long Official = 230622563926020;


    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long Id { get; set; }
    public Guid Guid { get; set; }


    [NotMapped]
    public string Action { get; set; } = ActionType.Create;


    public int GetHashCode(RefCarrierCategory model)
    {
        return model.Id.GetHashCode();
    }

    public bool Equals(RefCarrierCategory model1, RefCarrierCategory model2)
    {
        if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.Name == model2.Name)
        {
            return true;
        }

        if (model1.Id == model2.Id && model1.Guid != model2.Guid | model1.Name != model2.Name)
        {
            model2.Action = ActionType.Update;

        }

        return false;
    }

}