﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

//A615 Подразделения
public class RefDepartment : BaseReferenceEntity, IEqualityComparer<RefDepartment>
{
    public const long Central = 169049912770561; //Code 01
    public const long Bishkek = 169049912770564; //Code 02
    public const long Chui = 169049912770565; //Code 03
    public const long Naryn = 169049912770566; //Code 04
    public const long YssykKul = 169049912770567; //Code 05
    public const long Talas = 169049912770568; //Code 06
    public const long Osh = 169049912770569; //Code 07
    public const long JalalAbad = 169049912770570; //Code 08
    public const long Batken = 169049912770571;  //Code 09
    
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long Id { get; set; }
    [Display(Name = "Адрес")]
    public string? Address { get; set; }
    [Display(Name = "Телефон")]
    public string? Phone { get; set; }
    [Display(Name = "Email")]
    public string? Email { get; set; }
    [Display(Name = "ИНН")]
    public string? INN { get; set; }
    [Display(Name = "ОКПО")]
    public string? CodeOKPO { get; set; }
    public string? Code { get; set; }
    [Display(Name = "Геолокация")]
    public string? Geolocation { get; set; }
    
    public long? RegionId { get; set; }
    [ForeignKey("RegionId")]
    [Display(Name = "Начальный АТЕ")]
    public RefRegion? Region { get; set; }
    [Display(Name = "График работы")]
    public string? Schedule { get; set; }
    public long? ChiefId { get; set; }
    [Display(Name = "Начальник")]
    public string? Chief { get; set; }
    
    public long? HeadOfDepartmentId { get; set; }
    [Display(Name = "Начальник отдела")]
    public string? HeadOfDepartment { get; set; }
    
  [NotMapped] 
        public virtual string Action { get; set; } = ActionType.Create;

        public virtual int GetHashCode(RefDepartment department)
        {
            return department.Id.GetHashCode();
        }
            

        public virtual bool Equals(RefDepartment department1, RefDepartment department2)
        {
            if (department1.Id == department2.Id && department1.ChiefId == department2.ChiefId && department1.Address == department2.Address &&
                department1.Phone == department2.Phone &&
                department1.Email == department2.Email && department1.Schedule == department2.Schedule && department1.Name == department2.Name &&
                department1.Geolocation == department2.Geolocation && department1.RegionId == department2.RegionId && department1.HeadOfDepartmentId == department2.HeadOfDepartmentId &&
                department1.INN == department2.INN && department1.CodeOKPO == department2.CodeOKPO)
            {
                return true;
            }


            if (department1.Id == department2.Id && department1.ChiefId != department2.ChiefId | department1.Address != department2.Address | department1.Phone != department2.Phone |
                department1.Email != department2.Email | department1.Schedule != department2.Schedule | department1.Name != department2.Name |
                department1.Geolocation != department2.Geolocation | department1.RegionId != department2.RegionId | department1.HeadOfDepartmentId != department2.HeadOfDepartmentId |
                department1.INN != department2.INN | department1.CodeOKPO != department2.CodeOKPO)
            {
                department2.Action = ActionType.Update;

            }        
            
            return false;
        }
    
}