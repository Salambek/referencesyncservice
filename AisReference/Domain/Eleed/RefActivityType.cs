﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AisReference.Domain.Eleed

{
    //A834
    public class RefActivityType : IEqualityComparer<RefActivityType>
    {
        
        public const long WaterCargo = 229248174391302;
        public const long WaterPassenger = 229248174391303;
        
        [Key]
        public long Id { get; set; }
        
        public Guid Guid { get; set; }
        
        public string? Abbreviation { get; set; }//F81840
        public string? Code { get; set; }//F80197
        public long? CoveredTerId { get; set; }//F311301
        
        [ForeignKey("CoveredTerId")]
        public virtual RefCoveredTer? CoveredTer { get; set; }
        public string? NameRus { get; set; }//F80194
        public string? NameKyr { get; set; }//F80195
        public string? LicNameRus { get; set; }//F80910
        public string? LicNameKyr { get; set; }//F80908
        public long? Type { get; set; }//F81910
        public bool? IsWaterTransport { get; set; }//F392562
        public bool? InValid { get; set; } //F80912
        
        public bool? IsDeleted { get; set; } = false;

        [NotMapped] 
        public string Action { get; set; } = ActionType.Create;
        
        [NotMapped]
        public string Name
        {
            get 
            {
                if (NameRus != null)
                {
                    return NameRus;
                }
                else
                {
                    return NameKyr;
                }
            }
        }

                
        [NotMapped]
        public int ValidYear
        {
            get 
            {
                //Cargo = 67070209294336 WaterCargo = 67070209294340
                if (Type == 67070209294336 || Type == 67070209294340)
                    return 3;
                //Passenger = 67070209294337 WaterPassenger = 67070209294341
                return 1;
            }
        }
        
        public override string ToString()
        {
            return Name;
        }
        
        public int GetHashCode(RefActivityType model)
        {
            return model.Id.GetHashCode();
        }


        public bool Equals(RefActivityType model1, RefActivityType model2)
        {
            if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.NameKyr == model2.NameKyr &&
                model1.NameRus == model2.NameRus && model1.Code == model2.Code && model1.InValid == model2.InValid &&
                model1.Abbreviation == model2.Abbreviation && model1.CoveredTerId == model2.CoveredTerId &&
                model1.LicNameKyr == model2.LicNameKyr && model1.LicNameRus == model2.LicNameRus &&
                model1.Type == model2.Type && model1.IsWaterTransport == model2.IsWaterTransport)
            {
                return true;
            }

            if (model1.Id == model2.Id && model1.Guid != model2.Guid | model1.NameKyr != model2.NameKyr |
                model1.NameRus != model2.NameRus | model1.Code != model2.Code | model1.InValid != model2.InValid |
                model1.Abbreviation != model2.Abbreviation |
                model1.CoveredTerId != model2.CoveredTerId | model1.LicNameKyr != model2.LicNameKyr |
                model1.LicNameRus != model2.LicNameRus | model1.Type != model2.Type) 
            {
                model2.Action = ActionType.Update;
            }        
            
            return false;
        }
    }
}