﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

//A3100 Категории АТС
public class RefVehicleType : BaseReferenceEntity, IEqualityComparer<RefVehicleType>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long Id { get; set; }
    [Display(Name = "Описание")]
    public string? Descriptions { get; set; }//F351905
    
    [NotMapped]
    public virtual string Action { get; set; } = ActionType.Create;
    
    public virtual int GetHashCode(RefVehicleType vehicleType)
    {
        return vehicleType.Id.GetHashCode();
    }
    
    public virtual bool Equals(RefVehicleType vehicleType1, RefVehicleType vehicleType2)
    {
        if (vehicleType1.Id == vehicleType2.Id && 
            vehicleType1.Name == vehicleType2.Name &&
            vehicleType1.Descriptions == vehicleType2.Descriptions)
        {
            return true;
        }

        if (vehicleType1.Id == vehicleType2.Id &&  vehicleType1.Name != vehicleType2.Name |
            vehicleType1.Descriptions != vehicleType2.Descriptions)
        {
            vehicleType2.Action = ActionType.Update;
        }

        return false;
    }
    
}