﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

public class RefVehicleKind : BaseReferenceEntity, IEqualityComparer<RefVehicleKind>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long Id { get; set; }
    [Display(Name = "Код")]
    public string? Code { get; set; }//311318
    [Display(Name = "Прицеп")]
    public bool? IsTrailer { get; set; }//311319
    [Display(Name = "Водный транспорт")]
    public bool? IsWaterTransport { get; set; } //392560

    [Display(AutoGenerateField = false)]
    public long? VehicleTypeId { get; set; }  
    [ForeignKey("VehicleTypeId")]
    [Display(Name = "Вид транспорта")]
    public RefVehicleType? VehicleType { get; set; } //351906
    
    [NotMapped]
    public virtual string Action { get; set; } = ActionType.Create;
    
    public virtual int GetHashCode(RefVehicleKind vehicleKind)
    {
        return vehicleKind.Id.GetHashCode();
    }
    
    public virtual bool Equals(RefVehicleKind vehicleKind1, RefVehicleKind vehicleKind2)
    {
        if (vehicleKind1.Id == vehicleKind2.Id && 
            vehicleKind1.Name == vehicleKind2.Name &&
            vehicleKind1.Code == vehicleKind2.Code &&
            vehicleKind1.IsTrailer == vehicleKind2.IsTrailer &&
            vehicleKind1.IsWaterTransport == vehicleKind2.IsWaterTransport &&
            vehicleKind1.VehicleTypeId == vehicleKind2.VehicleTypeId)
        {
            return true;
        }

        if (vehicleKind1.Id == vehicleKind2.Id | vehicleKind1.Name != vehicleKind2.Name |
            vehicleKind1.Code != vehicleKind2.Code |
            vehicleKind1.IsTrailer != vehicleKind2.IsTrailer |
            vehicleKind1.IsWaterTransport != vehicleKind2.IsWaterTransport |
            vehicleKind1.VehicleTypeId != vehicleKind2.VehicleTypeId)
        {
            vehicleKind2.Action = ActionType.Update;
        }

        return false;
    }
    
}