﻿
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Eleed
{
    //A214
    public class RefCheckpoint : IEqualityComparer<RefCheckpoint>
    {
        [Key]
        [Display(Name = "Идентификатор")]
        public long Id { get; set; }
        public Guid Guid { get; set; }
        [Display(Name = "Название на русском")]
        public string? NameRus { get; set; } //[F20058]
        [Display(Name = "Название на кыргызском")] 
        public string? NameKyr { get; set; } //[F20059]
        [Display(Name = "Электронная почта")]
        public string? Email { get; set; } //[F20063]
        [Display(Name = "Номер телефона")]
        public string? Phone { get; set; }//[F20061]
        [Display(Name = "Номер пункта")]
        public string? Number { get; set; } //[F20213]
        [Display(Name = "Номера телефакса")]
        public string? Fax { get; set; } //[F20062]
        [Display(Name = "Геолокация")]
        public string? Geolocation { get; set; } //[F80553]
        [Display(Name = "Местонахождение")]
        public string? Location { get; set; } //[F20060] 
        public long? CountryId { get; set; } //[F80418]
        [ForeignKey("CountryId")]
        [Display(Name = "Страна")]
        public virtual RefCountry? Country { get; set; }

        public long? DepartureCountryId { get; set; } //[F331827]
        [ForeignKey("DepartureCountryId")]
        [Display(Name = "Страна отправления по умолчанию")]
        public virtual RefCountry? DepartureCountry { get; set; }

        public long? DestinationCountryId { get; set; } //[F331828]
        [ForeignKey("DestinationCountryId")]
        [Display(Name = "Страна назначения по умолчанию")]
        public virtual RefCountry? DestinationCountry { get; set; }

        public long? DepartmentId { get; set; } //[F792869]
        [ForeignKey("DepartmentId")]
        [Display(Name = "Подразделение")]
        public virtual RefDepartment? Department { get; set; }

        [Display(Name = "На внешней границе EAЭC")]
        public bool? ExternalCheckpoint { get; set; } //[F81265]
        [Display(Name = "Строка подключения к сервису логирования снимков")]
        public string? ImageLogService { get; set; } //[F372409]
        [Display(Name = "На границе КР")]
        public bool? KGBorderCheckpoint { get; set; } //[F793455]

        [Display(Name = "Пункт проверки уведомления")]
        public bool? ViolationCheckpoint { get; set; } //[F80464]
        [Display(Name = "Вид дороги")]
        public long? RoadType { get; set; } //[F351917]

        [Display(Name = "Идентификатор пункта пропуска")]
        public long? PostId { get; set; } //[F792870]
                                          //[ForeignKey("PostId")]
                                          //public virtual RefPost? Post { get; set; }

        [Display(Name = "Идентификатор пользователя")]
        public long? UserId { get; set; } //[F792868]
        //[ForeignKey("UserId")]
        //public virtual RegEmployee? User { get; set; }
        [Display(Name = "IP табло 1")]
        public string? BoardIP1 { get; set; } //[F372428]
        [Display(Name = "IP табло 2")]
        public string? BoardIP2 { get; set; } //[F372429]
        [Display(Name = "IP камеры 1")]
        public string? CameraIP1 { get; set; } //[F81294]
        [Display(Name = "IP камеры 2")]
        public string? CameraIP2 { get; set; } //[F81295]

        [Display(Name = "Дата модификации")]
        public DateTime? ModifiedDate { get; set; } //[F792867]
        [Display(Name = "Дата создания")]
        public DateTime? CreationDate { get; set; } //[F792866]
        [Display(Name = "Не действует")]
        public bool? InValid { get; set; } //[F81031]
        [Display(Name = "Удален?")]
        public bool? IsDeleted { get; set; } = false;

        [NotMapped]
        [Display(Name = "Название")]
        public string Name
        {
            get { return NameRus ?? NameKyr; }
        }

        [NotMapped]
        public string Action { get; set; } = ActionType.Create;

        public int GetHashCode(RefCheckpoint model)
        {
            return model.Id.GetHashCode();
        }

        public bool Equals(RefCheckpoint model1, RefCheckpoint model2)
        {
            if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.NameRus == model2.NameRus &&
                model1.NameKyr == model2.NameKyr && model1.Email == model2.Email && model1.Phone == model2.Phone &&
                model1.Fax == model2.Fax && model1.Geolocation == model2.Geolocation && model1.Location == model2.Location &&
                model1.CountryId == model2.CountryId && model1.DepartureCountryId == model2.DepartureCountryId &&
                model1.DestinationCountryId == model2.DestinationCountryId && model1.DepartmentId == model2.DepartmentId &&
                model1.Number == model2.Number && model1.ExternalCheckpoint == model2.ExternalCheckpoint &&
                model1.ImageLogService == model2.ImageLogService && model1.KGBorderCheckpoint == model2.KGBorderCheckpoint &&
                model1.ViolationCheckpoint == model2.ViolationCheckpoint && model1.RoadType == model2.RoadType &&
                model1.PostId == model2.PostId && model1.UserId == model2.UserId && model1.BoardIP1 == model2.BoardIP1 &&
                model1.BoardIP2 == model2.BoardIP2 && model1.CameraIP1 == model2.CameraIP1 && model1.CameraIP2 == model2.CameraIP2 &&
                model1.ModifiedDate == model2.ModifiedDate && model1.CreationDate == model2.CreationDate &&
                model1.InValid == model2.InValid
               )
            {
                return true;
            }

            if (model1.Id == model2.Id && model1.Guid != model2.Guid | model1.NameRus != model2.NameRus | model1.NameKyr != model2.NameKyr |
                model1.Email != model2.Email | model1.Phone != model2.Phone | model1.Fax != model2.Fax | model1.Geolocation != model2.Geolocation |
                model1.Location != model2.Location | model1.CountryId != model2.CountryId | model1.DepartureCountryId != model2.DepartureCountryId |
                model1.DestinationCountryId != model2.DestinationCountryId | model1.DepartmentId != model2.DepartmentId |
                model1.Number != model2.Number | model1.ExternalCheckpoint != model2.ExternalCheckpoint |
                model1.ImageLogService != model2.ImageLogService | model1.KGBorderCheckpoint != model2.KGBorderCheckpoint |
                model1.ViolationCheckpoint != model2.ViolationCheckpoint | model1.RoadType != model2.RoadType |
                model1.PostId != model2.PostId | model1.UserId != model2.UserId | model1.BoardIP1 != model2.BoardIP1 |
                model1.BoardIP2 != model2.BoardIP2 | model1.CameraIP1 != model2.CameraIP1 | model1.CameraIP2 != model2.CameraIP2 |
                model1.ModifiedDate != model2.ModifiedDate | model1.CreationDate != model2.CreationDate |
                model1.InValid != model2.InValid
               )
            {
                model2.Action = ActionType.Update;
            }

            return false;
        }

    }
}
