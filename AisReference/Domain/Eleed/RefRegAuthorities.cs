
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;


//Органы регистрации субъекта предпринимательства  
//A849
public class RefRegAuthorities : BaseReferenceEntity, IEqualityComparer<RefRegAuthorities>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long Id { get; set; }
    [Display(Name = "Адрес")]
    public string? Address { get; set; } //F80281
    [Display(Name = "Код")]
    public string? Code { get; set; } //F80281
    [Display(Name = "Контакты")]
    public string? Contacts { get; set; } //F80282
    [Display(Name = "Полное наименование на русском")]
    public string? NameFullRu { get; set; } //F80279
    [Display(Name = "Полное наименование на кыргызском")]
    public string? NameFullKy { get; set; } //F80279
    
    [NotMapped]
    public virtual string Action { get; set; } = ActionType.Create;
    
    public virtual int GetHashCode(RefRegAuthorities regAuthorities)
    {
        return regAuthorities.Id.GetHashCode();
    }
    
    public virtual bool Equals(RefRegAuthorities regAuthorities1, RefRegAuthorities regAuthorities2)
    {
        if (regAuthorities1.Id == regAuthorities2.Id && 
            regAuthorities1.Name == regAuthorities2.Name &&
            regAuthorities1.NameFullRu == regAuthorities2.NameFullRu &&
            regAuthorities1.NameFullKy == regAuthorities2.NameFullKy &&
            regAuthorities1.Address == regAuthorities2.Address &&
            regAuthorities1.Code == regAuthorities2.Code &&
            regAuthorities1.Contacts == regAuthorities2.Contacts)
        {
            return true;
        }

        if (regAuthorities1.Id == regAuthorities2.Id && 
            regAuthorities1.Name != regAuthorities2.Name |
            regAuthorities1.NameFullRu != regAuthorities2.NameFullRu |
            regAuthorities1.NameFullKy != regAuthorities2.NameFullKy |
            regAuthorities1.Address != regAuthorities2.Address |
            regAuthorities1.Code != regAuthorities2.Code |
            regAuthorities1.Contacts != regAuthorities2.Contacts)
        {
            regAuthorities2.Action = ActionType.Update;
        }

        return false;
    }
    
    
    
}