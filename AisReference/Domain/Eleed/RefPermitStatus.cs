﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Eleed
{
    public class RefPermitStatus : IHasId
    {
        public const int ReceivedFromElicense = 1; //Поступил из Elicense/Новый
        public const int Accepted = 2; //Принят
        public const int Refused = 3; //Отказано
        public const int Revision = 4; //На доработке
        public const int Verified = 5; //Проверен
        public const int InAgreementGuobdd = 6; //На согласовании в ГУОБДД
        public const int RefusedGuobdd = 7; //Отказано с ГУОБДД
        public const int AgreedWithGuobdd = 8; //Согласовано с ГУОБДД
        public const int PaymentWaiting = 9; //Ожидание оплаты
        public const int PaymentAccepted = 10; //Оплата принята
        public const int PaymentIncorrect = 11; //Оплата некорректна
        public const int Issued = 12; //Выдано
        public const int Extended = 13; //Продлено
        public const int Suspend = 14; //Приостановлено
        public const int Resumed = 15; //Возобновлено 
        public const int Stopped = 16; //Прикрашено  
        public const int Duplicated = 17; //Дубликат выдан
        public const int ChangedATC = 18; //Заменен АТС

        public const int Approved = 19; //Утверждена
        public const int RefusedCorrect = 20; //Отказано для устранения недочетов 
        public const int RefusedInjunctions = 21; //Отказать на основании судебных запретов

        public const int SentToCommissions = 22; //Отправлено на комиссию
        public const int Canceled = 23; //Отменен
        public const int CreatedNew = 24; //Создан новый


        public const int RegistryFree = 25; //Свободный
        public const int RegistryIssued = 26; //Выдан
        public const int RegistryReturned = 27; //Возвращен
        public const int RegistryOverdue = 28; //Просрочен
        public const int RegistryCanceled = 29; //Аннулирован
        public const int RegistryClosed = 30; //Закрыт
        public const int RegistryRezerv = 31; //Резерв
        public const int RegistryTransfered = 32; //Передан

        public const int Distributed = 33; //Распределен
        public const int ReceivedFromDnvt = 34; //Поступил из формы подачи ДНВТ/Новый
        public const int SentForPayment = 35; //Отправлен на оплату
        public const int ReceivedPayment = 36; //Получен платеж

        public const int CommissionAccepted = 37; //Удовлетворено комиссией
        public const int CommissionCancelled = 38; //Отказано комиссией
        public const int RevokedByApplicant = 39; //Отозвано заявителем


        public static readonly int[] LicenseHistoryStatuses = { Extended, Suspend, Resumed, Stopped };
        public static readonly int[] RulingProtocolStatuses = { Approved, RefusedCorrect, RefusedInjunctions, Revision, CommissionAccepted, CommissionCancelled };



        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public bool? IsValid { get; set; } //Действует | Не действует

        public string? ActionName { get; set; } //Действия
        public string? Color { get; set; } //Цвет
        public bool? SendToTunduk { get; set; } //Отправить статус изменение

    }

    public interface IHasId
    {
        Int32 Id { get; }

    }
}