﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Eleed
{
    //Виды бланков разрешения
    public class RefFormRequestType : IEqualityComparer<RefFormRequestType>
    {
        [Key]
        public long Id { get; set; }
        public Guid Guid { get; set; }

        public string? NameRus { get; set; }//F80555
        public string? NameKyr { get; set; }//F80556
        public string? NameShort { get; set; }//F80556
        public bool? InValid { get; set; }//F331842

        public bool? IsDeleted { get; set; } = false;

        [NotMapped]
        public string Name
        {
            get
            {
                if (NameRus != null)
                {
                    return NameRus;
                }
                else
                {
                    return NameKyr;
                }
            }
        }



        [NotMapped]
        public string Action { get; set; } = ActionType.Create;


        public int GetHashCode(RefFormRequestType model)
        {
            return model.Id.GetHashCode();
        }


        public bool Equals(RefFormRequestType model1, RefFormRequestType model2)
        {
            if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.NameKyr == model2.NameKyr &&
                 model1.NameRus == model2.NameRus && model1.NameShort == model2.NameShort && model1.InValid == model2.InValid)
            {
                return true;
            }


            if (model1.Id == model2.Id && model1.Guid != model2.Guid | model1.NameKyr != model2.NameKyr |
                                           model1.NameRus != model2.NameRus | model1.NameShort != model2.NameShort |
                                           model1.InValid != model2.InValid)
            {
                model2.Action = ActionType.Update;

            }

            return false;
        }

    }
}