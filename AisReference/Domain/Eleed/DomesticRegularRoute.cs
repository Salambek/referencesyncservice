﻿using AisReference.Domain;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AisReference.Domain.Eleed;

//A919 Внутренние регулярные маршруты
public class DomesticRegularRoute : BaseReferenceEntity, IEqualityComparer<DomesticRegularRoute>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    public int BackTripDuration { get; set; } = 0; //F311017 Время движения оборотного рейса

    [Display(AutoGenerateField = false)]


    public long? DepartmentId { get; set; }
    [ForeignKey("DepartmentId")]
    public RefDepartment Department { get; set; } //F331593 Подразделение


    [Display(AutoGenerateField = false)]
    public long? DepartureId { get; set; }
    [ForeignKey("DepartureId")]
    public RefRegion? Departure { get; set; } //F311009 Территория пункта назначения
    public string? DepartureName { get; set; } //F311010 Название пункта отправления

    [Display(AutoGenerateField = false)]
    public long? DestinationId { get; set; }
    [ForeignKey("DestinationId")]
    public RefRegion? Destination { get; set; } //F311011 Территория пункта назначения
    public string? DestinationName { get; set; } //F311012 Название пункта назначение

    public int Interval { get; set; } = 0; //F311018 Интервал движения между машинами
    public decimal Length { get; set; } = 0; //F311015 Расстояние по маршруту (км)
    public string NameRoute { get; set; } //F311008 Название маршрута
    public string NumberRoute { get; set; } //F331596 Номер маршрута
    public DateTime RegDate { get; set; } //F331597 Дата добавления

    [Display(AutoGenerateField = false)]
    public long? RouteTypeId { get; set; }
    [ForeignKey("RouteTypeId")]
    public RefRouteType? RouteType { get; set; } //F331598 Вид маршрута

    public int TripDuration { get; set; } = 0; //F311016 Время в наряде одной машины
    public int TripsQty { get; set; } = 0; //F311014 Кол-во рейсов по расписанию

    [Display(AutoGenerateField = false)]
    public long? VehicleKindId { get; set; }
    [ForeignKey("VehicleKindId")]
    public RefVehicleKind? VehicleKind { get; set; } //F331824 Вид АТС
    public int VehiclesQty { get; set; } = 0; //F311013 Кол-во машин на маршруте


    [NotMapped] public string Action { get; set; } = ActionType.Create;

    public int GetHashCode(DomesticRegularRoute model)
    {
        return model.Id.GetHashCode();
    }

    public bool Equals(DomesticRegularRoute model1, DomesticRegularRoute model2)
    {
        if (model1.Id == model2.Id && model1.BackTripDuration == model2.BackTripDuration &&
            model1.DepartmentId == model2.DepartmentId && model1.DepartureId == model2.DepartureId &&
            model1.DepartureName == model2.DepartureName && model1.DestinationId == model2.DestinationId &&
            model1.DestinationName == model2.DestinationName && model1.Interval == model2.Interval &&
            model1.Length == model2.Length && model1.NameRoute == model2.NameRoute &&
            model1.NumberRoute == model2.NumberRoute && model1.RegDate == model2.RegDate &&
            model1.RouteTypeId == model2.RouteTypeId && model1.TripDuration == model2.TripDuration &&
            model1.TripsQty == model2.TripsQty && model1.VehicleKindId == model2.VehicleKindId &&
            model1.VehiclesQty == model2.VehiclesQty)
        {
            return true;
        }

        if (model1.Id == model2.Id | model1.BackTripDuration != model2.BackTripDuration |
            model1.DepartmentId != model2.DepartmentId | model1.DepartureId != model2.DepartureId |
            model1.DepartureName != model2.DepartureName | model1.DestinationId != model2.DestinationId |
            model1.DestinationName != model2.DestinationName | model1.Interval != model2.Interval |
            model1.Length != model2.Length | model1.NameRoute != model2.NameRoute |
            model1.NumberRoute != model2.NumberRoute | model1.RegDate != model2.RegDate |
            model1.RouteTypeId != model2.RouteTypeId | model1.TripDuration != model2.TripDuration |
            model1.TripsQty != model2.TripsQty | model1.VehicleKindId != model2.VehicleKindId |
            model1.VehiclesQty != model2.VehiclesQty)
        {
            model2.Action = ActionType.Update;
        }

        return false;
    }
}