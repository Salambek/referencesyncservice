﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Eleed
{
    public class PermitFormReceivedBlankNumber
    {
        [Key]
        public Guid Guid { get; set; }
        public long CountryId { get; set; }

        public Guid PermitFormReceivedId { get; set; }
        
        public int StartNumber { get; set; }
        public int EndNumber { get; set; }
        public int Quantity { get; set; }
    }
}
