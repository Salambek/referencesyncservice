﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain.Eleed
{
    public class RefEuroLimit : IEqualityComparer<RefEuroLimit>
    {
        [Key]
        public long Id { get; set; }

        public Guid Guid { get; set; }

        public string? NameRus { get; set; }//F130391
        public string? NameKyr { get; set; }//F130392

        public bool? IsDeleted { get; set; } = false;

        [NotMapped]
        public string Action { get; set; } = ActionType.Create;

        [NotMapped]
        public string Name
        {
            get
            {
                if (NameRus != null)
                {
                    return NameRus;
                }
                else
                {
                    return NameKyr;
                }
            }
        }

        public int GetHashCode(RefEuroLimit model)
        {
            return model.Id.GetHashCode();
        }


        public bool Equals(RefEuroLimit model1, RefEuroLimit model2)
        {
            if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.NameKyr == model2.NameKyr &&
                model1.NameRus == model2.NameRus)
            {
                return true;
            }

            if (model1.Id == model2.Id && model1.Guid != model2.Guid | model1.NameKyr != model2.NameKyr |
                model1.NameRus != model2.NameRus)
            {
                model2.Action = ActionType.Update;

            }

            return false;
        }
    }
}