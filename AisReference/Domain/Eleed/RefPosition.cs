﻿
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using AisReference.Domain;

namespace AisReference.Domain.Eleed
{
    public class RefPosition : IEqualityComparer<RefPosition>
    {
        //A616
        [Key]
        [Display(Name = "Идентификатор")]
        public long Id { get; set; }
        public Guid Guid { get; set; }
        [Display(Name = "Название")]
        public string? Name { get; set; }//F60077
        public bool? InValid { get; set; }//F81017
        [Display(Name = "Вес")]
        public int? Weight { get; set; }//F331559
        [Display(Name = "Удален?")]
        public bool? IsDeleted { get; set; }//81019

        [NotMapped]
        public virtual string Action { get; set; } = ActionType.Create;

        public virtual int GetHashCode(RefPosition model)
        {
            return model.Id.GetHashCode();
        }


        public virtual bool Equals(RefPosition model1, RefPosition model2)
        {
            if (model1.Guid == model2.Guid)
            {
                return true;
            }

            if (model1.Id == model2.Id)
            {
                model2.Action = ActionType.Update;

            }

            return false;
        }
    }
}
