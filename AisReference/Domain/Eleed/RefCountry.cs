using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

public class RefCountry : BaseReferenceEntity, IEqualityComparer<RefCountry> 
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "�������������")]
    public long Id { get; set; }
    [Display(Name = "��� ������ 2-� ����������")]
    public string? Code2 { get; set; }
    [Display(Name = "��� ������")]
    public string? Code3 { get; set; } 
    
    [NotMapped] 
    public string Action { get; set; } = ActionType.Create;
        
        
    public int GetHashCode(RefCountry model)
    {
        return model.Id.GetHashCode();
    }
        
    public bool Equals(RefCountry model1, RefCountry model2)
    {
        if (model1.Id == model2.Id && model1.Name == model2.Name && 
            model1.Code2 == model2.Code2 && model1.Code3 == model2.Code3)    
        {
            return true;
        }
            
        if (model1.Id == model2.Id | model1.Name != model2.Name | 
            model1.Code2 != model2.Code2 | model1.Code3 != model2.Code3)
        {
            model2.Action = ActionType.Update;
        }        
            
        return false;
    }
}