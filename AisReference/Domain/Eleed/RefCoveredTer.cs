﻿using AisReference.Domain;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AisReference.Domain.Eleed
{
    //A832
    public class RefCoveredTer : IEqualityComparer<RefCoveredTer>
    {
        [Key]
        public long Id { get; set; }
        
        public Guid Guid { get; set; }
        
        public string? Code { get; set; }//F80185
        public string? NameRus { get; set; }//F80194
        public string? NameKyr { get; set; }//F80195
        
        public bool? InValid { get; set; } //F80912
        
        public bool? IsDeleted { get; set; } = false;

        [NotMapped] 
        public string Action { get; set; } = ActionType.Create;
        
        [NotMapped]
        public string Name
        {
            get 
            {
                if (NameRus != null)
                {
                    return NameRus;
                }
                else
                {
                    return NameKyr;
                }
            }
        }
        
        public int GetHashCode(RefCoveredTer model)
        {
            return model.Id.GetHashCode();
        }


        public bool Equals(RefCoveredTer model1, RefCoveredTer model2)
        {
            if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.NameKyr == model2.NameKyr &&
                model1.NameRus == model2.NameRus && model1.Code == model2.Code && model1.InValid == model2.InValid)
            {
                return true;
            }
            
            if (model1.Id == model2.Id && model1.Guid != model2.Guid | model1.NameKyr != model2.NameKyr |
                model1.NameRus != model2.NameRus | model1.Code != model2.Code | model1.InValid != model2.InValid)
            {
                model2.Action = ActionType.Update;

            }        
            
            return false;
        }
    }
}
