using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace AisReference.Domain.Eleed;

//A878
//Статус лицензии
public class RefLicenseState : IEqualityComparer<RefLicenseState>
{
    [Key]
    public long Id { get; set; }
    public Guid Guid { get; set; } = Guid.NewGuid();
    public string? Name { get; set; } //F80435
    [DefaultValue("CURRENT_TIMESTAMP")]
    public virtual DateTime? CreatedDate { get; set; } = DateTime.Now;
    [DefaultValue("CURRENT_TIMESTAMP")]
    public virtual DateTime? UpdatedDate { get; set; } = DateTime.Now;
    [DefaultValue("true")]
    public virtual bool? Status { get; set; } = true;
    [DefaultValue("false")]
    public virtual bool? IsDeleted { get; set; } = false;
    
    [NotMapped] 
    public string Action { get; set; } = ActionType.Create;


    public int GetHashCode(RefLicenseState model)
    {
        return model.Id.GetHashCode();
    }

    public bool Equals(RefLicenseState model1, RefLicenseState model2)
    {
        if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.Name == model2.Name)
        {
            return true;
        }
        if (model1.Id == model2.Id | model1.Guid != model2.Guid | model1.Name != model2.Name)
        {
            model2.Action = ActionType.Update;
        }

        return false;
    }
}