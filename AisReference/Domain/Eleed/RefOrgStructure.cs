﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

//Организационно-правовая форма
//A920
public class RefOrgStructure : BaseReferenceEntity, IEqualityComparer<RefOrgStructure>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long Id { get; set; }
    [Display(Name = "Полное название на русском")]
    public string? NameFullRu { get; set; } //F80919
    [Display(Name = "Полное название на кыргызском")]
    public string? NameFullKy { get; set; } //F80919
    [Display(Name = "Физ лицо?")]
    public bool? IsIndividual { get; set; } //F80720
    [Display(Name = "Код Мин. юстиции")]
    public string? MinJustCode { get; set; } //F883502

    [NotMapped] 
    public virtual string Action { get; set; } = ActionType.Create;

    public virtual int GetHashCode(RefOrgStructure orgStructure)
    {
        return orgStructure.Id.GetHashCode();
    }

    public virtual bool Equals(RefOrgStructure orgStructure1, RefOrgStructure orgStructure2)
    {
        if (orgStructure1.Id == orgStructure2.Id && 
            orgStructure1.Name == orgStructure2.Name &&
            orgStructure1.NameFullRu == orgStructure2.NameFullRu &&
            orgStructure1.NameFullKy == orgStructure2.NameFullKy &&
            orgStructure1.IsIndividual == orgStructure2.IsIndividual &&
            orgStructure1.MinJustCode == orgStructure2.MinJustCode)
        {
            return true;
        }

        if (orgStructure1.Id == orgStructure2.Id && 
            orgStructure1.Name != orgStructure2.Name |
            orgStructure1.NameFullRu != orgStructure2.NameFullRu |
            orgStructure1.NameFullKy != orgStructure2.NameFullKy |
            orgStructure1.IsIndividual != orgStructure2.IsIndividual |
            orgStructure1.MinJustCode != orgStructure2.MinJustCode)
        {
            orgStructure2.Action = ActionType.Update;
        }

        return false;
    }
}