using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

public class RefUsageBasis : BaseReferenceEntity, IEqualityComparer<RefUsageBasis>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "�������������")]
    public long Id { get; set; }
    
    [NotMapped]
    public virtual string Action { get; set; } = ActionType.Create;
    
    public virtual int GetHashCode(RefUsageBasis usageBasis)
    {
        return usageBasis.Id.GetHashCode();
    }
    
    public virtual bool Equals(RefUsageBasis usageBasis1, RefUsageBasis usageBasis2)
    {
        if (usageBasis1.Id == usageBasis2.Id && 
            usageBasis1.Name == usageBasis2.Name)
        {
            return true;
        }

        if (usageBasis1.Id == usageBasis2.Id | usageBasis1.Name != usageBasis2.Name)
        {
            usageBasis2.Action = ActionType.Update;
        }

        return false;
    }
}