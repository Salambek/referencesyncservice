using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

public class RefSpecialistPosition : BaseReferenceEntity, IEqualityComparer<RefSpecialistPosition>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "�������������")]
    public long Id { get; set; }
    
    [NotMapped]
    public virtual string Action { get; set; } = ActionType.Create;
    
    public virtual int GetHashCode(RefSpecialistPosition specialistPosition)
    {
        return specialistPosition.Id.GetHashCode();
    }
    
    public virtual bool Equals(RefSpecialistPosition specialistPosition1, RefSpecialistPosition specialistPosition2)
    {
        if (specialistPosition1.Id == specialistPosition2.Id && 
            specialistPosition1.Name == specialistPosition2.Name)
        {
            return true;
        }

        if (specialistPosition1.Id == specialistPosition2.Id | specialistPosition1 != specialistPosition2)
        {
            specialistPosition2.Action = ActionType.Update;
        }

        return false;
    }
}