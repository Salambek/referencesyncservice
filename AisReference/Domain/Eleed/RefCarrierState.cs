﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AisReference.Domain.Eleed;

//Статус автоперевозчика
//A922
public class RefCarrierState : BaseReferenceEntity, IEqualityComparer<RefCarrierState>
{
    public const long Valid = 253437430202368;
    public const long Archive = 253437430202369;

    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long Id { get; set; }

    [NotMapped]
    public string Action { get; set; } = ActionType.Create;


    public int GetHashCode(RefCarrierState model)
    {
        return model.Id.GetHashCode();
    }

    public bool Equals(RefCarrierState model1, RefCarrierState model2)
    {
        if (model1.Id == model2.Id && model1.Name == model2.Name)
        {
            return true;
        }

        if (model1.Id == model2.Id | model1.Name != model2.Name)
        {
            model2.Action = ActionType.Update;

        }

        return false;
    }
}
