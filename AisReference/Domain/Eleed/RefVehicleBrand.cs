using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

public class RefVehicleBrand : BaseReferenceEntity, IEqualityComparer<RefVehicleBrand>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "�������������")]
    public long Id { get; set; }
    [Display(Name = "������������")]
    public string? Name { get; set; }//522744
    [Display(Name = "���")]
    public string? Code { get; set; }//20188
    [Display(Name = "������ ���������")]
    public bool? IsWaterTransport { get; set; } = false; //392561
    
    [NotMapped]
    public virtual string Action { get; set; } = ActionType.Create;
    
    public virtual int GetHashCode(RefVehicleBrand vehicleBrand)
    {
        return vehicleBrand.Id.GetHashCode();
    }
    
    public virtual bool Equals(RefVehicleBrand vehicleBrand1, RefVehicleBrand vehicleBrand2)
    {
        if (vehicleBrand1.Id == vehicleBrand2.Id && 
            vehicleBrand1.Name == vehicleBrand2.Name &&
            vehicleBrand1.Code == vehicleBrand2.Code &&
            vehicleBrand1.IsWaterTransport == vehicleBrand2.IsWaterTransport)
        {
            return true;
        }

        if (vehicleBrand1.Id == vehicleBrand2.Id | vehicleBrand1.Name != vehicleBrand2.Name |
            vehicleBrand1.Code != vehicleBrand2.Code |
            vehicleBrand1.IsWaterTransport != vehicleBrand2.IsWaterTransport)
        {
            vehicleBrand2.Action = ActionType.Update;
        }

        return false;
    }
}