﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Eleed;

public class RefRegion : BaseReferenceEntity, IEqualityComparer<RefRegion>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long Id { get; set; }
    [Display(Name = "Наименование")]
    public string? Name { get; set; }//80914
    [Display(Name = "Код")]
    public string? Code { get; set; }//80913
    
    [NotMapped]
    public virtual string Action { get; set; } = ActionType.Create;
    
    public virtual int GetHashCode(RefRegion regAuthorities)
    {
        return regAuthorities.Id.GetHashCode();
    }
    
    public virtual bool Equals(RefRegion region1, RefRegion region2)
    {
        if (region1.Id == region2.Id && 
            region1.Name == region2.Name &&
            region1.Code == region2.Code)
        {
            return true;
        }

        if (region1.Id == region2.Id && 
            region1.Name != region2.Name |       
            region1.Code != region2.Code)
        {
            region2.Action = ActionType.Update;
        }

        return false;
    }

}