﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;
using AisReference.Domain.Ekm;
using AisReference.Domain.Eleed;

namespace AisReference.Domain.Registry;

public class RegistryCarrier : IEqualityComparer<RegistryCarrier>
{
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public Guid Guid { get; set; } = new Guid();
        public string? BaseAddress { get; set; } //F80771
        public string? BrandName { get; set; } //F81241
        
        [Display(AutoGenerateField = false)]
        public long? BaseRegionId { get; set; } //F81356
        [ForeignKey("BaseRegionId")] 
        public virtual RefRegion? BaseRegion { get; set; }

        [Display(AutoGenerateField = false)]
        public long? CarrierCategoryId { get; set; } //F80735
        [ForeignKey("CarrierCategoryId")] 
        public virtual RefCarrierCategory? CarrierCategory { get; set; }
        
        [Display(AutoGenerateField = false)]
        public long? CarrierStateId { get; set; } //F80729
        [ForeignKey("CarrierStateId")] 
        public virtual RefCarrierState? CarrierState { get; set; }

        public string? Comment { get; set; } //F80734

        [Display(AutoGenerateField = false)]
        public long? DepartmentId { get; set; } //F80728
        [ForeignKey("DepartmentId")] 
        public virtual RefDepartment? Department { get; set; }

        public string? Education { get; set; } //F80745
        public string? Email { get; set; } //F80748
        public DateTime? Experience { get; set; } //F80746
        public string? HeadName { get; set; } //F80743
        public string? HeadPosition { get; set; } //80744
        public string? INN { get; set; } //F80736
        public string? LegalAddress { get; set; } //F80753

        [Display(AutoGenerateField = false)]
        public long? LegalRegionId { get; set; } //F81355       
        [ForeignKey("LegalRegionId")] 
        public virtual RefRegion? LegalRegion { get; set; }

        public string? Lessor { get; set; } //F311313 Арендодатель
        
        
        public string? Login { get; set; } //F80774
        public string? MiddleName { get; set; } //F311339
        public string? Name { get; set; } //F311338
        public string? NameLegal { get; set; } //F80739

        [Display(AutoGenerateField = false)]
        public long? OrgStructureId { get; set; } //F80737       
        [ForeignKey("OrgStructureId")] 
        public virtual RefOrgStructure? OrgStructure { get; set; }

        [Display(AutoGenerateField = false)]
        public long? OwnershipTypeId { get; set; } //F80738       
        [ForeignKey("OwnershipTypeId")] 
        public virtual RefOwnershipType? OwnershipType { get; set; }

        public DateTime? PassportDate { get; set; } //F80759
        public DateTime? PassportEnd { get; set; } //F80760
        public string? PassportIssuedBy { get; set; } //F80761
        public string? PassportNum { get; set; } //F80758
        public string? Password { get; set; } //F80775 пароль
        public bool? PasswordHashed { get; set; } //F883491
        public string? Phones { get; set; } //F80747
        public string? RealAddress { get; set; } //F80757


        [Display(AutoGenerateField = false)]
        public long? RealRegionId { get; set; } //F80755    
        [ForeignKey("RealRegionId")] 
        public virtual RefRegion? RealRegion { get; set; }


        [Display(AutoGenerateField = false)]
        public long? RegAuthoritiesId { get; set; } //F80763
        [ForeignKey("RegAuthoritiesId")] 
        public virtual RefRegAuthorities? RegAuthorities { get; set; }

        public DateTime? RegDate { get; set; } //F80766
        public DateTime? RegExpirationDate { get; set; } //F80767

        [Display(AutoGenerateField = false)]
        public long? RegionId { get; set; } //F80916
        [ForeignKey("RegionId")] 
        public virtual RefRegion? Region { get; set; }

        public string? RegNumber { get; set; } //F80765
        public string? RegSeries { get; set; } //F80764 Серия регистрации
        
        
        [Display(AutoGenerateField = false)]
        public long? RegTypeId { get; set; } //F80762 Регистрация
        [ForeignKey("RegTypeId")] 
        public virtual RefRegType? RegType { get; set; }

        
        public DateTime? RegСarDate { get; set; } //F81933 Ввод в систему

        public string? ShortName { get; set; } //F81360
        public long? SubjectId { get; set; } //F170992
        public string? Surname { get; set; } //F311337

        [Display(AutoGenerateField = false)]
        public long? UsageBasisId { get; set; } //F80772
        [ForeignKey("UsageBasisId")] 
        public virtual RefUsageBasis? UsageBasis { get; set; }
        
        public DateTime? UsageExpireDate { get; set; } //F80773
        
        public DateTime? ExperienceDate { get; set; } = new DateTime(); //Дата начала стажа
        
        [NotMapped] 
        public string Action { get; set; } = ActionType.Create;


        public int GetHashCode(RegistryCarrier model)
        {
                return model.Id.GetHashCode();
        }
        
        public bool Equals(RegistryCarrier model1, RegistryCarrier model2)
        {
            if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.BaseAddress == model2.BaseAddress &&
                model1.BrandName == model2.BrandName && model1.BaseRegionId == model2.BaseRegionId &&
                model1.CarrierCategoryId == model2.CarrierCategoryId &&
                model1.CarrierStateId == model2.CarrierStateId && model1.Comment == model2.Comment &&
                model1.DepartmentId == model2.DepartmentId && model1.Education == model2.Education &&
                model1.Email == model2.Email && model1.Experience == model2.Experience &&
                model1.HeadName == model2.HeadName && model1.HeadPosition == model2.HeadPosition &&
                model1.INN == model2.INN && model1.LegalAddress == model2.LegalAddress &&
                model1.LegalRegionId == model2.LegalRegionId && model1.Lessor == model2.Lessor &&
                model1.Login == model2.Login && model1.MiddleName == model2.MiddleName && model1.Name == model2.Name &&
                model1.NameLegal == model2.NameLegal && model1.OrgStructureId == model2.OrgStructureId &&
                model1.OwnershipTypeId == model2.OwnershipTypeId && model1.PassportDate == model2.PassportDate &&
                model1.PassportEnd == model2.PassportEnd && model1.PassportIssuedBy == model2.PassportIssuedBy &&
                model1.PassportNum == model2.PassportNum && model1.Password == model2.Password &&
                model1.PasswordHashed == model2.PasswordHashed && model1.Phones == model2.Phones &&
                model1.RealAddress == model2.RealAddress && model1.RealRegionId == model2.RealRegionId &&
                model1.RegAuthoritiesId == model2.RegAuthoritiesId && model1.RegDate == model2.RegDate &&
                model1.RegExpirationDate == model2.RegExpirationDate && model1.RegionId == model2.RegionId &&
                model1.RegNumber == model2.RegNumber && model1.RegSeries == model2.RegSeries &&
                model1.RegTypeId == model2.RegTypeId && model1.RegСarDate == model2.RegСarDate &&
                model1.ShortName == model2.ShortName && model1.SubjectId == model2.SubjectId &&
                model1.Surname == model2.Surname && model1.UsageBasisId == model2.UsageBasisId &&
                model1.UsageExpireDate == model2.UsageExpireDate && model1.ExperienceDate == model2.ExperienceDate && 
                model1.UsageBasisId == model2.UsageBasisId)
            {
                return true;
            }

            if (model1.Id == model2.Id | model1.Guid != model2.Guid | model1.BaseAddress != model2.BaseAddress |
                model1.BrandName != model2.BrandName | model1.BaseRegionId != model2.BaseRegionId |
                model1.CarrierCategoryId != model2.CarrierCategoryId |
                model1.CarrierStateId != model2.CarrierStateId | model1.Comment != model2.Comment |
                model1.DepartmentId != model2.DepartmentId | model1.Education != model2.Education |
                model1.Email != model2.Email | model1.Experience != model2.Experience |
                model1.HeadName != model2.HeadName | model1.HeadPosition != model2.HeadPosition |
                model1.INN != model2.INN | model1.LegalAddress != model2.LegalAddress |
                model1.LegalRegionId != model2.LegalRegionId | model1.Lessor != model2.Lessor |
                model1.Login != model2.Login | model1.MiddleName != model2.MiddleName | model1.Name != model2.Name |
                model1.NameLegal != model2.NameLegal | model1.OrgStructureId != model2.OrgStructureId |
                model1.OwnershipTypeId != model2.OwnershipTypeId | model1.PassportDate != model2.PassportDate |
                model1.PassportEnd != model2.PassportEnd | model1.PassportIssuedBy != model2.PassportIssuedBy |
                model1.PassportNum != model2.PassportNum | model1.Password != model2.Password |
                model1.PasswordHashed != model2.PasswordHashed | model1.Phones != model2.Phones |
                model1.RealAddress != model2.RealAddress | model1.RealRegionId != model2.RealRegionId |
                model1.RegAuthoritiesId != model2.RegAuthoritiesId | model1.RegDate != model2.RegDate |
                model1.RegExpirationDate != model2.RegExpirationDate | model1.RegionId != model2.RegionId |
                model1.RegNumber != model2.RegNumber | model1.RegSeries != model2.RegSeries |
                model1.RegTypeId != model2.RegTypeId | model1.RegСarDate != model2.RegСarDate |
                model1.ShortName != model2.ShortName | model1.SubjectId != model2.SubjectId |
                model1.Surname != model2.Surname | model1.UsageBasisId != model2.UsageBasisId |
                model1.UsageExpireDate != model2.UsageExpireDate | model1.ExperienceDate != model2.ExperienceDate | 
                model1.UsageBasisId != model2.UsageBasisId)
            {
                model2.Action = ActionType.Update;
            }

            return false;
        }
        
}