using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain.Ekm;
using AisReference.Domain.Eleed;

namespace AisReference.Domain.Registry
{
    //997
    //АТС
    public class RegistryCarrierVehicle : IEqualityComparer<RegistryCarrierVehicle>
    {
        [Key] 
        public long Id { get; set; }
        public Guid Guid { get; set; } = new Guid(); //UID
        
        public long? ParentId { get; set; } //ID_PARENT
        [ForeignKey("ParentId")] 
        public RegistryCarrier? Parent { get; set; } //ID_PARENT
        
        public Guid? ApplicationUID { get; set; } = new Guid(); //F522587
        
        public long? UsageBasisId { get; set; } //F81374 ПТБ АТЕ
        [ForeignKey("UsageBasisId")] 
        public RefUsageBasis? UsageBasis { get; set; }

        public long? VehicleId { get; set; } //F81373 АТС
        [ForeignKey("VehicleId")] 
        public RegistryVehicle? Vehicle { get; set; }
        
        [NotMapped] 
        public string Action { get; set; } = ActionType.Create;


        public int GetHashCode(RegistryCarrierVehicle model)
        {
            return model.Id.GetHashCode();
        }

        public bool Equals(RegistryCarrierVehicle model1, RegistryCarrierVehicle model2)
        {
            if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.ParentId == model2.ParentId &&
                model1.ApplicationUID == model2.ApplicationUID && model1.UsageBasisId == model2.UsageBasisId &&
                model1.VehicleId == model2.VehicleId
               )
            {
                return true;
            }

            if (model1.Id == model2.Id | model1.Guid != model2.Guid | model1.ParentId != model2.ParentId |
                model1.ApplicationUID != model2.ApplicationUID | model1.UsageBasisId != model2.UsageBasisId |
                model1.VehicleId != model2.VehicleId
               )
            {
                model2.Action = ActionType.Update;
            }

            return false;
        }
    }
}