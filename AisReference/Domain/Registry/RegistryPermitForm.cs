﻿using AisReference.Domain.Eleed;
using AisReference.Domain.Users;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace AisReference.Domain.Registry
{
    public class RegistryPermitForm: IEqualityComparer<RegistryPermitForm>
    {
        [Key]
        public Guid Guid { get; set; }
        public long Id { get; set; }

        public Guid? PermitFormReceivedGuid { get; set; }
        
        public int? StatusId { get; set; }
        [ForeignKey("StatusId")]
        public RefPermitStatus? Status { get; set; } //Текущий статус

        public long? ArchiveStatusId { get; set; }

        public long? CountryId { get; set; }
        [ForeignKey("CountryId")]
        public RefCountry? Country { get; set; }

        public long? DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public RefDepartment? Department { get; set; }

        public long? CarrierId { get; set; }
        [ForeignKey("CarrierId")]
        public RegistryCarrier? Carrier { get; set; }

        public long? VehicleId { get; set; }
        [ForeignKey("VehicleId")]
        public RegistryVehicle? Vehicle { get; set; }

        public long? FormTypeId { get; set; }
        [ForeignKey("FormTypeId")]
        public RefFormRequestType? FormType { get; set; }

        public long? RestrictionId { get; set; }
        [ForeignKey("RestrictionId")]
        public RefEuroLimit? Restriction { get; set; }

        public DateTime? InvoiceDate { get; set; }   //Дата накладной
        public DateTime? ExpiryDate { get; set; }   //Дата окончания действия
        public DateTime? IssueDate { get; set; }   //Дата выдачи
        public DateTime? ReceiptDate { get; set; } //Дата поступления  
        public DateTime? ReturnDate { get; set; }  //Дата Возврата или Аннулирования 
        public DateTime? ReturnDeadline { get; set; }  //Дата возврата 
        public DateTime? TransferedDate { get; set; } = null;

        public string? Number { get; set; }
        public int? NumberInt { get; set; }
        public string? Series { get; set; }


        public virtual Users.User? CreatedBy { get; set; }
        public virtual Users.User? UpdatedBy { get; set; }

        [DefaultValue(typeof(DateTime), "CURRENT_TIMESTAMP")]
        public virtual DateTime CreatedDate { get; set; } = DateTime.Now;

        [DefaultValue(typeof(DateTime), "CURRENT_TIMESTAMP")]
        public virtual DateTime? UpdatedDate { get; set; } = DateTime.Now;

        public virtual DateTime? DeletedDate { get; set; } = null;


        [NotMapped]
        public string Action { get; set; } = ActionType.Create;


        public int GetHashCode(RegistryPermitForm model)
        {
            return model.Id.GetHashCode();
        }

        public bool Equals(RegistryPermitForm model1, RegistryPermitForm model2)
        {
            if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.PermitFormReceivedGuid == model2.PermitFormReceivedGuid &&
                model1.StatusId == model2.StatusId && model1.ArchiveStatusId == model2.ArchiveStatusId &&
                model1.CountryId == model2.CountryId && model1.DepartmentId == model2.DepartmentId &&
                model1.CarrierId == model2.CarrierId && model1.VehicleId == model2.VehicleId &&
                model1.FormTypeId == model2.FormTypeId && model1.RestrictionId == model2.RestrictionId &&
                model1.InvoiceDate == model2.InvoiceDate && model1.ExpiryDate == model2.ExpiryDate &&
                model1.IssueDate == model2.IssueDate && model1.ReceiptDate == model2.ReceiptDate &&
                model1.ReturnDate == model2.ReturnDate && model1.ReturnDeadline == model2.ReturnDeadline &&
                model1.TransferedDate == model2.TransferedDate && model1.Number == model2.Number &&
                model1.NumberInt == model2.NumberInt && model1.Series == model2.Series)
            {
                return true;
            }

            if (model1.Guid == model2.Guid | model1.PermitFormReceivedGuid != model2.PermitFormReceivedGuid |
                model1.StatusId != model2.StatusId | model1.ArchiveStatusId != model2.ArchiveStatusId |
                model1.CountryId != model2.CountryId | model1.DepartmentId != model2.DepartmentId |
                model1.CarrierId != model2.CarrierId | model1.VehicleId != model2.VehicleId |
                model1.FormTypeId != model2.FormTypeId | model1.RestrictionId != model2.RestrictionId |
                model1.InvoiceDate != model2.InvoiceDate | model1.ExpiryDate != model2.ExpiryDate |
                model1.IssueDate != model2.IssueDate | model1.ReceiptDate != model2.ReceiptDate |
                model1.ReturnDate != model2.ReturnDate | model1.ReturnDeadline != model2.ReturnDeadline |
                model1.TransferedDate != model2.TransferedDate | model1.Number != model2.Number |
                model1.NumberInt != model2.NumberInt | model1.Series != model2.Series)
            {
                model2.Action = ActionType.Update;
            };

            return false;
        }
    }
}
