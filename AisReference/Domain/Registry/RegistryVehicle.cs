using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain.Ekm;
using AisReference.Domain.Eleed;

namespace AisReference.Domain.Registry;

//A851
//Автотранспортные средства
public class RegistryVehicle  : IEqualityComparer<RegistryVehicle>
{
    [Key]
    public long Id { get; set; }
    public Guid Guid { get; set; }
    
    public int? State { get; set; }
    
    public long? VehicleBrandId { get; set; }//F80292 Марка 
    [ForeignKey("VehicleBrandId")]
    public virtual RefVehicleBrand? Brand { get; set; }


    public Guid? BrandSpelling { get; set; } //F311350 Написание Марки
    public bool? IsTrailer { get; set; } = false; //F311320 Прицеп 
    public string? Model { get; set; } //F80293 Модель 
    public string? Name { get; set; } //F392564 Название судна 

    
    public long? RegisteredCountryId { get; set; }//F81194 Страна регистраци 
    [ForeignKey("RegisteredCountryId")]
    public virtual RefCountry? RegisteredCountry { get; set; }
    
    public long? Scan { get; set; }//F81421 Фото 
    public string? StateNumber { get; set; }//F80291 Государственный номер АТС  Vehicle
    
    public long? VehicleKindId { get; set; }//F81540 Вид АТС 
    [ForeignKey("VehicleKindId")]
    public virtual RefVehicleKind? VehicleKind { get; set; }
    
    public string? VIN { get; set; } //F81193 VID код 
    public bool? WaterTransport { get; set; } = false; //F392563 Водный транспорт
    public int? YearRelease { get; set; } //F80295 Год выпуска 
    
    [NotMapped]
    public string? BrandSpellingText { get; set; }

    
    [NotMapped]
    public string Action { get; set; } = ActionType.Create;
    
    public int GetHashCode(RegistryVehicle model)
    {
        return model.Id.GetHashCode();
    }
    
    public bool Equals(RegistryVehicle model1, RegistryVehicle model2)
    {
        if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.State == model2.State &&
            model1.VehicleBrandId == model2.VehicleBrandId &&
            model1.BrandSpelling == model2.BrandSpelling && model1.IsTrailer == model2.IsTrailer &&
            model1.Model == model2.Model && model1.Name == model2.Name &&
            model1.RegisteredCountryId == model2.RegisteredCountryId && model1.Scan == model2.Scan &&
            model1.StateNumber == model2.StateNumber && model1.VehicleKindId == model2.VehicleKindId &&
            model1.VIN == model2.VIN && model1.WaterTransport == model2.WaterTransport &&
            model1.YearRelease == model2.YearRelease
           )
        {
            return true;
        }
        if (model1.Id == model2.Id | model1.Guid != model2.Guid | model1.State != model2.State |
            model1.VehicleBrandId != model2.VehicleBrandId |
            model1.BrandSpelling != model2.BrandSpelling | model1.IsTrailer != model2.IsTrailer |
            model1.Model != model2.Model | model1.Name != model2.Name |
            model1.RegisteredCountryId != model2.RegisteredCountryId | model1.Scan != model2.Scan |
            model1.StateNumber != model2.StateNumber | model1.VehicleKindId != model2.VehicleKindId |
            model1.VIN != model2.VIN | model1.WaterTransport != model2.WaterTransport |
            model1.YearRelease != model2.YearRelease
           )
        {
            model2.Action = ActionType.Update;
        }

        return false;
    }
}