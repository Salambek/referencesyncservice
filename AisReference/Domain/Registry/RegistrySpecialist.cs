using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain.Ekm;
using AisReference.Domain.Eleed;

namespace AisReference.Domain.Registry;

//A852
//Реестр Ответственные специалисты
public class RegistrySpecialist : IEqualityComparer<RegistrySpecialist>
{
    [Key] 
    public long Id { get; set; }
    public Guid Guid { get; set; } = new Guid();
    public DateTime? DateOfBirth { get; set; } //F81246 Дата рождения
    public DateTime? DrivingExperience { get; set; } //F331831 Водительский стаж с
    public string? INN { get; set; } //F80312 ИНН
    public string? MiddleName { get; set; } //F81245 Отчество
    public string? Name { get; set; } //F81244 Имя
    public string? Surname { get; set; } //F81243 Фамилия
    public long? OrgPattern { get; set; } //80297 OrgPattern
 
    [NotMapped] 
    public string Action { get; set; } = ActionType.Create;


    public int GetHashCode(RegistrySpecialist model)
    {
        return model.Id.GetHashCode();
    }

    public bool Equals(RegistrySpecialist model1, RegistrySpecialist model2)
    {
        if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.DateOfBirth == model2.DateOfBirth &&
            model1.DrivingExperience == model2.DrivingExperience && model1.INN == model2.INN &&
            model1.MiddleName == model2.MiddleName && model1.Name == model2.Name &&
            model1.Surname == model2.Surname
           )
        {
            return true;
        }
        if (model1.Id == model2.Id | model1.Guid != model2.Guid | model1.DateOfBirth != model2.DateOfBirth |
            model1.DrivingExperience != model2.DrivingExperience | model1.INN != model2.INN |
            model1.MiddleName != model2.MiddleName | model1.Name != model2.Name |
            model1.Surname != model2.Surname
           )
        {
            model2.Action = ActionType.Update;
        }

        return false;
    }
}