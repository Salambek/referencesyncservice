using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain.Ekm;
using AisReference.Domain.Eleed;

namespace AisReference.Domain.Registry
{
    //A996
    //Специалисты
    public class RegistryCarrierSpecialist : IEqualityComparer<RegistryCarrierSpecialist>
    {
        [Key] 
        public long Id { get; set; }
        public Guid Guid { get; set; } = new Guid(); //UID
        
        public long? ParentId { get; set; } //ID_PARENT
        [ForeignKey("ParentId")] 
        public RegistryCarrier? Parent { get; set; } //ID_PARENT
        
        public Guid? ApplicationUID { get; set; } = new Guid(); //F522585
        
        public DateTime? DrivingExperience { get; set; } //F81371
       
        public long? PositionId { get; set; } //ID_PARENT
        [ForeignKey("PositionId")] 
        public RefSpecialistPosition? Position { get; set; } //F81370
        public bool? ResponsibleForLicense { get; set; } //F81372
        
        public long? SpecialistId { get; set; } //F81369 Специалист
        [ForeignKey("SpecialistId")] 
        public RegistrySpecialist? Specialist { get; set; }
        
        [NotMapped] 
        public string Action { get; set; } = ActionType.Create;


        public int GetHashCode(RegistryCarrierSpecialist model)
        {
            return model.Id.GetHashCode();
        }

        public bool Equals(RegistryCarrierSpecialist model1, RegistryCarrierSpecialist model2)
        {
            if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.ParentId == model2.ParentId &&
                model1.ApplicationUID == model2.ApplicationUID && model1.DrivingExperience == model2.DrivingExperience &&
                model1.PositionId == model2.PositionId && model1.ResponsibleForLicense == model2.ResponsibleForLicense &&
                model1.SpecialistId == model2.SpecialistId
               )
            {
                return true;
            }

            if (model1.Id == model2.Id | model1.Guid != model2.Guid | model1.ParentId != model2.ParentId |
                model1.ApplicationUID != model2.ApplicationUID | model1.DrivingExperience != model2.DrivingExperience |
                model1.PositionId != model2.PositionId | model1.ResponsibleForLicense != model2.ResponsibleForLicense |
                model1.SpecialistId != model2.SpecialistId
               )
            {
                model2.Action = ActionType.Update;
            }

            return false;
        }
    }
    
}