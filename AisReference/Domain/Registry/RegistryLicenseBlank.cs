
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using AisReference.Domain.Registry;
using AisReference.Domain.Eleed;
using AisReference.Domain;

namespace MtPermitsBackend.Domain.Registry;

// ������ ������ ��������
// A979
public class RegistryLicenseBlank : IEqualityComparer<RegistryLicenseBlank>
{
    [Key]
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public long? DocId { get; set; }
    public DateTime? FormValidityEnd { get; set; } //81921
    public DateTime? FormValidityStart { get; set; } //81920
    public bool? IsCancelled { get; set; } // 81896
    public long? LicenseId { get; set; } // 81226
    [ForeignKey("LicenseId")]
    public virtual RegistryLicense? License { get; set; } 
    public DateTime? LicFormIssuedDate { get; set; } // 81228
    public string? LicFormLicNumber { get; set; } //331710
    public string? LicFormNumber { get; set; } //81225
    public bool? MainForm {  get; set; } // 290963
    public string? Receipt { get; set; } //81759
    public decimal? ReceiptBalance { get; set; } //81760
    public decimal? SumToPay { get; set; } // 81758
    public long? DepartmentId { get; set; }  // 81227
    [ForeignKey("DepartmentId")]
    public virtual RefDepartment? Department { get; set; } 
    public long? TrailerId { get; set; }  //311344
    [ForeignKey("TrailerId")]
    public virtual RegistryVehicle? Trailer { get; set; }
    public long? VehicleId { get; set; }  //81255
    [ForeignKey("VehicleId")]
    public virtual RegistryVehicle? Vehicle { get; set; }

    [NotMapped] public string Action { get; set; } = ActionType.Create;


    public int GetHashCode(RegistryLicenseBlank model)
    {
        return model.Id.GetHashCode();
    }

    public bool Equals(RegistryLicenseBlank model1, RegistryLicenseBlank model2)
    {
        if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.LicenseId == model2.LicenseId &&
            model1.LicFormIssuedDate == model2.LicFormIssuedDate && model1.LicFormLicNumber == model2.LicFormLicNumber &&
            model1.LicFormNumber == model2.LicFormNumber && model1.FormValidityEnd == model2.FormValidityEnd &&
            model1.FormValidityStart == model2.FormValidityStart && model1.MainForm == model2.MainForm &&
            model1.IsCancelled == model2.IsCancelled && model1.ReceiptBalance == model2.ReceiptBalance &&
            model1.DepartmentId == model2.DepartmentId && model1.SumToPay == model2.SumToPay &&
            model1.Receipt == model2.Receipt && model1.TrailerId == model2.TrailerId &&
            model1.VehicleId == model2.VehicleId)
        {
            return true;
        }
        if (model1.Id != model2.Id | model1.Guid != model2.Guid | model1.LicenseId == model2.LicenseId |
            model1.LicFormIssuedDate != model2.LicFormIssuedDate | model1.FormValidityEnd != model2.FormValidityEnd |
            model1.FormValidityStart != model2.FormValidityStart | model1.IsCancelled != model2.IsCancelled |
            model1.LicFormLicNumber != model2.LicFormLicNumber | model1.LicFormNumber != model2.LicFormNumber |
            model1.MainForm != model2.MainForm | model1.Receipt != model2.Receipt |
            model1.DepartmentId != model2.DepartmentId | model1.ReceiptBalance != model2.ReceiptBalance |
            model1.SumToPay != model2.SumToPay | model1.TrailerId != model2.TrailerId |
            model1.VehicleId != model2.VehicleId)
        {
            model2.Action = ActionType.Update;
        }
        return false;

    }
}