using AisReference.Domain.Eleed;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AisReference.Domain.Registry;

//Реестр лицензий
//A975
public class RegistryLicense  : IEqualityComparer<RegistryLicense>
{
    [Key]
    public long Id { get; set; }
    public Guid Guid { get; set; }
    public string? License { get; set; } //F81199
    
    public string? LicJustification { get; set; } //F81587
    public DateTime? LicIssuedDate { get; set; } //F81203
    public DateTime? LicValidityEnd { get; set; } //F81205
    public DateTime? LicValidityStart { get; set; } //F81204
    public DateTime? SuspendDateEnd { get; set; } //F81585
    
    public int? NumSpec { get; set; } //F372430
    public string? OldLicense { get; set; } //F331412
    
    public long? CoveredTerId { get; set; }  //F81202
    [ForeignKey("CoveredTerId")] 
    public virtual RefCoveredTer? CoveredTer { get; set; }
    
    public long? DepartmentId { get; set; }  //F81200
    [ForeignKey("DepartmentId")] 
    public virtual RefDepartment? Department { get; set; }
    
    public long? DocReasonId { get; set; }  //F81586
    //[ForeignKey("DocReasonId")] 
    //public virtual License? DocReason { get; set; }
    
    public long? CarrierId { get; set; }  //F81242
    [ForeignKey("CarrierId")] 
    public virtual RegistryCarrier? Carrier { get; set; }
    
    public long? ParentId { get; set; }  //F81786
    [ForeignKey("ParentId")] 
    public virtual RegistryLicense? Parent { get; set; }

    public long? LicenseStateId { get; set; }
    [ForeignKey("LicenseStateId")]
    public RefLicenseState? LicenseState { get; set; } //F81208
    
    public long? LicenseTypeId { get; set; } //F81201
    [ForeignKey("LicenseTypeId")]
    public RefActivityType? LicenseType { get; set; }
    
    public bool? IsDeleted { get; set; } = false;


    [NotMapped] public string Action { get; set; } = ActionType.Create;
    
    
    public int GetHashCode(RegistryLicense model)
    {
        return model.Id.GetHashCode();
    }

    public bool Equals(RegistryLicense model1, RegistryLicense model2)
    {
        if (model1.Id == model2.Id && model1.Guid == model2.Guid && model1.License == model2.License &&
            model1.LicJustification == model2.LicJustification && model1.LicIssuedDate == model2.LicIssuedDate &&
            model1.LicValidityEnd == model2.LicValidityEnd && model1.LicValidityStart == model2.LicValidityStart &&
            model1.SuspendDateEnd == model2.SuspendDateEnd && model1.NumSpec == model2.NumSpec &&
            model1.OldLicense == model2.OldLicense && model1.CoveredTerId == model2.CoveredTerId &&
            model1.DepartmentId == model2.DepartmentId && model1.DocReasonId == model2.DocReasonId &&
            model1.CarrierId == model2.CarrierId && model1.ParentId == model2.ParentId &&
            model1.LicenseStateId == model2.LicenseStateId && model1.LicenseTypeId == model2.LicenseTypeId)
        {
            return true;
        }
        if (model1.Id != model2.Id | model1.Guid != model2.Guid | model1.License == model2.License |
            model1.LicJustification != model2.LicJustification | model1.LicIssuedDate != model2.LicIssuedDate |
            model1.LicValidityEnd != model2.LicValidityEnd | model1.LicValidityStart != model2.LicValidityStart |
            model1.SuspendDateEnd != model2.SuspendDateEnd | model1.NumSpec != model2.NumSpec |
            model1.OldLicense != model2.OldLicense | model1.CoveredTerId != model2.CoveredTerId |
            model1.DepartmentId != model2.DepartmentId | model1.DocReasonId != model2.DocReasonId |
            model1.CarrierId != model2.CarrierId | model1.ParentId != model2.ParentId |
            model1.LicenseStateId != model2.LicenseStateId | model1.LicenseTypeId != model2.LicenseTypeId)
        {
            model2.Action = ActionType.Update;
        }
        return false;

    }
}