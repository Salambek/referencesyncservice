using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain
{
    public class LogType : BaseReferenceEntity
    {
        public const int UserLogin = 1;
        public const int UserLogout = 2;
        public const int RefreshToken = 3;
        public const int StatusChange = 4;

        public const int PostCategoryCreate = 5;
        public const int PostCategoryUpdate = 6;
        public const int PostCategoryDelete = 7;

        public const int PostCreate = 8;
        public const int PostUpdate = 9;
        public const int PostDelete = 10;

        public const int PermitFormReceivedCreate = 11;
        public const int PermitFormReceivedUpdate = 12;
        public const int PermitFormReceivedDelete = 13;

        public const int PermitStatusCreate = 14;
        public const int PermitStatusUpdate = 15;
        public const int PermitStatusDelete = 16;

        public const int LicenseFormCreate = 17;
        public const int LicenseFormUpdate = 18;
        public const int LicenseFormDelete = 19;

        public const int LicenseBlankCreate = 20;
        public const int LicenseBlankUpdate = 21;
        public const int LicenseBlankDelete = 22;

        public const int RulingProtocolCreate = 23;
        public const int RulingProtocolUpdate = 24;
        public const int RulingProtocolDelete = 25;
        public const int RulingProtocolSwapSpecialist = 31;

        public const int RulingProtocolCommissionUpdate = 26;

        public const int PermitBlankSampleCreate = 27;
        public const int PermitBlankSampleUpdate = 28;
        public const int PermitBlankSampleDelete = 29;



        public const int TundukRequest = 30;



        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }
    }
}