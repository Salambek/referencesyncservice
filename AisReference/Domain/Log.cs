﻿using AisReference.Domain;
using AisReference.Domain.Users;
using System.ComponentModel.DataAnnotations;

namespace AisReference.Domain
{
    public class Log
    {
        [Key]
        public virtual Guid Guid { get; set; }
        public virtual LogType? LogType { get; set; }
        public virtual DateTime? ActionDate { get; set; } = DateTime.Now;
        public virtual string UserIP { get; set; }
        public virtual bool? Success { get; set; }
        public virtual string? ErrorMessage { get; set; }
        public virtual User? User { get; set; }
        public virtual string? Model { get; set; }
    }

}
