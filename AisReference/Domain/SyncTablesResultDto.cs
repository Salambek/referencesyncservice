namespace AisReference.Domain;

public class SyncTablesResultDto
{
    public object SyncTableData { get; set; }

    public SyncTablesResultDto( object syncTableData)
    {
        this.SyncTableData = syncTableData;
    }
}