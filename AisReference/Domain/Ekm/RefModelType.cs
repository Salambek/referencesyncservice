﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using AisReference.Domain;

namespace AisReference.Domain.Ekm;

public class RefModelType : BaseReferenceEntity, IEqualityComparer<RefModelType>
{
    public const long Application = 1;
    public const long AppApplicant = 2;
    public const long AppSpecialist = 3;
    public const long AppVehicle = 4;
    
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long? Id { get; set; }
    
    [MaxLength(100)]
    //[Display(Name = "Model Name")]
    [DisplayName("Название модели")]
    public string? ModelName { get; set; }
    
    
    [MaxLength(100)]
    [Display(Name = "Url Api для фронта")]
    public string? FrontApiUrl { get; set; }
    [Display(Name = "Обновлять с фронта")]
    public bool? UpdateWithFront { get; set; }
    [NotMapped] public string Action { get; set; } = ActionType.Create;

    public int GetHashCode(RefModelType model)
    {
        return model.Id.GetHashCode();
    }
    public bool Equals(RefModelType model1, RefModelType model2)
    {
        if (model1.Id == model2.Id && model1.ModelName == model2.ModelName && model1.FrontApiUrl == model2.FrontApiUrl && model1.UpdateWithFront == model2.UpdateWithFront
            && model1.Name == model2.Name && model1.NameKy == model2.NameKy && model1.NameRu == model2.NameRu && model1.CreatedDate == model2.CreatedDate
            && model1.UpdatedDate == model2.UpdatedDate && model1.IsActive == model2.IsActive && model1.DeletedDate == model2.DeletedDate)
        {
            return true;
        }
        if (model1.Id != model2.Id | model1.ModelName != model2.ModelName | model1.FrontApiUrl != model2.FrontApiUrl | model1.UpdateWithFront != model2.UpdateWithFront
            | model1.Name != model2.Name | model1.NameKy != model2.NameKy | model1.NameRu != model2.NameRu | model1.CreatedDate != model2.CreatedDate
            | model1.UpdatedDate != model2.UpdatedDate | model1.IsActive != model2.IsActive | model1.DeletedDate != model2.DeletedDate)
        {
            model2.Action = ActionType.Update;
        }
        return false;
    }

    
}