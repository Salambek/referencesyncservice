using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Ekm;

public class RefLotStatus : BaseReferenceEntity, IEqualityComparer<RefLotStatus>
{
    public const int Created = 1;   //Создан (но не опубликован);
    public const int Published = 2; //Опубликован (идет прием заявок);
    public const int Checking = 3;  //Рассматривается (на рассмотрении тендерной комиссией)
    public const int Finished = 4;  //Завершен (объявлен победитель)
    public const int Canceled = 5;  //Отменен (признан несостоявшимся)
    
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long Id { get; set; }

    public int GetHashCode(RefLotStatus model)
    {
        return model.Id.GetHashCode();
    }
    [NotMapped] public string Action { get; set; } = ActionType.Create;


    public bool Equals(RefLotStatus model1, RefLotStatus model2)
    {
        if (model1.Id == model2.Id
            && model1.Name == model2.Name && model1.NameKy == model2.NameKy && model1.NameRu == model2.NameRu && model1.CreatedDate == model2.CreatedDate
            && model1.UpdatedDate == model2.UpdatedDate && model1.IsActive == model2.IsActive && model1.DeletedDate == model2.DeletedDate)
        {
            return true;
        }
        if (model1.Id != model2.Id
            | model1.Name != model2.Name | model1.NameKy != model2.NameKy | model1.NameRu != model2.NameRu | model1.CreatedDate != model2.CreatedDate
            | model1.UpdatedDate != model2.UpdatedDate | model1.IsActive != model2.IsActive | model1.DeletedDate != model2.DeletedDate)
        {
            model2.Action = ActionType.Update;
        }
        return false;
    }
}