using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AisReference.Domain.Ekm;

public class RefApplicationStatus : BaseReferenceEntity, IEqualityComparer<RefApplicationStatus>
{
    public const int NewReceived = 1;

    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "�������������")]
    public int Id { get; set; }
    [NotMapped] public string Action { get; set; } = ActionType.Create;

    public int GetHashCode(RefApplicationStatus model)
    {
        return model.Id.GetHashCode();
    }
    public bool Equals(RefApplicationStatus model1, RefApplicationStatus model2)
    {
        if (model1.Id == model2.Id 
            && model1.Name == model2.Name && model1.NameKy == model2.NameKy && model1.NameRu == model2.NameRu && model1.CreatedDate == model2.CreatedDate
            && model1.UpdatedDate == model2.UpdatedDate && model1.IsActive == model2.IsActive && model1.DeletedDate == model2.DeletedDate)
        {
            return true;
        }
        if (model1.Id != model2.Id 
            | model1.Name != model2.Name | model1.NameKy != model2.NameKy | model1.NameRu != model2.NameRu | model1.CreatedDate != model2.CreatedDate
            | model1.UpdatedDate != model2.UpdatedDate | model1.IsActive != model2.IsActive | model1.DeletedDate != model2.DeletedDate)
        {
            model2.Action = ActionType.Update;
        }
        return false;
    }
}