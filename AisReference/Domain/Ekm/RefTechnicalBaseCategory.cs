using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using AisReference.Domain;

namespace AisReference.Domain.Ekm;

//Категория производственно-технической базы
public class RefTechnicalBaseCategory : BaseReferenceEntity, IEqualityComparer<RefTechnicalBaseCategory>
{

    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }


    [Display(AutoGenerateField = false)]
    public long? ParentCategoryId { get; set; }
    [ForeignKey("ParentCategoryId")]
    public RefTechnicalBaseCategory? ParentCategory { get; set; } //351906
    public bool? IsRated { get; set; } = true;
    [NotMapped] public string Action { get; set; } = ActionType.Create;

    public bool Equals(RefTechnicalBaseCategory model1, RefTechnicalBaseCategory model2)
    {
        if (model1.Id == model2.Id && model1.Name == model2.Name && model1.IsRated == model2.IsRated)
        {
            return true;
        }

        if (model1.Id == model2.Id | model1.Name != model2.Name | model1.IsRated != model2.IsRated)
        {
            model2.Action = ActionType.Update;

        }

        return false;
    }

    public int GetHashCode([DisallowNull] RefTechnicalBaseCategory model)
    {
        return model.Id.GetHashCode();
    }

    public override string ToString()
    {
        return Name + "";
    }

}