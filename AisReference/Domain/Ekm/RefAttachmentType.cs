﻿
using AisReference.Domain.Registry;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AisReference.Domain.Ekm;

public class RefAttachmentType : BaseReferenceEntity, IEqualityComparer<RefAttachmentType>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public long? Id { get; set; }
    [Display(Name = "Физическое лицо")]
    public bool IsIndividual { get; set; } = true;
    public bool IsLegal { get; set; } = true;
    [NotMapped] public string Action { get; set; } = ActionType.Create;

    public int GetHashCode(RefAttachmentType model)
    {
        return model.Id.GetHashCode();
    }
    public bool Equals(RefAttachmentType model1, RefAttachmentType model2)
    {
        if (model1.Id == model2.Id && model1.IsIndividual == model2.IsIndividual && model1.IsLegal == model2.IsLegal
            && model1.Name == model2.Name && model1.NameKy == model2.NameKy && model1.NameRu == model2.NameRu && model1.CreatedDate == model2.CreatedDate 
            && model1.UpdatedDate == model2.UpdatedDate && model1.IsActive == model2.IsActive && model1.DeletedDate == model2.DeletedDate)
        {
            return true;
        }
        if (model1.Id != model2.Id | model1.IsIndividual != model2.IsIndividual | model1.IsLegal != model2.IsLegal 
            | model1.Name != model2.Name | model1.NameKy != model2.NameKy | model1.NameRu != model2.NameRu | model1.CreatedDate != model2.CreatedDate
            | model1.UpdatedDate != model2.UpdatedDate | model1.IsActive != model2.IsActive | model1.DeletedDate != model2.DeletedDate)
        {
            model2.Action = ActionType.Update;
        }
        return false;
    }

}