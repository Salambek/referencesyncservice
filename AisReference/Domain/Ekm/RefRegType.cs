using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Ekm;

public class RefRegType : BaseReferenceEntity, IEqualityComparer<RefRegType>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "�������������")]
    public long Id { get; set; }
    
    [NotMapped]
    public virtual string Action { get; set; } = ActionType.Create;
    
    public virtual int GetHashCode(RefRegType regType)
    {
        return regType.Id.GetHashCode();
    }
    
    public virtual bool Equals(RefRegType regType1, RefRegType regType2)
    {
        if (regType1.Id == regType2.Id && 
            regType1.Name == regType2.Name)
        {
            return true;
        }

        if (regType1.Id == regType2.Id | regType1 != regType2)
        {
            regType2.Action = ActionType.Update;
        }

        return false;
    }
}