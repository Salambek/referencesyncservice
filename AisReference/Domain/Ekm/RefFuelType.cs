using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AisReference.Domain;

namespace AisReference.Domain.Ekm;

public class RefFuelType : BaseReferenceEntity, IEqualityComparer<RefFuelType>
{
    public const int Diesel = 1;  //Дизель
    public const int Gas = 2;   //Газ
    public const int Electro = 3; //Электро
    public const int Petrol = 4;  //Бензин

    
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Display(Name = "Идентификатор")]
    public int Id { get; set; }
    [NotMapped] public string Action { get; set; } = ActionType.Create;

    public int GetHashCode(RefFuelType model)
    {
        return model.Id.GetHashCode();
    }

    public bool Equals(RefFuelType model1, RefFuelType model2)
    {
        if (model1.Id == model2.Id
            && model1.Name == model2.Name && model1.NameKy == model2.NameKy && model1.NameRu == model2.NameRu && model1.CreatedDate == model2.CreatedDate
            && model1.UpdatedDate == model2.UpdatedDate && model1.IsActive == model2.IsActive && model1.DeletedDate == model2.DeletedDate)
        {
            return true;
        }
        if (model1.Id != model2.Id
            | model1.Name != model2.Name | model1.NameKy != model2.NameKy | model1.NameRu != model2.NameRu | model1.CreatedDate != model2.CreatedDate
            | model1.UpdatedDate != model2.UpdatedDate | model1.IsActive != model2.IsActive | model1.DeletedDate != model2.DeletedDate)
        {
            model2.Action = ActionType.Update;
        }
        return false;
    }
}