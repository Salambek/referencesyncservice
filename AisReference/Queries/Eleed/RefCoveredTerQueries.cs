﻿namespace AisReference.Queries.Eleed
{
    public class RefCoveredTerQueries
    {
        public readonly string CoveredTerEleedSelectQuery =
           "SELECT id, uid as Guid, F80185 as Code, F80182 as NameKyr, F80183 as NameRus, F80909 as InValid FROM A832";

        public readonly string CoveredTerEleedToDefaultSelectQuery =
            "SELECT id, guid, name_rus as NameRus, name_kyr as NameKyr, in_valid as InValid, code as Code FROM ref_covered_ters";

        public readonly string CoveredTerSelectQuery =
            "SELECT id, name_rus as NameRus, name_kyr as NameKyr, code as Code FROM ref_covered_ters";

        public readonly string CoveredTerSiteSelectQuery =
            "SELECT id, name_ru as NameRus, name_ky as NameKyr, code as Code FROM ref_covered_ters";

        public readonly string CoveredTerDefaultCreateQuery = @"
                                                    INSERT INTO ref_covered_ters(id, guid, name_rus, name_kyr, in_valid, code)
                                                    VALUES (@Id, @Guid, @NameRus, @NameKyr, @InValid, @Code);
                                                  ";

        public readonly string CoveredTerSiteCreateQuery = @"
                                                    INSERT INTO ref_covered_ters(id, name_ru, name_ky, code)
                                                    VALUES (@Id, @NameRus, @NameKyr, @Code);
                                                  ";

        public readonly string CoveredTerDefaultUpdateQuery = @"
                                                    UPDATE ref_covered_ters 
                                                        SET guid = @Guid,
                                                            name_rus = @NameRus,
                                                            name_kyr = @NameKyr,
                                                            in_valid = @InValid,
                                                            code = @Code
                                                    WHERE id = @Id;
                                                  ";

        public readonly string CoveredTerSiteUpdateQuery = @"
                                                    UPDATE ref_covered_ters 
                                                        SET name_ru = @NameRus,
                                                            name_ky = @NameKyr,
                                                            code = @Code
                                                    WHERE id = @Id;
                                                  ";

        public readonly string CoveredTerDeleteQuery = @"DELETE FROM ref_covered_ters  WHERE id = @Id;";

    }
}
