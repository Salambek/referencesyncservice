﻿namespace AisReference.Queries.Eleed
{
    public class RefCarrierStateQueries
    {
        public readonly string CarrierStateEleedSelectQuery =
            "SELECT id, F80730 as name FROM A922";

        public readonly string CarrierStateEleedToDefaultSelectQuery =
            "SELECT id, name as name FROM ref_carrier_states";

        public readonly string CarrierStateSelectQuery =
            "SELECT id, " +
             "name as Name, name_ky as NameKy, name_ru as NameRu, created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_carrier_states";

        public readonly string CarrierStateDefaultCreateQuery = @"
                                                    INSERT INTO ref_carrier_states(id, name, created_date, updated_date)
                                                    VALUES (@Id, @Name, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
                                                  ";

        public readonly string CarrierStateCreateQuery = @"
                                                    INSERT INTO ref_carrier_states(
                                                        id, 
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    );
                                                  ";

        public readonly string CarrierStateDefaultUpdateQuery = @"
                                                    UPDATE ref_carrier_states 
                                                        SET id = @Id
                                                            name = @Name 
                                                    WHERE id = @Id;
                                                  ";

        public readonly string CarrierStateUpdateQuery = @"
                                                    UPDATE ref_carrier_states 
                                                        SET id = @Id
                                                            name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate
                                                    WHERE id = @Id;
                                                  ";

        public readonly string CarrierStateDeleteQuery =
            @"DELETE FROM ref_carrier_states WHERE id = @Id;";

    }
}
