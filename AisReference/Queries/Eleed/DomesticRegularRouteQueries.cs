﻿namespace AisReference.Queries.Eleed
{
    public class DomesticRegularRouteQueries
    {
        public readonly string DomesticRegularRouteEleedSelectQuery =
            "SELECT id, F311017 AS BackTripDuration, F331593 as DepartmentId, F311010 as DepartureName, F311011 as DestinationId, F311018 as Interval, " +
            "F311015 as Length, F311008 as NameRoute, F331596 as NumberRoute, F331597 as RegDate, F331598 as RouteTypeId, F311016 as TripDuration, " +
            "F311014 as TripsQty, F331824 as VehicleKindId, F311013 as VehiclesQty, F311009 as DepartureId, " +
            "F311012 as DestinationName " +
            "FROM A919";

        public readonly string DomesticRegularRouteSelectQuery =
            "SELECT id, back_trip_duration AS BackTripDuration, department_id as DepartmentId, departure_name as DepartureName, destination_id as DestinationId, interval as Interval, " +
            "length as Length, name_route as NameRoute, number_route as NumberRoute, reg_date as RegDate, route_type_id as RouteTypeId, trip_duration as TripDuration, " +
            "trips_qty as TripsQty, vehicle_kind_id as VehicleKindId, vehicles_qty as VehiclesQty, departure_id as DepartureId, " +
            "name as Name, name_ky as NameKy, name_ru as NameRu, destination_name as DestinationName, " +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM domestic_regular_routes";

        public readonly string DomesticRegularRouteCreateQuery = @"
                                                    INSERT INTO domestic_regular_routes(
                                                        id, 
                                                        back_trip_duration,
                                                        department_id,
                                                        departure_name,
                                                        destination_id,
                                                        destination_name,
                                                        interval,
                                                        length,
                                                        name_route,
                                                        number_route,
                                                        reg_date,
                                                        route_type_id,
                                                        trip_duration,
                                                        trips_qty,
                                                        vehicle_kind_id,
                                                        vehicles_qty,
                                                        departure_id,
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @BackTripDuration,
                                                        @DepartmentId,
                                                        @DepartureName,
                                                        @DestinationId,
                                                        @DestinationName,
                                                        @Interval,
                                                        @Length,
                                                        @NameRoute,
                                                        @NumberRoute,
                                                        @RegDate,
                                                        @RouteTypeId,
                                                        @TripDuration,
                                                        @TripsQty,
                                                        @VehicleKindId,
                                                        @VehiclesQty,
                                                        @DepartureId,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";

        public readonly string DomesticRegularRouteUpdateQuery = @"
                                                    UPDATE domestic_regular_routes 
                                                        SET back_trip_duration = @BackTripDuration,
                                                            department_id = @DepartmentId,
                                                            departure_name = @DepartureName,
                                                            destination_id = @DestinationId,
                                                            destination_name = @DestinationName,
                                                            interval = @Interval,
                                                            length = @Length,
                                                            name_route = @NameRoute,
                                                            number_route = @NumberRoute,
                                                            reg_date = @RegDate,
                                                            route_type_id = @RouteTypeId,
                                                            trip_duration = @TripDuration,
                                                            trips_qty = @TripsQty,
                                                            vehicle_kind_id = @VehicleKindId,
                                                            vehicles_qty = @VehiclesQty,
                                                            departure_id = @DepartureId,
                                                            name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string DomesticRegularRouteDeleteQuery =
            @"DELETE FROM domestic_regular_routes WHERE id = @Id;";
    }
}