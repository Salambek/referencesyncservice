﻿namespace AisReference.Queries.Eleed
{
    public class RefOrgStructureQueries
    {
        public readonly string OrgStructureEleedSelectQuery =
            "SELECT id, F80919 as NameFullRu, F80717 as NameKy, F80717 as NameRu, F80717 as Name, F80720 as IsIndividual FROM A920";

        public readonly string OrgStructureEleedToDefaultSelectQuery =
            "SELECT id, name_full_ru as NameFullRu, name_ky as NameKy, name_ru as NameRu, name as Name, is_individual as IsIndividual  FROM ref_org_structures";

        public readonly string OrgStructureSelectQuery =
            "SELECT id, name_full_ru as NameFullRu, name_ky as NameKy, name_ru as NameRu, name as Name, is_individual as IsIndividual  FROM ref_org_structures";


        public readonly string OrgStructureDefaultCreateQuery = @"
                                                    INSERT INTO ref_org_structures(id, name_full_ru, name_ky, name_ru, name, is_individual, created_date, updated_date)
                                                    VALUES (@Id, @NameFullRu, @NameKy, @NameRu, @Name, @IsIndividual, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
                                                  ";

        public readonly string OrgStructureDefaultUpdateQuery = @"
                                                    UPDATE ref_org_structures 
                                                        SET name_full_ru = @NameFullRu,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            name = @Name,
                                                            is_individual = @IsIndividual,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate
                                                    WHERE id = @id;
                                                  ";

        public readonly string OrgStructureDeleteQuery =
            @"DELETE FROM ref_org_structures WHERE id = @Id;";

    }
}
