﻿namespace AisReference.Queries.Eleed
{
    public class RefCheckpointQueries
    {
        public readonly string CheckpointEleedSelectQuery =
            "SELECT id, uid as guid, F20058 as NameRus, F20059 as NameKyr, F20063 as Email, F20061 as Phone, F20213 as Number, " +
            " F20062 as Fax, F80553 as Geolocation, F20060 as Location, F80418 as CountryId, F331827 as  DepartureCountryId,  " +
            " F331828 as DestinationCountryId,  F792869 as DepartmentId, F81265 as ExternalCheckpoint, F372409 as ImageLogService, " +
            " F793455 as KGBorderCheckpoint, F80464 as ViolationCheckpoint, F351917 as RoadType, F792870 as PostId, " +
            " F792868 as UserId, F372428 as BoardIP1, F372429 as BoardIP2, F81294 as CameraIP1, F81295 as CameraIP2, " +
            " F792867 as ModifiedDate, F792866 as CreationDate, F81031 as  InValid FROM A214";

        public readonly string CheckpointSelectQuery =
            "SELECT id, guid, name_rus as NameRus, name_kyr as NameKyr, email as Email, phone as Phone, number as Number, fax as Fax," +
            " geolocation as Geolocation, location as Location, country_id as CountryId, departure_country_id as DestinationCountryId, " +
            " destination_country_id as DestinationCountryId, department_id as DepartmentId, external_checkpoint as ExternalCheckpoint, " +
            " image_log_service as ImageLogService, kgborder_checkpoint as KGBorderCheckpoint, violation_checkpoint as ViolationCheckpoint, " +
            " road_type as RoadType, post_id as PostId, user_id as UserId, board_ip1 as BoardIP1, board_ip2 as BoardIP2, camera_ip1 as CameraIP1, " +
            " camera_ip2 as CameraIP2, modified_date as ModifiedDate, creation_date as CreationDate, in_valid as InValid FROM ref_checkpoint";

        public readonly string CheckpointCreateQuery = @"
                                                    INSERT INTO ref_checkpoint(
                                                        id, 
                                                        guid, 
                                                        name_rus, 
                                                        name_kyr, 
                                                        email, 
                                                        phone, 
                                                        number, 
                                                        fax, 
                                                        geolocation, 
                                                        location, 
                                                        country_id, 
                                                        departure_country_id, 
                                                        destination_country_id, 
                                                        department_id, 
                                                        external_checkpoint, 
                                                        image_log_service, 
                                                        kgborder_checkpoint, 
                                                        violation_checkpoint, 
                                                        road_type, 
                                                        post_id, 
                                                        user_id, 
                                                        board_ip1, 
                                                        board_ip2, 
                                                        camera_ip1, 
                                                        camera_ip2, 
                                                        modified_date, 
                                                        creation_date, 
                                                        in_valid
                                                    )
                                                    VALUES (
                                                            @Id, 
                                                            @Guid, 
                                                            @NameRus, 
                                                            @NameKyr, 
                                                            @Email, 
                                                            @Phone, 
                                                            @Number, 
                                                            @Fax, 
                                                            @Geolocation, 
                                                            @Location, 
                                                            @CountryId, 
                                                            @DepartureCountryId, 
                                                            @DestinationCountryId, 
                                                            @DepartmentId, 
                                                            @ExternalCheckpoint, 
                                                            @ImageLogService, 
                                                            @KGBorderCheckpoint, 
                                                            @ViolationCheckpoint, 
                                                            @RoadType, 
                                                            @PostId, 
                                                            @UserId, 
                                                            @BoardIP1, 
                                                            @BoardIP2, 
                                                            @CameraIP1, 
                                                            @CameraIP2, 
                                                            @ModifiedDate, 
                                                            @CreationDate, 
                                                            @InValid
                                                        );
                                                  ";

        public readonly string CheckpointUpdateQuery = @"
                                                    UPDATE ref_checkpoint 
                                                        SET guid = @Guid
                                                            name_rus = @NameRus, 
                                                            name_kyr = @NameKyr, 
                                                            email = @Email, 
                                                            phone = @Phone, 
                                                            number = @Number, 
                                                            fax = @Fax, 
                                                            geolocation = @Geolocation, 
                                                            location = @Location, 
                                                            country_id = @CountryId, 
                                                            departure_country_id = @DepartureCountryId, 
                                                            destination_country_id = @DestinationCountryId, 
                                                            department_id = @DepartmentId, 
                                                            external_checkpoint = @ExternalCheckpoint, 
                                                            image_log_service = @ImageLogService, 
                                                            kgborder_checkpoint = @KGBorderCheckpoint, 
                                                            violation_checkpoint = @ViolationCheckpoint, 
                                                            road_type = @RoadType, 
                                                            post_id = @PostId, 
                                                            user_id = @UserId, 
                                                            board_ip1 = @BoardIP1, 
                                                            board_ip2 = @BoardIP2, 
                                                            camera_ip1 = @CameraIP1, 
                                                            camera_ip2 = @CameraIP2, 
                                                            modified_date = @ModifiedDate, 
                                                            creation_date = @CreationDate, 
                                                            in_valid = @InValid
                                                    WHERE id = @Id;
                                                  ";

        public readonly string CheckpointDeleteQuery = @"DELETE FROM ref_checkpoint WHERE id = @Id;";
    }
}
