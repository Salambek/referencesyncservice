﻿namespace AisReference.Queries.Eleed
{
    public class RefVehicleKindQueries
    {
        public readonly string VehicleKindEleedSelectQuery =
           "SELECT id, F20226 as Name, F20226 as NameRu, F20225 as NameKy, F311318 as Code, F81013 as IsActive, F351906 as VehicleTypeId, F311319 as IsTrailer, F392560 as IsWaterTransport FROM A243";

        public readonly string VehicleKindEleedToDefaultSelectQuery =
            "SELECT id, name, name_ru as NameRu, name_ky as NameKy, Code, is_active as IsActive, is_trailer as IsTrailer, vehicle_type_id as VehicleTypeId, is_water_transport as IsWaterTransport  FROM ref_vehicle_kinds";

        public readonly string VehicleKindSelectQuery =
            "SELECT id, name, name_ru as NameRu, name_ky as NameKy, Code, is_active as IsActive, is_trailer as IsTrailer, vehicle_type_id as VehicleTypeId, is_water_transport as IsWaterTransport, created_date As CreatedDate, updated_date as UpdatedDate, deleted_date as DeletedDate  FROM ref_vehicle_kinds";

        public readonly string VehicleKindCreateQuery = @"
                                                    INSERT INTO ref_vehicle_kinds(id, name, name_ru, name_ky, code, is_active, is_trailer, vehicle_type_id, is_water_transport, created_date, updated_date)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, @Code, @IsActive, @IsTrailer, @VehicleTypeId, @IsWaterTransport, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
                                                  ";

        public readonly string VehicleKindUpdateQuery = @"
                                                    UPDATE ref_vehicle_kinds 
                                                        SET name = @Name, code = @Code, is_active = @IsActive, is_trailer = @IsTrailer, vehicle_type_id = @VehicleTypeId, is_water_transport = @IsWaterTransport, updated_date = CURRENT_TIMESTAMP
                                                    WHERE id = @Id;
                                                  ";

        public readonly string VehicleKindDeleteQuery =
            @"UPDATE ref_vehicle_kinds SET deleted_date = CURRENT_TIMESTAMP, send_to_front_office = true WHERE id = @Id;";
    }
}
