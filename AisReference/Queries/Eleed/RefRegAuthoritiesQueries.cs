﻿namespace AisReference.Queries.Eleed
{
    public class RefRegAuthoritiesQueries
    {
        public readonly string RegAuthoritiesEleedSelectQuery =
            "SELECT id, F80281 as Address, F80281 as Code, F80282 as Contacts, F80280 as Name, F80279 as NameFullRu, F80279 as NameFullKy  FROM A849";

        public readonly string RegAuthoritiesEleedToSiteSelectQuery =
            "SELECT id, address as Address, code as Code, contacts as Contacts, name as Name, name_full_ru as NameFullRu, name_full_ky as NameFullKy " +
            " FROM ref_reg_authorities";

        public readonly string RegAuthoritiesSelectQuery =
            "SELECT id, address as Address, code as Code, contacts as Contacts, name as Name, name_full_ru as NameFullRu, name_full_ky as NameFullKy, " +
            "name_ru as NameRu, name_ky as NameKy, created_date as CreatedDate, updated_date as UpdatedDate, deleted_date as DeletedDate, is_active as IsActive " +
            " FROM ref_reg_authorities";

        public readonly string RegAuthoritiesDefaultCreateQuery = @"
                                                    INSERT INTO ref_reg_authorities(id, address, code, contacts, name, name_full_ru, name_full_ky, created_date, updated_date)
                                                    VALUES (@Id, @Address, @Code, @Contacts, @Name, @NameFullRu, @NameFullKy, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
                                                  ";

        public readonly string RegAuthoritiesCreateQuery = @"
                                                    INSERT INTO ref_reg_authorities(id, address, code, contacts, name, name_full_ru, name_full_ky, created_date, updated_date, name_ru, name_ky, deleted_date, is_active)
                                                    VALUES (@Id, @Address, @Code, @Contacts, @Name, @NameFullRu, @NameFullKy, @CreatedDate, @UpdatedDate, @NameRu, @NameKy, @DeletedDate, @IsActive);
                                                  ";

        public readonly string RegAuthoritiesDefaultUpdateQuery = @"
                                                    UPDATE ref_reg_authorities 
                                                        SET id = @Id,
                                                            address = @Address,
                                                            name = @Code,
                                                            code = @Name,
                                                            name_full_ru = @NameFullRu,
                                                            name_full_ky = @NameFullKy
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegAuthoritiesUpdateQuery = @"
                                                    UPDATE ref_reg_authorities 
                                                        SET id = @Id,
                                                            address = @Address,
                                                            name = @Code,
                                                            code = @Name,
                                                            name_full_ru = @NameFullRu,
                                                            name_full_ky = @NameFullKy,
                                                            name_ru = @NameRu,
                                                            name_ky = @NameKy,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate, 
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegAuthoritiesDeleteQuery =
            @"DELETE FROM ref_reg_authorities WHERE id = @Id;";

    }
}
