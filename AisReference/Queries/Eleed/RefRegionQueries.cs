﻿namespace AisReference.Queries.Eleed
{
    public class RefRegionQueries
    {
        public readonly string RegionEleedSelectQuery =
            "SELECT id, F80914 as Name, F80914 as NameRu, F80914 as NameKy, F80913 as Code, F80915 as IsActive FROM A1325";

        public readonly string RegionEleedToDefaultSelectQuery =
            "SELECT id, name, name_ru as NameRy, name_ky as NameKy, code as Code, is_active as IsActive  FROM ref_regions";

        public readonly string RegionSelectQuery =
            "SELECT id, name, name_ru as NameRy, name_ky as NameKy, code as Code, created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate  FROM ref_regions";


        public readonly string RegionCreateQuery = @"
                                                    INSERT INTO ref_regions(id, name, name_ru, name_ky, code, is_active, created_date, updated_date, deleted_date)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, @Code, @IsActive, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @DeletedDate);
                                                  ";

        public readonly string RegionUpdateQuery = @"
                                                    UPDATE ref_regions 
                                                        SET name=@Name, code=@Code, is_active=@IsActive, updated_date=CURRENT_TIMESTAMP
                                                    WHERE id = @Id; 
                                                    ";

        public readonly string RegionDeleteQuery = @"DELETE FROM ref_regions WHERE id = @Id;";

    }
}
