﻿namespace AisReference.Queries.Eleed
{
    public class RefOwnershipTypeQueries
    {
        public readonly string OwnershipTypeEleedSelectQuery =
            "SELECT id, F80705 as ClassCode, F80708 as NameKy, F80709 as NameRu, F80709 as Name, F80706 as IDCode FROM A917";

        public readonly string OwnershipTypeEleedToDefaultpSelectQuery =
            "SELECT id, class_code as ClassCode, name_ky as NameKy, name_ru as NameRu, name as Name, idcode as IDCode FROM ref_ownership_types";

        public readonly string OwnershipTypepSelectQuery =
            "SELECT id, idcode AS IdCode, name_ky as NameKy, name_ru as NameRu, name as Name, created_date AS CreatedDate, updated_date AS UpdatedDate, deleted_date AS DeletedDate FROM ref_ownership_types";

        public readonly string OwnershipTypeSitepSelectQuery =
            "SELECT id, id_code AS IdCode, name_ky as NameKy, name_ru as NameRu, name as Name, created_date AS CreatedDate, updated_date AS UpdatedDate, deleted_date AS DeletedDate FROM ref_ownership_types";


        public readonly string OwnershipTypeDefaultCreateQuery = @"
                                                    INSERT INTO ref_ownership_types(id, class_code, name_ky, name_ru, name, idcode, created_date, updated_date)
                                                    VALUES (@Id, @ClassCode, @NameKy, @NameRu, @Name, @IDCode, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
                                                  ";

        public readonly string OwnershipTypeCreateQuery = @"
                                                    INSERT INTO ref_ownership_types(id, name_ky, name_ru, name, idcode, created_date, updated_date, deleted_date)
                                                    VALUES (@Id, @NameKy, @NameRu, @Name, @IDCode, CreatedDate, UpdatedDate, @DeletedDate);
                                                  ";

        public readonly string OwnershipTypeSiteCreateQuery = @"
                                                    INSERT INTO ref_ownership_types(id, name_ky, name_ru, name, id_code, created_date, updated_date, deleted_date)
                                                    VALUES (@Id, @NameKy, @NameRu, @Name, @IDCode, @CreatedDate, @UpdatedDate, @DeletedDate);
                                                  ";

        public readonly string OwnershipTypeUpdateQuery = @"
                                                    UPDATE ref_ownership_types 
                                                        SET id = @Id,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            name = @Name,
                                                            idcode = @IDCode,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            class_code = @ClassCode
                                                    WHERE id = @Id;
                                                 ";

        public readonly string OwnershipTypeDefaultUpdateQuery = @"
                                                    UPDATE ref_ownership_types 
                                                        SET id = @Id,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            name = @Name,
                                                            idcode = @IDCode,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            class_code = @ClassCode
                                                    WHERE id = @Id;
                                                 ";

        public readonly string OwnershipTypeSiteUpdateQuery = @"
                                                    UPDATE ref_ownership_types 
                                                        SET id = @Id,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            name = @Name,
                                                            id_code = @IDCode,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            deleted_date = @DeletedDate
                                                    WHERE id = @Id;
                                                 ";

        public readonly string OwnershipTypeDeleteQuery =
            @"DELETE FROM ref_ownership_types WHERE id = @Id;";

    }
}
