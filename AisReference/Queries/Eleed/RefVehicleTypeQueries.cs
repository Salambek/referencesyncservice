﻿namespace AisReference.Queries.Eleed
{
    public class RefVehicleTypeQueries
    {
        public readonly string VehicleTypeEleedSelectQuery =
            "SELECT id, F351904 as Name, F351904 as NameRu, F351904 as NameKy FROM A3100";

        public readonly string VehicleTypeELeedToDefaultSelectQuery =
            "SELECT id, name, name_ru as NameRu, name_ky as NameKy FROM ref_vehicle_types";

        public readonly string VehicleTypeSelectQuery =
            "SELECT id, name, name_ru as NameRu, name_ky as NameKy, created_date as CreatedDate, updated_date as UpdatedDate, descriptions as Descriptions FROM ref_vehicle_types";

        public readonly string VehicleTypeSiteSelectQuery =
            "SELECT id, name, name_ru as NameRu, name_ky as NameKy, created_date as CreatedDate, updated_date as UpdatedDate, description as Descriptions FROM ref_vehicle_types";


        public readonly string VehicleTypeCreateQuery = @"
                                                    INSERT INTO ref_vehicle_types(id, name, name_ru, name_ky, created_date, updated_date, descriptions)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @Descriptions);
                                                  ";

        public readonly string VehicleTypeSiteCreateQuery = @"
                                                    INSERT INTO ref_vehicle_types(id, name, name_ru, name_ky, created_date, updated_date, description)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @Descriptions);
                                                  ";

        public readonly string VehicleTypeUpdateQuery = @"
                                                    UPDATE ref_vehicle_types 
                                                        SET name = @Name, updated_date = CURRENT_TIMESTAMP, descriptions = @Descriptions
                                                    WHERE id = @Id;
                                                  ";

        public readonly string VehicleTypeSiteUpdateQuery = @"
                                                    UPDATE ref_vehicle_types 
                                                        SET name = @Name, updated_date = CURRENT_TIMESTAMP, description = @Descriptions
                                                    WHERE id = @Id;
                                                  ";

        public readonly string VehicleTypeDeleteQuery =
            @"DELETE FROM ref_vehicle_types WHERE id = @Id;";

    }
}
