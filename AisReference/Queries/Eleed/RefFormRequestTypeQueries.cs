﻿namespace AisReference.Queries.Eleed
{
    public class RefFormRequestTypeQueries
    {
        public readonly string FormRequestTypeEleedSelectQuery =
            "SELECT id, uid as guid, F80555 as NameRus, F80556 as NameKyr, F80556 as NameShort, F311290 as InValid FROM A1330";

        public readonly string FormRequestTypeEleedToDefaultSelectQuery =
            "SELECT id, guid, name_rus as NameRus, name_kyr as NameKyr, name_short as NameShort, in_valid as InValid  FROM ref_form_request_type";


        public readonly string FormRequestTypeSiteSelectQuery =
            "SELECT id, guid, name_ru as NameRus, name_ky as NameKyr, name as NameShort, is_active as InValid  FROM ref_form_request_type";

        public readonly string FormRequestTypeCreateQuery = @"
                                                    INSERT INTO ref_form_request_type(id, guid, name_rus, name_kyr, name_short, in_valid)
                                                    VALUES (@Id, @Guid, @NameRus, @NameKyr, @NameShort, @InValid);
                                                  ";

        public readonly string FormRequestTypeSiteCreateQuery = @"
                                                    INSERT INTO ref_form_request_type(id, guid, name_ru, name_ky, name, is_active)
                                                    VALUES (@Id, @Guid, @NameRus, @NameKyr, @NameShort, @InValid);
                                                  ";

        public readonly string FormRequestTypeUpdateQuery = @"
                                                    UPDATE ref_form_request_type 
                                                        SET guid = @Guid, name_rus=@NameRus, name_kyr=@NameKyr, name_short=@NameShort, in_valid=@InValid
                                                    WHERE id = @Id;
                                                  ";

        public readonly string FormRequestTypeSiteUpdateQuery = @"
                                                    UPDATE ref_form_request_type 
                                                        SET guid = @Guid, name_ru=@NameRus, name_ky=@NameKyr, name=@NameShort, is_active=@InValid
                                                    WHERE id = @Id;
                                                  ";

        public readonly string FormRequestTypeDeleteQuery = @"DELETE FROM ref_form_request_type WHERE id = @Id;";
    }
}
