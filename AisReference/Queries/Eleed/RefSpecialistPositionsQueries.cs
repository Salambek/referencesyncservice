﻿namespace AisReference.Queries.Eleed
{
    public class RefSpecialistPositionsQueries
    {
        public readonly string SpecialistPositionEleedSelectQuery =
            "SELECT id, F80265 as Name, F80265 as NameRu, F80265 as NameKy FROM A846";

        public readonly string SpecialistPositionDefaultToEleedSelectQuery =
            "SELECT id, name as Name, name_ru as NameRu, name_ky as NameKy FROM ref_specialist_positions";

        public readonly string SpecialistPositionSelectQuery =
            "SELECT id, name as Name, name_ru as NameRu, name_ky as NameKy, created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate FROM ref_specialist_positions";


        public readonly string SpecialistPositionCreateQuery = @"
                                                    INSERT INTO ref_specialist_positions(id, name, name_ru, name_ky, created_date, updated_date, is_active, deleted_date)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @IsActive, @DeletedDate);
                                                  ";

        public readonly string SpecialistPositionUpdateQuery = @"
                                                    UPDATE ref_specialist_positions 
                                                        SET name = @Name,
                                                            name_ru = @NameRu,
                                                            name_ky = @NameKy,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string SpecialistPositionDeleteQuery =
            @"DELETE FROM ref_specialist_positions WHERE id = @Id;";


    }
}
