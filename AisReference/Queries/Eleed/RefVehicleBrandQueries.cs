﻿namespace AisReference.Queries.Eleed
{
    public class RefVehicleBrandQueries
    {
        public readonly string VehicleBrandEleedSelectQuery =
            "SELECT id, F20189 as Name, F20189 as NameRu,  F20189 as NameKy, F20188 as Code, F392561 as IsWaterTransport, F81021 as IsActive FROM A238";

        public readonly string VehicleBrandEleedToDefaultSelectQuery =
            "SELECT id, name, name_ru as NameRu, name_ky as NameKy, code as Code, is_water_transport as IsWaterTransport, is_active as IsActive FROM ref_vehicle_brands";

        public readonly string VehicleBrandSelectQuery =
            "SELECT id, name, name_ru as NameRu, name_ky as NameKy, code as Code, is_water_transport as IsWaterTransport, is_active as IsActive,  created_date As CreatedDate, updated_date as UpdatedDate, deleted_date as DeletedDate FROM ref_vehicle_brands";


        public readonly string VehicleBrandDefaultCreateQuery = @"
                                                    INSERT INTO ref_vehicle_brands(id, name, name_ru, name_ky, code, is_water_transport, is_active, created_date, updated_date)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, @Code, @IsWaterTransport, @IsActive, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
                                                  ";

        public readonly string VehicleBrandCreateQuery = @"
                                                    INSERT INTO ref_vehicle_brands(id, name, name_ru, name_ky, code, is_water_transport, is_active, created_date, updated_date, deleted_date)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, @Code, @IsWaterTransport, @IsActive, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @DeletedDate);
                                                  ";

        public readonly string VehicleBrandDefaultpdateQuery = @"
                                                    UPDATE ref_vehicle_brands 
                                                        SET name=@Name, code=@Code, is_water_transport=@IsWaterTransport, is_active=@IsActive
                                                    WHERE id = @Id;
                                                  ";

        public readonly string VehicleBrandUpdateQuery = @"
                                                    UPDATE ref_vehicle_brands 
                                                        SET name=@Name, code=@Code, is_water_transport=@IsWaterTransport, is_active=@IsActive, updated_date = @UpdatedDate, created_date = @CreatedDate, deleted_date = @DeletedDate
                                                    WHERE id = @Id;
                                                  ";

        public readonly string VehicleBrandDeleteQuery =
            @"DELETE FROM ref_vehicle_brands WHERE id = @Id;";

    }
}
