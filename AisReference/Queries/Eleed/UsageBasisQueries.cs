﻿namespace AisReference.Queries.Eleed
{
    public class UsageBasisQueries
    {
        public readonly string UsageBasisEleedSelectQuery =
            "SELECT id, F80722 as Name, F80722 as NameRu, F80723 as NameKy FROM A921";

        public readonly string UsageBasisEleedToDefaultSelectQuery =
            "SELECT id, name, name_ru as NameRu, name_ky as NameKy FROM ref_usage_basis";

        public readonly string UsageBasisSelectQuery =
            "SELECT id, name, name_ru as NameRu, name_ky as NameKy, is_active as IsActive,  created_date As CreatedDate, updated_date as UpdatedDate, deleted_date as DeletedDate FROM ref_usage_basis";


        public readonly string UsageBasisDefaultCreateQuery = @"
                                                    INSERT INTO ref_usage_basis(id, name, name_ru, name_ky, created_date, updated_date)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
                                                  ";

        public readonly string UsageBasisCreateQuery = @"
                                                    INSERT INTO ref_usage_basis(id, name, name_ru, name_ky, created_date, updated_date, is_active, deleted_date)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @IsActive, @DeletedDate );
                                                  ";

        public readonly string UsageBasisUpdateQuery = @"
                                                    UPDATE ref_usage_basis 
                                                        SET name = @Name, updated_date = CURRENT_TIMESTAMP, is_active = @IsActive, deleted_date = @DeletedDate
                                                    WHERE id = @Id;
                                                  ";

        public readonly string UsageBasisDefaultUpdateQuery = @"
                                                    UPDATE ref_usage_basis 
                                                        SET name = @Name, updated_date = CURRENT_TIMESTAMP
                                                    WHERE id = @Id;
                                                  ";

        public readonly string UsageBasisDeleteQuery = @"UPDATE ref_usage_basis SET deleted_date = CURRENT_TIMESTAMP, send_to_front_office = true WHERE id = @Id;";

    }
}
