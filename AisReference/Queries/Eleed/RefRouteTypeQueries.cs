﻿namespace AisReference.Queries.Eleed
{
    public class RefRouteTypeQueries
    {
        public readonly string RouteTypeEleedSelectQuery =
            "SELECT id, F331588 as Name, F331588 as NameRu, F331588 as NameKy FROM A2845";

        public readonly string RouteTypeDefaultToEleedSelectQuery =
            "SELECT id, Name, name_ru as NameRu, name_ky as NameKy FROM ref_route_types";

        public readonly string RouteTypeSelectQuery =
            "SELECT id, Name, name_ru as NameRu, name_ky as NameKy, created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive FROM ref_route_types";

        public readonly string RouteTypeCreateQuery = @"
                                                    INSERT INTO ref_route_types(id, name_ru, name_ky, name, created_date, updated_date)
                                                    VALUES (@Id, @NameRu, @NameKy, @Name, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
                                                  ";


        public readonly string RouteTypeSiteCreateQuery = @"
                                                    INSERT INTO ref_route_types(id, name_ru, name_ky, name, created_date, updated_date, is_active)
                                                    VALUES (@Id, @NameRu, @NameKy, @Name, @CreatedDate, @UpdatedDate, IsActive);
                                                  ";

        public readonly string RouteTypeUpdateQuery = @"
                                                    UPDATE ref_route_types 
                                                        SET name = @Name,
                                                            name_ru = @NameRu,
                                                            name_ky = @NameKy,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate, 
                                                            is_active = @IsActive 

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RouteTypeEkmDeleteQuery = @"UPDATE ref_route_types SET deleted_date = CURRENT_TIMESTAMP, send_to_front_office = true WHERE id = @Id;";

        public readonly string RouteTypeDeleteQuery = @"DELETE FROM ref_route_types WHERE id = @Id;";
    }
}
