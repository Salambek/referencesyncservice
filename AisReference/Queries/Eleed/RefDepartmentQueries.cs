﻿namespace AisReference.Queries.Eleed
{
    public class RefDepartmentQueries
    {
        public readonly string RefDepartmentEleedSelectQuery =
            "SELECT id, F80546 as Address, F80545 as ChiefId, F80547 as Phone, F80548 as Email, F352122 as INN, F352123 as CodeOKPO, F130697 as Geolocation, F170959 as RegionId, " +
            "F80549 as Schedule, F331658 as HeadOfDepartmentId " +
            "FROM A615";

        public readonly string RefDepartmentDefaultToEleedSelectQuery =
            "SELECT id, Name, address as Address, chief_id as ChiefId, phone as Phone, email as Email, inn as INN, code_OKPO as CodeOKPO, geolocation as Geolocation, region_Id as RegionId, " +
            "schedule as Schedule, head_of_department_Id as HeadOfDepartmentId " +
            "FROM ref_departments";

        public readonly string RefDepartmentSelectQuery =
            "SELECT id, Name, address as Address, chief_id as ChiefId, phone as Phone, email as Email, inn as INN, code_OKPO as CodeOKPO, geolocation as Geolocation, region_Id as RegionId, " +
            "schedule as Schedule, head_of_department_Id as HeadOfDepartmentId, " +
            "name_ru as NameRu, name_ky as NameKy, created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive " +
            "FROM ref_departments";

        public readonly string RefDepartmentSiteSelectQuery =
            "SELECT id, Name, address as Address, phone as Phone, email as Email, inn as INN, code_OKPO as CodeOKPO, geolocation as Geolocation, region_Id as RegionId, " +
            "schedule as Schedule, head_of_department as HeadOfDepartment, " +
            "name_ru as NameRu, name_ky as NameKy, created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive " +
            "FROM ref_departments";

        public readonly string RefDepartmentDefaultCreateQuery = @"
                                                    INSERT INTO ref_departments(id, Name, address, chief_id, phone, email, inn, code_OKPO, geolocation, region_Id, schedule, head_of_department_Id)
                                                    VALUES (@Id, @Name, @Address, @ChiefId, @Phone, @Email, @INN, @CodeOKPO, @Geolocation, @RegionId, @Schedule, @HeadOfDepartmentId);
                                                  ";

        public readonly string RefDepartmentCreateQuery = @"
                                                    INSERT INTO ref_departments(id, Name, address, chief_id, phone, email, inn, code_OKPO, geolocation, region_Id, schedule, head_of_department_Id, name_ru, name_ky, created_date, updated_date, is_active)
                                                    VALUES (@Id, @Name, @Address, @ChiefId, @Phone, @Email, @INN, @CodeOKPO, @Geolocation, @RegionId, @Schedule, @HeadOfDepartmentId, @NameRu, @NameKy, @CreatedDate, @UpdatedDate, IsActive);
                                                  ";

        public readonly string RefDepartmentSiteCreateQuery = @"
                                                    INSERT INTO ref_departments(id, Name, address, phone, email, inn, code_OKPO, geolocation, region_Id, schedule, head_of_department, name_ru, name_ky, created_date, updated_date, is_active)
                                                    VALUES (@Id, @Name, @Address, @Phone, @Email, @INN, @CodeOKPO, @Geolocation, @RegionId, @Schedule, @HeadOfDepartment, @NameRu, @NameKy, @CreatedDate, @UpdatedDate, IsActive);
                                                  ";

        public readonly string RefDepartmentDefalutUpdateQuery = @"
                                                    UPDATE ref_departments 
                                                        SET name = @Name,
                                                            address = @Address,
                                                            chief_id = @ChiefId,
                                                            phone = @Phone,
                                                            email = @Email,
                                                            inn = @INN,
                                                            code_OKPO = @CodeOKPO,
                                                            geolocation = @Geolocation,
                                                            region_Id = @RegionId,
                                                            schedule = @Schedule,
                                                            head_of_department_Id = @HeadOfDepartmentId 
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefDepartmentUpdateQuery = @"
                                                    UPDATE ref_departments 
                                                        SET name = @Name,
                                                            address = @Address,
                                                            chief_id = @ChiefId,
                                                            phone = @Phone,
                                                            email = @Email,
                                                            inn = @INN,
                                                            code_OKPO = @CodeOKPO,
                                                            geolocation = @Geolocation,
                                                            region_Id = @RegionId,
                                                            schedule = @Schedule,
                                                            head_of_department_Id = @HeadOfDepartmentId,
                                                            name_ru = @NameRu,
                                                            name_ky = @NameKy,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate, 
                                                            is_active = @IsActive 
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefDepartmentSiteUpdateQuery = @"
                                                    UPDATE ref_departments 
                                                        SET name = @Name,
                                                            address = @Address,
                                                            phone = @Phone,
                                                            email = @Email,
                                                            inn = @INN,
                                                            code_OKPO = @CodeOKPO,
                                                            geolocation = @Geolocation,
                                                            region_Id = @RegionId,
                                                            schedule = @Schedule,
                                                            head_of_department = @HeadOfDepartment,
                                                            name_ru = @NameRu,
                                                            name_ky = @NameKy,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate, 
                                                            is_active = @IsActive 
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefDepartmentEkmDeleteQuery = @"UPDATE ref_departments SET deleted_date = CURRENT_TIMESTAMP, send_to_front_office = true WHERE id = @Id;";

        public readonly string RefDepartmentDeleteQuery = @"DELETE FROM ref_departments WHERE id = @Id;";
    }
}
