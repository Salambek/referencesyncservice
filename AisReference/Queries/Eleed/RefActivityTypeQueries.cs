﻿namespace AisReference.Queries.Eleed
{
    public class RefActivityTypeQueries
    {
        public readonly string ActivityTypeEleedSelectQuery =
            "SELECT id, uid as Guid, F81840 as Abbreviation, F80197 as Code, F311301 as CoveredTerId, F80194 as NameKyr, F80195 as NameRus, F80910 as LicNameRus, " + 
            "F80908 as LicNameKyr, F81910 as Type, F392562 as IsWaterTransport, F80912 as InValid FROM A834";

        public readonly string ActivityTypeEleedToDefaultSelectQuery =
            "SELECT id, guid, name_rus as NameRus, name_kyr as NameKyr, abbreviation as Abbreviation, covered_ter_id as CoveredTerId, lic_name_rus as LicNameRus, " +
            "lic_name_kyr as LicNameKyr, type as Type, is_water_transport as IsWaterTransport, in_valid as InValid, code as Code " +
            "FROM ref_activity_types";

        public readonly string ActivityTypeDefaultSelectQuery =
            "SELECT id, name_rus as NameRus, name_kyr as NameKyr, abbreviation as Abbreviation, covered_ter_id as CoveredTerId, lic_name_rus as LicNameRus, " +
            "lic_name_kyr as LicNameKyr, is_water_transport as IsWaterTransport, code as Code " +
            "FROM ref_activity_types";

        public readonly string ActivityTypeSiteSelectQuery =
            "SELECT id, name_ru as NameRus, name_ky as NameKyr, abbreviation as Abbreviation, covered_ter_id as CoveredTerId, lic_name_rus as LicNameRus, " + 
            "lic_name_kyr as LicNameKyr, is_water_transport as IsWaterTransport, code as Code " +
            "FROM ref_activity_types";


        public readonly string ActivityTypeCreateQuery = @"
                                                    INSERT INTO ref_activity_types(
                                                                id, 
                                                                guid, 
                                                                name_rus, 
                                                                name_kyr, 
                                                                abbreviation, 
                                                                covered_ter_id, 
                                                                lic_name_rus,
                                                                lic_name_kyr, 
                                                                type, 
                                                                is_water_transport, 
                                                                in_valid, 
                                                                code
                                                                                )
                                                    VALUES (
                                                                @Id, 
                                                                @Guid,
                                                                @NameRus, 
                                                                @NameKyr, 
                                                                @Abbreviation, 
                                                                @CoveredTerId, 
                                                                @LicNameRus, 
                                                                @LicNameKyr, 
                                                                @Type, 
                                                                @IsWaterTransport, 
                                                                @InValid, 
                                                                @Code
                                                            );
                                                  ";

        public readonly string ActivityTypeSiteCreateQuery = @"
                                                    INSERT INTO ref_activity_types(
                                                                id, 
                                                                name_ru, 
                                                                name_ky, 
                                                                abbreviation, 
                                                                covered_ter_id, 
                                                                lic_name_rus,
                                                                lic_name_kyr, 
                                                                is_water_transport, 
                                                                code
                                                                                )
                                                    VALUES (
                                                                @Id, 
                                                                @NameRus, 
                                                                @NameKyr, 
                                                                @Abbreviation, 
                                                                @CoveredTerId, 
                                                                @LicNameRus, 
                                                                @LicNameKyr, 
                                                                @IsWaterTransport, 
                                                                @Code
                                                            );
                                                  ";

        public readonly string ActivityTypeUpdateQuery = @"
                                                    UPDATE ref_activity_types 
                                                        SET guid = @Guid
                                                            name_rus = @NameRus, 
                                                            name_kyr = @NameKyr, 
                                                            abbreviation = @Abbreviation, 
                                                            covered_ter_id = @CoveredTerId, 
                                                            lic_name_rus = @LicNameRus,
                                                            lic_name_kyr = @LicNameKyr, 
                                                            type = @Type, 
                                                            is_water_transport = @IsWaterTransport, 
                                                            in_valid = @InValid, 
                                                            code = @Code
                                                    WHERE id = @Id;
                                                  ";

        public readonly string ActivityTypeDefaultUpdateQuery = @"
                                                    UPDATE ref_activity_types 
                                                        SET name_ru = @NameRus, 
                                                            name_ky = @NameKyr, 
                                                            abbreviation = @Abbreviation, 
                                                            covered_ter_id = @CoveredTerId, 
                                                            lic_name_rus = @LicNameRus,
                                                            lic_name_kyr = @LicNameKyr, 
                                                            is_water_transport = @IsWaterTransport, 
                                                            code = @Code
                                                    WHERE id = @Id;
                                                  ";

        public readonly string ActivityTypeDeleteQuery =
            @"DELETE FROM ref_activity_types WHERE id = @Id;";

    }
}
