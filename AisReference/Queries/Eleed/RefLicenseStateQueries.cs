﻿namespace AisReference.Queries.Eleed
{
    public class RefLicenseStateQueries
    {
        public readonly string LicenseStateEleedSelectQuery =
            "SELECT id, uid as guid, F80435 as Name FROM A878";

        public readonly string LicenseStateEleedToDefaultSelectQuery =
            "SELECT id, guid, name as Name FROM ref_license_states";

        public readonly string LicenseStateSiteSelectQuery =
            "SELECT id, name as Name FROM ref_license_states";

        public readonly string LicenseStateDefaultCreateQuery = @"
                                                    INSERT INTO ref_license_states(id, guid, name)
                                                    VALUES (@Id, @Guid, @Name);
                                                  ";

        public readonly string LicenseStateSiteCreateQuery = @"
                                                    INSERT INTO ref_license_states(id, name)
                                                    VALUES (@Id, @Name);
                                                  ";

        public readonly string LicenseStateDefaultUpdateQuery = @"
                                                    UPDATE ref_license_states 
                                                        SET guid = @Guid,
                                                            name = @Name,
                                                    WHERE id = @Id;
                                                  ";

        public readonly string LicenseStateSiteUpdateQuery = @"
                                                    UPDATE ref_license_states 
                                                        SET name = @Name
                                                    WHERE id = @Id;
                                                  ";

        public readonly string LicenseStateDeleteQuery =
            @"DELETE FROM ref_license_states WHERE id = @Id;";

    }
}
