﻿namespace AisReference.Queries.Eleed
{
    public class RefEuroLimitQueries
    {
        public readonly string EuroLimitEleedSelectQuery =
           "SELECT id, uid as guid, F130391 as NameRus, F130392 as NameKyr FROM A1372";

        public readonly string EuroLimitEleedToDefaultSelectQuery =
            "SELECT id, guid, name_rus as NameRus, name_kyr as NameKyr FROM ref_euro_limit";
        
        public readonly string EuroLimitDefaultSelectQuery =
            "SELECT id, name_rus as NameRus, name_kyr as NameKyr FROM ref_euro_limit";

        public readonly string EuroLimitSiteSelectQuery =
            "SELECT id, name_ru as NameRus, name_ky as NameKyr FROM ref_permit_restriction_types";

        public readonly string EuroLimitDefaultCreateQuery = @"
                                                    INSERT INTO ref_euro_limit(id, guid, name_rus, name_kyr)
                                                    VALUES (@Id, @Guid, @NameRus, @NameKyr);
                                                  ";
        
        public readonly string EuroLimitSiteCreateQuery = @"
                                                    INSERT INTO ref_permit_restriction_types(id, name_ru, name_ky)
                                                    VALUES (@Id, @NameRus, @NameKyr);
                                                  ";

        public readonly string EuroLimitDefaultUpdateQuery = @"
                                                    UPDATE ref_euro_limit 
                                                        SET guid = @Guid, name_rus = @NameRus, name_kyr = @NameKyr
                                                    WHERE id = @Id;
                                                  ";

        public readonly string EuroLimitSiteUpdateQuery = @"
                                                    UPDATE ref_permit_restriction_types 
                                                        SET name_ru = @NameRus, name_ky = @NameKyr
                                                    WHERE id = @Id;
                                                  ";

        public readonly string EuroLimitDeleteQuery = @"DELETE FROM ref_euro_limit WHERE id = @Id;";

        public readonly string EuroLimitSiteDeleteQuery = @"DELETE FROM ref_permit_restriction_types WHERE id = @Id;";
    }
}
