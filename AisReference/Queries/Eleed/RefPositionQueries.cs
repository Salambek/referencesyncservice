﻿namespace AisReference.Queries.Eleed
{
    public class RefPositionQueries
    {
        public readonly string PositionEleedSelectQuery =
            "SELECT id, uid as guid, F60077 as Name, F81017 as InValid, F331559 as Weight FROM A616";

        public readonly string PositionSelectQuery =
            "SELECT id, guid, name as Name, in_valid as InValid, weight as Weight FROM ref_position";



        public readonly string PositionCreateQuery = @"
                                                    INSERT INTO ref_position(id, guid, name, in_valid, weight)
                                                    VALUES (@Id, @Guid, @Name, @InValid, @Weight);
                                                 ";

        public readonly string PositionUpdateQuery = @" 
                                                    UPDATE ref_position 
                                                        SET guid = @Guid,
                                                            name = @Name,
                                                            in_valid = @InValid,
                                                            weight = @Weight
                                                    WHERE id = @Id;
                                                  ";

        public readonly string PositionDeleteQuery = @"DELETE FROM ref_position WHERE id = @Id;";

    }
}
