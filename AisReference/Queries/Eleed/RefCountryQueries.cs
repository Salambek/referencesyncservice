﻿namespace AisReference.Queries.Eleed
{
    public class RefCountryQueries
    {
        public readonly string CountryEleedSelectQuery =
            "SELECT id, F20051 as NameRu, F20052 as NameKy, F20191 as Code2, F20053 as Code3 FROM A212";

        public readonly string CountryEleedToDefaultSelectQuery =
            "SELECT id, name_ru as NameRu, name_ky as NameKy, code2 as Code2, code3 as Code3 FROM ref_countries";

        public readonly string CountrySelectQuery =
            "SELECT id, name as Name, name_ru as NameRu, name_ky as NameKy, code2 as Code2, code3 as Code3, " +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            " FROM ref_countries";

        public readonly string CountrySiteSelectQuery =
            "SELECT id, name as Name, name_ru as NameRu, name_ky as NameKy, code_2 as Code2, code_3 as Code3, " +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            " FROM ref_countries";

        public readonly string CountryDefaultCreateQuery = @"
                                                    INSERT INTO ref_countries(id, name_ru, name_ky, code2, code3, created_date, updated_date)
                                                    VALUES (@Id, @NameRu, @NameKy, @Code2, @Code3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
                                                  ";

        public readonly string CountryCreateQuery = @"
                                                    INSERT INTO ref_countries(id, name, name_ru, name_ky, code2, code3, is_active, created_date, updated_date, deleted_date)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, @Code2, @Code3, @IsActive, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @DeletedDate);
                                                  ";

        public readonly string CountrySiteCreateQuery = @"
                                                    INSERT INTO ref_countries(id, name, name_ru, name_ky, code_2, code_3, is_active, created_date, updated_date, deleted_date)
                                                    VALUES (@Id, @Name, @NameRu, @NameKy, @Code2, @Code3, @IsActive, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @DeletedDate);
                                                  ";

        public readonly string CountryDefaultUpdateQuery = @"UPDATE ref_countries 
                                                    SET id = @Id, 
                                                        code2 = @Code2,
                                                        code3 = @Code3,
                                                        name_ky = @NameKy,
                                                        name_ru = @NameRu
                                                    WHERE id = @Id;";

        public readonly string CountryUpdateQuery = @"UPDATE ref_countries 
                                                    SET id = @Id, 
                                                        name = @Name,
                                                        code2 = @Code2,
                                                        code3 = @Code3,
                                                        name_ky = @NameKy,
                                                        name_ru = @NameRu,
                                                        is_active = @IsActive,
                                                        created_date = @CreatedDate,
                                                        updated_date = @UpdatedDate,
                                                        deleted_date = @DeletedDate
                                                    WHERE id = @Id;";

        public readonly string CountrySiteUpdateQuery = @"UPDATE ref_countries 
                                                    SET id = @Id, 
                                                        name = @Name,
                                                        code_2 = @Code2,
                                                        code_3 = @Code3,
                                                        name_ky = @NameKy,
                                                        name_ru = @NameRu,
                                                        is_active = @IsActive,
                                                        created_date = @CreatedDate,
                                                        updated_date = @UpdatedDate,
                                                        deleted_date = @DeletedDate
                                                    WHERE id = @Id;";

        public readonly string CountryDeleteQuery = @"DELETE FROM ref_countries WHERE id = @Id;";
    }
}
