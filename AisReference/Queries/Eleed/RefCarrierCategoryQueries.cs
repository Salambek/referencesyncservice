﻿namespace AisReference.Queries.Eleed
{
    public class RefCarrierCategoryQueries
    {
        public readonly string CarrierCategoryEleedSelectQuery =
            "SELECT id, uid as guid, F80224 as name FROM A839";

        public readonly string CarrierCategoryEleedToDefaultSelectQuery =
            "SELECT id, guid, name as name FROM ref_carrier_categories";

        public readonly string CarrierCategorySelectQuery =
            "SELECT id, guid, name as name, name_ky as NameKy, name_ru as NameRu, " +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_carrier_categories";

        public readonly string CarrierCategorySiteSelectQuery =
            "SELECT id, name as name, name_ky as NameKy, name_ru as NameRu, " +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_carrier_categories";

        public readonly string CarrierCategoryCreateQuery = @"
                                                    INSERT INTO ref_carrier_categories(id, guid, name, created_date, updated_date, deleted_date)
                                                    VALUES (@Id, @Guid, @Name, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @DeletedDate);
                                                  ";

        public readonly string CarrierCategorySiteCreateQuery = @"
                                                    INSERT INTO ref_carrier_categories(id, name, created_date, updated_date, deleted_date)
                                                    VALUES (@Id, @Name, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @DeletedDate);
                                                  ";

        public readonly string CarrierCategoryUpdateQuery = @"
                                                    UPDATE ref_carrier_categories 
                                                        SET guid = @Guid,
                                                            name = @Name,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            deleted_date = @DeletedDate 
                                                    WHERE id = @Id;
                                                  ";

        public readonly string CarrierCategorySiteUpdateQuery = @"
                                                    UPDATE ref_carrier_categories 
                                                        SET name = @Name,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            deleted_date = @DeletedDate 
                                                    WHERE id = @Id;
                                                  ";

        public readonly string CarrierCategoryDeleteQuery =
            @"DELETE FROM ref_carrier_categories WHERE id = @Id;";

    }
}
