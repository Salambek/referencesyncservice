﻿namespace AisReference.Queries
{
    public class UserQueries
    {
        //User
        public readonly string UserEleedSelectQuery = "SELECT id_user as id, loginname as username, password, firstname, middlename, lastname, uid FROM BIZUSERS";

        public readonly string UserDefaultSelectQuery = "SELECT id, user_name as username, password, first_name as firstname, middle_name as middlename, last_name as lastname, uid FROM users";

        public readonly string UserSiteSelectQuery = "SELECT id, username, password FROM users";

        public readonly string UserSelectQuery = "SELECT id, user_name, password FROM users";

        public readonly string UserCreateQuery = @"
                                                    INSERT INTO users(id, user_name, password, first_name, middle_name, last_name, uid, is_deleted)
                                                    VALUES (@Id, @UserName, @Password, @FirstName, @MiddleName, @LastName, @Uid, @IsDeleted);
                                                  ";

        public readonly string UserSiteCreateQuery = @"
                                                    INSERT INTO users(id, username, password)
                                                    VALUES (@Id, @UserName, @Password);
                                                  ";

        public readonly string UserUpdateQuery = @"
                                                    UPDATE users 
                                                        SET user_name = @UserName, password = @Password, first_name = @FirstName, middle_name = @MiddleName, last_name = @LastName, uid = @Uid 
                                                    WHERE id = @Id;
                                                  ";

        public readonly string UserSiteUpdateQuery = @"
                                                    UPDATE users 
                                                        SET username = @UserName, password = @Password 
                                                    WHERE id = @Id;
                                                  ";

        public readonly string UserDeleteQuery = @"DELETE FROM users WHERE id = @Id;";


        //UserGroup
        public readonly string UserGroupEleedSelectQuery = "SELECT id_group as id, name, uid FROM BIZGROUPS";

        public readonly string UserGroupSelectQuery = "SELECT id, name, uid FROM user_group";

        public readonly string UserGroupCreateQuery = @"
                                                         INSERT INTO user_group(id, name, uid)
                                                         VALUES (@Id, @Name, @Uid);
                                                       ";

        public readonly string UserGroupUpdateQuery = @"UPDATE user_group 
                                                         SET name = @Name, uid = @Uid 
                                                       WHERE id = @Id;";

        public readonly string UserGroupDeleteQuery = @"
                                                         DELETE FROM user_group WHERE id = @Id AND name = @Name AND uid = @Uid;
                                                       ";


        //UserInGroup
        public readonly string UserInGroupELeedSelectQuery = "SELECT id_usringrp as ID, id_group as UserGroupId, uid_user as UserUid FROM BIZUSRINGRP";

        public readonly string UserInGroupSelectQuery = "SELECT id as ID, user_group_id as UserGroupId, user_uid as UserUid FROM user_in_group";

        public readonly string UserInGroupCreateQuery = @"
                                                          INSERT INTO user_in_group(id, user_group_id, user_uid)
                                                          VALUES (@Id, @UserGroupId, @UserUid);
                                                        ";

        public readonly string UserInGroupUpdateQuery = @"UPDATE user_in_group 
                                                          SET user_group_id = @UserGroupId, user_uid = @UserUid
                                                        WHERE id = @Id;";

        public readonly string UserInGroupDeleteQuery = @"
                                                          DELETE FROM user_in_group WHERE id = @Id AND user_group_id = @UserGroupId AND user_uid = @UserUid;
                                                       ";

        //RegEmployee
        public readonly string RegEmployeeELeedSelectQuery = "SELECT id, uid as Guid, F60065 as Name, F60064 as FullName, F60071 as Email, F60070 as CellPhone, " +
                                                                "F60069 as Phone, F60068 as PhysicalAddress, F60067 as PostAddress, F60072 as Skype, F60073 as ICQ, F20205 as BadgeNumber, " +
                                                                "F60078 as Login, F80700 as Password, F80699 as Role, F20204 as CheckpointId, F20202 as DepartmentId, F20203 as PositionId, " +
                                                                "F81105 as ForcePwdChange, F60063 as ParentId, F60066 as PhotoId, F81025 as UserGuid, F80703 as UserId, " +
                                                                "F81030 as InValid, F80702 as ModifiedDate, F80701 as CreationDate " +
                                                                " FROM A613";

        public readonly string RegEmployeeDefaultSelectQuery = "SELECT id, guid as Guid, name as Name, full_name as FullName, email as Email, cell_phone as CellPhone, " +
                                                                "phone as Phone, physical_address as PhysicalAddress, post_address as PostAddress, skype as Skype, icq as ICQ, badge_number as BadgeNumber, " +
                                                                "login as Login, password as Password, role as Role, checkpoint_id as CheckpointId, department_id as DepartmentId, position_id as PositionId, " +
                                                                "force_pwd_change as ForcePwdChange, parent_id as ParentId, photo_id as PhotoId, user_guid as UserGuid, user_id as UserId, " +
                                                                "in_valid as InValid, modified_date as ModifiedDate, creation_date as CreationDate " +
                                                                " FROM reg_employee";

        public readonly string RegEmployeeDefaultCreateQuery = @"
                                                          INSERT INTO reg_employee(
                                                                    id, 
                                                                    guid, 
                                                                    name, 
                                                                    full_name, 
                                                                    email,
                                                                    cell_phone,
                                                                    phone,
                                                                    physical_address,
                                                                    post_address,
                                                                    skype,
                                                                    icq,
                                                                    badge_number,
                                                                    login,
                                                                    password,
                                                                    checkpoint_id,
                                                                    department_id,
                                                                    position_id,
                                                                    force_pwd_change,
                                                                    parent_id,
                                                                    photo_id,
                                                                    user_guid,
                                                                    user_id,
                                                                    in_valid,
                                                                    modified_date,
                                                                    creation_date
                                                                )
                                                          VALUES (
                                                                    @Id, 
                                                                    @Guid, 
                                                                    @Name, 
                                                                    @FullName,
                                                                    @Email,
                                                                    @CellPhone,
                                                                    @Phone,
                                                                    @PhysicalAddress,
                                                                    @PostAddress,
                                                                    @Skype,
                                                                    @ICQ,
                                                                    @BadgeNumber,
                                                                    @Login,
                                                                    @Password,
                                                                    @CheckpointId,
                                                                    @DepartmentId,
                                                                    @PositionId,
                                                                    @ForcePwdChange,
                                                                    @ParentId,
                                                                    @PhotoId,
                                                                    @UserGuid,
                                                                    @UserId,
                                                                    @InValid,
                                                                    @ModifiedDate,
                                                                    @CreationDate
                                                                 );";


        public readonly string RegEmployeeDefaultUpdateQuery = @"UPDATE reg_employee 
                                                          SET guid = @Guid, 
                                                              name = @Name, 
                                                              full_name = @FullName, 
                                                              email = @Email,
                                                              cell_phone = @CellPhone,
                                                              phone = @Phone,
                                                              physical_address = @PhysicalAddress,
                                                              post_address = @PostAddress,
                                                              skype = @Skype,
                                                              icq = @ICQ,
                                                              badge_number = @BadgeNumber,
                                                              login = @Login,
                                                              password = @Password,
                                                              checkpoint_id = @CheckpointId,
                                                              department_id = @DepartmentId,
                                                              position_id = @PositionId,
                                                              force_pwd_change = @ForcePwdChange,
                                                              parent_id = @ParentId,
                                                              photo_id = @PhotoId,
                                                              user_guid = @UserGuid,
                                                              user_id = @UserId,
                                                              in_valid = @InValid,
                                                              modified_date = @ModifiedDate,
                                                              creation_date = @CreationDate
                                                        WHERE id = @Id;";

        public readonly string RegEmployeeDefaultDeleteQuery = @"
                                                          DELETE FROM reg_employee WHERE id = @Id;
                                                       ";
    }
}