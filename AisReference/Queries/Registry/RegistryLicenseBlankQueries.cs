﻿namespace AisReference.Queries.Registry
{
    public class RegistryLicenseBlankQueries
    {
        public readonly string RegisterLicenseBlankEleedSelectQuery =
            "SELECT id, uid as guid, F81921 as FormValidityEnd, F81920 as FormValidityStart, F81896 as IsCancelled, F81226 as LicenseId, " +
            "F81228 as LicFormIssuedDate, F331710 as LicFormLicNumber, F81225 as LicFormNumber, F290963 as MainForm, " +
            "F81760 as ReceiptBalance, F81758 as SumToPay, F81227 as DepartmentId, F311344 as TrailerId, F81255 as VehicleId" +
            " FROM A979";

        public readonly string RegisterLicenseBlankEleedToDefaultSelectQuery =
            "SELECT id, form_validity_end as FormValidityEnd, form_validity_start as FormValidityStart, is_cancelled as IsCancelled, " +
            "license_id as LicenseId, lic_form_issued_date as LicFormIssuedDate, lic_form_lic_number as LicFormLicNumber, " +
            "lic_form_Number as LicFormNumber, main_form as MainForm, receipt_balance as ReceiptBalance, sum_to_pay as SumToPay, " +
            "department_id as DepartmentId, trailer_id as TrailerId, vehicle_id as VehicleId" +
            " FROM registry_license_blanks" +
            " WHERE vehicle_id IS NOT NULL and trailer_id IS NOT NULL and license_id IS NOT NULL";


        public readonly string RegisterLicenseBlankCreateQuery = @"
            INSERT INTO registry_license_blanks(id, form_validity_end, form_validity_start, is_cancelled, license_id, lic_form_issued_date, lic_form_lic_number, lic_form_Number, main_form, receipt_balance, sum_to_pay, department_id, vehicle_id, trailer_id)
            VALUES (@Id, @FormValidityEnd, @FormValidityStart, @IsCancelled, @LicenseId, @LicFormIssuedDate, @LicFormLicNumber, @LicFormNumber, @MainForm, @ReceiptBalance, @SumToPay, @DepartmentId, @VehicleId, @TrailerId);
                                                    ";

        public readonly string RegisterLicenseBlankUpdateQuery = @"
                                                    UPDATE registry_license_blanks 
                                                        SET form_validity_end = @FormValidityEnd, form_validity_start = @FormValidityStart, is_cancelled = @IsCancelled, license_id = @LicenseId, lic_form_issued_date = @LicFormIssuedDate, lic_form_lic_number = @LicFormLicNumber, lic_form_Number = @LicFormNumber, main_form = @MainForm, receipt_balance = @ReceiptBalance, sum_to_pay = @SumToPay, department_id = @DepartmentId, vehicle_id = @VehicleId, trailer_id = @TrailerId
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegisterLicenseBlankDeleteQuery =
            @"DELETE FROM registry_license_blanks WHERE id = @Id;";
    }
}
