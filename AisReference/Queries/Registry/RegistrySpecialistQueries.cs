﻿namespace AisReference.Queries.Registry
{
    public class RegistrySpecialistQueries
    {
        public readonly string RegistrySpecialistEleedSelectQuery =
        "SELECT id, uid as Guid, F80312 as INN, F81243 as Surname, F81244 as Name, F81245 as MiddleName, F331831 as DrivingExperience, F81246 as DateOfBirth FROM A852";

        public readonly string RegistrySpecialistSelectQuery =
            "SELECT id, guid, inn, surname, name, middle_name, driving_experience, date_of_birth  FROM registry_specialists";

        public readonly string RegistrySpecialistSiteSelectQuery =
            "SELECT id, guid, inn, surname, name, middle_name, work_experience, date_of_birth  FROM registry_specialists";

        public readonly string RegistrySpecialistCreateQuery = @"
                                                    INSERT INTO registry_specialists(id, guid, inn, surname, name, middle_name, driving_experience, date_of_birth)
                                                    VALUES (@Id, @Guid, @INN, @Surname, @Name, @MiddleName, @DrivingExperience, @DateOfBirth);
                                                  ";

        public readonly string RegistrySpecialistSiteCreateQuery = @"
                                                    INSERT INTO registry_specialists(id, guid, inn, surname, name, middle_name, work_experience, date_of_birth)
                                                    VALUES (@Id, @Guid, @INN, @Surname, @Name, @MiddleName, @DrivingExperience, @DateOfBirth);
                                                  ";

        public readonly string RegistrySpecialistUpdateQuery =
            @"
                                                    UPDATE registry_specialists 
                                                        SET guid = @Guid, inn = @INN, surname = @Surname, name = @Name, middle_name = @MiddleName, driving_experience = @DrivingExperience, date_of_birth = @DateOfBirth
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegistrySpecialistSiteUpdateQuery =
            @"
                                                    UPDATE registry_specialists 
                                                        SET guid = @Guid, inn = @INN, surname = @Surname, name = @Name, middle_name = @MiddleName, work_experience = @DrivingExperience, date_of_birth = @DateOfBirth
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegistrySpecialistDeleteQuery =
            @"DELETE FROM registry_specialists WHERE id = @Id;";
    }
}
