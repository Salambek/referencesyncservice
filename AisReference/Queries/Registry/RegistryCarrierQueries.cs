﻿namespace AisReference.Queries.Registry
{
    public class RegistryCarrierQueries
    {
        public readonly string CarrierEleedSelectQuery =
        "SELECT id, uid as guid, F80771 as BaseAddress, F81241 as BrandName, F81356 as BaseRegionId, F80735 as CarrierCategoryId, " +
        "F80729 as CarrierStateId, F80734 as Comment, F80728 as DepartmentId, F80745 as Education, F80748 as Email, " +
        "F80746 as Experience, F80743 as HeadName, F80744 as HeadPosition, F80736 as INN, F80753 as LegalAddress, " +
        "F81355 as LegalRegionId, F311313 as Lessor, F80774 as login, F311339 as MiddleName, F311338 as Name, " +
        "F80739 as NameLegal, F80737 as OrgStructureId, F80738 as OwnershipTypeId, F80759 as PassportDate, F80760 as PassportEnd, " +
        "F80761 as PassportIssuedBy, F80758 as PassportNum, F80775 as Password, F883491 as PasswordHashed, F80747 as Phones, " +
        "F80757 as RealAddress, F80755 as RealRegionId, F80763 as RegAuthoritiesId, F80766 as RegDate, F80767 as RegExpirationDate, " +
        "F80916 as RegionId, F80765 as RegNumber, F80764 as RegSeries, F80762 as RegTypeId, F81933 as RegСarDate, " +
        "F81360 as ShortName, F170992 as SubjectId, F311337 as Surname, F80772 as UsageBasisId, F80773 as UsageExpireDate " +
        "FROM A863";

        public readonly string CarrierSelectQuery =
            "SELECT id, guid, base_address as BaseAddress, brand_name as BrandName, base_region_id as BaseRegionId, carrier_category_id as CarrierCategoryId, " +
            "carrier_state_id as CarrierStateId, comment as Comment, department_id as DepartmentId, education as Education, email as Email, " +
            "experience as Experience, head_name as HeadName, head_position as HeadPosition, inn as INN, legal_address as LegalAddress, " +
            "legal_region_id as LegalRegionId, lessor as Lessor, login as login, middle_name as MiddleName, name as Name, " +
            "name_legal as NameLegal, org_structure_id as OrgStructureId, ownership_type_id as OwnershipTypeId, passport_date as PassportDate, passport_end as PassportEnd, " +
            "passport_issued_by as PassportIssuedBy, passport_num as PassportNum, password as Password, password_hashed as PasswordHashed, phones as Phones, " +
            "real_address as RealAddress, real_region_id as RealRegionId, reg_authorities_id as RegAuthoritiesId, reg_date as RegDate, reg_expiration_date as RegExpirationDate, " +
            "region_id as RegionId, reg_number as RegNumber, reg_series as RegSeries, reg_type_id as RegTypeId, regсar_date as RegСarDate, " +
            "short_name as ShortName, subject_id as SubjectId, surname as Surname, usage_basis_id as UsageBasisId, usage_expire_date as UsageExpireDate " +
            "FROM registry_carriers";

        public readonly string CarrierSiteSelectQuery =
            "SELECT id, guid, base_address as BaseAddress, brand_name as BrandName, base_region_id as BaseRegionId, carrier_category_id as CarrierCategoryId, " +
            "carrier_state_id as CarrierStateId, comment as Comment, department_id as DepartmentId, education as Education, email as Email, " +
            "experience as Experience, head_name as HeadName, head_position as HeadPosition, inn as INN, legal_address as LegalAddress, " +
            "legal_region_id as LegalRegionId, lessor as Lessor, middle_name as MiddleName, name as Name, " +
            "name_legal as NameLegal, org_structure_id as OrgStructureId, ownership_type_id as OwnershipTypeId, passport_date as PassportDate, passport_end as PassportEnd, " +
            "passport_issued_by as PassportIssuedBy, passport_num as PassportNum, phones as Phones, " +
            "real_address as RealAddress, real_region_id as RealRegionId, reg_authorities_id as RegAuthoritiesId, reg_date as RegDate, reg_expiration_date as RegExpirationDate, " +
            "region_id as RegionId, reg_number as RegNumber, reg_series as RegSeries, reg_type_id as RegTypeId, reg_car_date as RegСarDate, " +
            "short_name as ShortName, subject_id as SubjectId, surname as Surname, usage_basis_id as UsageBasisId, usage_expire_date as UsageExpireDate " +
            "FROM registry_carriers";

        public readonly string CarrierDefaultToSiteSelectQuery =
            "SELECT id, guid, base_address as BaseAddress, brand_name as BrandName, base_region_id as BaseRegionId, carrier_category_id as CarrierCategoryId, " +
            "carrier_state_id as CarrierStateId, comment as Comment, department_id as DepartmentId, education as Education, email as Email, " +
            "experience as Experience, head_name as HeadName, head_position as HeadPosition, inn as INN, legal_address as LegalAddress, " +
            "legal_region_id as LegalRegionId, lessor as Lessor, middle_name as MiddleName, name as Name, " +
            "name_legal as NameLegal, org_structure_id as OrgStructureId, ownership_type_id as OwnershipTypeId, passport_date as PassportDate, passport_end as PassportEnd, " +
            "passport_issued_by as PassportIssuedBy, passport_num as PassportNum, phones as Phones, " +
            "real_address as RealAddress, real_region_id as RealRegionId, reg_authorities_id as RegAuthoritiesId, reg_date as RegDate, reg_expiration_date as RegExpirationDate, " +
            "region_id as RegionId, reg_number as RegNumber, reg_series as RegSeries, reg_type_id as RegTypeId, regсar_date as RegСarDate, " +
            "short_name as ShortName, subject_id as SubjectId, surname as Surname, usage_basis_id as UsageBasisId, usage_expire_date as UsageExpireDate " +
            "FROM registry_carriers";

        public readonly string CarrierCreateQuery = @"
                                                    INSERT INTO registry_carriers(id, guid, base_address, brand_name, 
                                                              base_region_id, carrier_category_id, 
                                                              carrier_state_id, comment, department_id, 
                                                              education, email, experience, head_name, 
                                                              head_position, inn, legal_address, legal_region_id, 
                                                              lessor, login, middle_name, name, name_legal, 
                                                              org_structure_id, ownership_type_id, passport_date, 
                                                              passport_end, passport_issued_by, passport_num, password, 
                                                              password_hashed, phones, real_address, real_region_id, 
                                                              reg_authorities_id, reg_date, reg_expiration_date, 
                                                              region_id, reg_number, reg_series, reg_type_id, regсar_date, 
                                                              short_name, subject_id, surname, usage_basis_id, usage_expire_date)
                                                    VALUES (@Id, @Guid, @BaseAddress, @BrandName, @BaseRegionId, @CarrierCategoryId, 
                                                            @CarrierStateId, @Comment, @DepartmentId, @Education, @Email, @Experience, 
                                                            @HeadName, @HeadPosition, @INN, @LegalAddress, @LegalRegionId, @Lessor, 
                                                            @Login, @MiddleName, @Name, @NameLegal, @OrgStructureId, @OwnershipTypeId, 
                                                            @PassportDate, @PassportEnd, @PassportIssuedBy, @PassportNum, @Password, 
                                                            @PasswordHashed, @Phones, @RealAddress, @RealRegionId, @RegAuthoritiesId, 
                                                            @RegDate, @RegExpirationDate, @RegionId, @RegNumber, @RegSeries, @RegTypeId, 
                                                            @RegСarDate, @ShortName, @SubjectId, @Surname, @UsageBasisId, @UsageExpireDate);
                                                  ";

        public readonly string CarrierSiteCreateQuery = @"
                                                    INSERT INTO registry_carriers(id, guid, base_address, brand_name, 
                                                              base_region_id, carrier_category_id, 
                                                              carrier_state_id, comment, department_id, 
                                                              education, email, experience, head_name, 
                                                              head_position, inn, legal_address, legal_region_id, 
                                                              lessor, middle_name, name, name_legal, 
                                                              org_structure_id, ownership_type_id, passport_date, 
                                                              passport_end, passport_issued_by, passport_num, phones, real_address, real_region_id, 
                                                              reg_authorities_id, reg_date, reg_expiration_date, 
                                                              region_id, reg_number, reg_series, reg_type_id, reg_car_date, 
                                                              short_name, subject_id, surname, usage_basis_id, usage_expire_date)
                                                    VALUES (@Id, @Guid, @BaseAddress, @BrandName, @BaseRegionId, @CarrierCategoryId, 
                                                            @CarrierStateId, @Comment, @DepartmentId, @Education, @Email, @Experience, 
                                                            @HeadName, @HeadPosition, @INN, @LegalAddress, @LegalRegionId, @Lessor,
                                                            @MiddleName, @Name, @NameLegal, @OrgStructureId, @OwnershipTypeId, 
                                                            @PassportDate, @PassportEnd, @PassportIssuedBy, @PassportNum,  
                                                            @Phones, @RealAddress, @RealRegionId, @RegAuthoritiesId, 
                                                            @RegDate, @RegExpirationDate, @RegionId, @RegNumber, @RegSeries, @RegTypeId, 
                                                            @RegСarDate, @ShortName, @SubjectId, @Surname, @UsageBasisId, @UsageExpireDate);
                                                  ";

        public readonly string CarrierUpdateQuery = @"
                                                    UPDATE registry_carriers 
                                                        SET guid = @Guid, base_address = @BaseAddress, brand_name = @BrandName, 
                                                            base_region_id = @BaseRegionId, carrier_category_id = @CarrierCategoryId, 
                                                            carrier_state_id = @CarrierStateId, comment = @Comment, department_id = @DepartmentId, 
                                                            education = @Education, email = @Email, experience = @Experience, head_name = @HeadName, 
                                                            head_position = @HeadPosition, inn = @INN, legal_address = @LegalAddress, 
                                                            legal_region_id = @LegalRegionId, lessor = @Lessor, login = @Login, middle_name = @MiddleName, 
                                                            name = @Name, name_legal = @NameLegal, org_structure_id = @OrgStructureId, ownership_type_id = @OwnershipTypeId, 
                                                            passport_date = @PassportDate, passport_end = @PassportEnd, passport_issued_by = @PassportIssuedBy, 
                                                            passport_num = @PassportNum, password = @Password, password_hashed = @PasswordHashed, phones = @Phones, 
                                                            real_address = @RealAddress, real_region_id = @RealRegionId, reg_authorities_id = @RegAuthoritiesId, 
                                                            reg_date = @RegDate, reg_expiration_date = @RegExpirationDate, region_id = @RegionId, 
                                                            reg_number = @RegNumber, reg_series = @RegSeries, reg_type_id = @RegTypeId, regсar_date = @RegСarDate, 
                                                            short_name = @ShortName, subject_id = @SubjectId, surname = @Surname, usage_basis_id = @UsageBasisId, 
                                                            usage_expire_date = @UsageExpireDate
                                                    WHERE id = @Id;
                                            ";

        public readonly string CarrierSiteUpdateQuery = @"
                                                    UPDATE registry_carriers 
                                                        SET guid = @Guid, base_address = @BaseAddress, brand_name = @BrandName, 
                                                            base_region_id = @BaseRegionId, carrier_category_id = @CarrierCategoryId, 
                                                            carrier_state_id = @CarrierStateId, comment = @Comment, department_id = @DepartmentId, 
                                                            education = @Education, email = @Email, experience = @Experience, head_name = @HeadName, 
                                                            head_position = @HeadPosition, inn = @INN, legal_address = @LegalAddress, 
                                                            legal_region_id = @LegalRegionId, lessor = @Lessor, middle_name = @MiddleName, 
                                                            name = @Name, name_legal = @NameLegal, org_structure_id = @OrgStructureId, ownership_type_id = @OwnershipTypeId, 
                                                            passport_date = @PassportDate, passport_end = @PassportEnd, passport_issued_by = @PassportIssuedBy, 
                                                            passport_num = @PassportNum, phones = @Phones, 
                                                            real_address = @RealAddress, real_region_id = @RealRegionId, reg_authorities_id = @RegAuthoritiesId, 
                                                            reg_date = @RegDate, reg_expiration_date = @RegExpirationDate, region_id = @RegionId, 
                                                            reg_number = @RegNumber, reg_series = @RegSeries, reg_type_id = @RegTypeId, reg_car_date = @RegСarDate, 
                                                            short_name = @ShortName, subject_id = @SubjectId, surname = @Surname, usage_basis_id = @UsageBasisId, 
                                                            usage_expire_date = @UsageExpireDate
                                                    WHERE id = @Id;
                                            ";

        public readonly string CarrierDeleteQuery =
            @"UPDATE registry_carriers SET is_deleted = CURRENT_TIMESTAMP WHERE id = @Id;";

        public readonly string CarrierSiteDeleteQuery =
            @"DELETE FROM registry_carriers SET is_deleted = CURRENT_TIMESTAMP WHERE id = @Id;";
    }
}
