﻿namespace AisReference.Queries.Registry
{
    public class RegistryPermitFormQueries
    {
        public readonly string RegistryPermitFormEleedSelectQuery =
        "SELECT id, uid as guid, F130405 as ArchiveStatusId, F130404 as CarrierId, " +
        "F130401 as CountryId, F310964 as DepartmentId, F130399 as ExpiryDate, F130397 as IssueDate, " +
        "F130394 as Number, F392506 as NumberInt, F130396 as ReceiptDate, F130403 as RestrictionId, " +
        "F130400 as ReturnDate, F130398 as ReturnDeadline, F130395 as Series, F130402 as FormTypeId, F170964 as VehicleId " +
        "FROM A1373";

        public readonly string RegistryPermitFormEleedToDefaultSelectQuery =
            "SELECT id, guid, archive_status_id as ArchiveStatusId, carrier_id as CarrierId, " +
            "country_id as CountryId, department_id as DepartmentId, expiry_date as ExpiryDate, " +
            "issue_date as IssueDate, number as Number, number_int as NumberInt, receipt_date as ReceiptDate, " +
            "restriction_id as RestrictionId, return_date as ReturnDate, return_deadline as ReturnDeadline, " +
            "series as Series, form_type_id as FormTypeId, vehicle_id as VehicleId" +
            " FROM registry_permit_forms";

        public readonly string RegistryPermitFormDefaultSelectQuery =
            "SELECT id, guid, carrier_id as CarrierId, " +
            "deleted_date as DeletedDate, updated_date as UpdatedDate, created_date as CreatedDate, permit_form_received_guid as PermitFormReceivedGuid, " +
            "invoice_date as InvoiceDate, transfered_date as TransferedDate, status_id as StatusId, " +
            "country_id as CountryId, department_id as DepartmentId, expiry_date as ExpiryDate, " +
            "issue_date as IssueDate, number as Number, number_int as NumberInt, receipt_date as ReceiptDate, " +
            "restriction_id as RestrictionId, return_date as ReturnDate, return_deadline as ReturnDeadline, " +
            "series as Series, form_type_id as FormTypeId, vehicle_id as VehicleId" +
            " FROM registry_permit_forms" +
            " WHERE restriction_id IS NOT NULL" + 
            "";

        public readonly string RegistryPermitFormSiteSelectQuery =
            "SELECT id, guid, carrier_id as CarrierId, " +
            "deleted_date as DeletedDate, updated_date as UpdatedDate, created_date as CreatedDate, permit_form_received_guid as PermitFormReceivedGuid, " +
            "invoice_date as InvoiceDate, transferred_date as TransferedDate, status_id as StatusId, " +
            "country_id as CountryId, department_id as DepartmentId, expiry_date as ExpiryDate, " +
            "issue_date as IssueDate, number as Number, number_int as NumberInt, receipt_date as ReceiptDate, " +
            "restriction_id as RestrictionId, return_date as ReturnDate, return_deadline as ReturnDeadline, " +
            "series as Series, form_type_id as FormTypeId, vehicle_id as VehicleId" +
            " FROM registry_permit_forms";


        public readonly string RegistryPermitFormDefaultCreateQuery = @"
                                                    INSERT INTO registry_permit_forms(id, guid, archive_status_id, carrier_id, country_id, department_id, expiry_date, issue_date, number, number_int, receipt_date, restriction_id, return_date, return_deadline, series, form_type_id, vehicle_id, created_date, updated_date)
                                                    VALUES (@Id, @Guid, @ArchiveStatusId, @CarrierId, @CountryId, @DepartmentId, @ExpiryDate, @IssueDate, @Number, @NumberInt, @ReceiptDate, @RestrictionId, @ReturnDate, @ReturnDeadline, @Series, @FormTypeId, @VehicleId, CURRENT_DATE, CURRENT_DATE
                                                     );";

        public readonly string RegistryPermitFormSiteCreateQuery = @"
                                                    INSERT INTO registry_permit_forms(
                                                            id, 
                                                            guid,
                                                            deleted_date,
                                                            permit_form_received_guid,
                                                            carrier_id, 
                                                            country_id, 
                                                            invoice_date, 
                                                            transferred_date, 
                                                            status_id, 
                                                            department_id, 
                                                            expiry_date, 
                                                            issue_date, 
                                                            number, 
                                                            number_int, 
                                                            receipt_date, 
                                                            restriction_id, 
                                                            return_date, 
                                                            return_deadline, 
                                                            series, 
                                                            form_type_id, 
                                                            vehicle_id, 
                                                            created_date, 
                                                            updated_date
                                                        )
                                                    VALUES (
                                                            @Id, 
                                                            @Guid, 
                                                            @DeletedDate,
                                                            @PermitFormReceivedGuid, 
                                                            @CarrierId, 
                                                            @CountryId, 
                                                            @InvoiceDate, 
                                                            @TransferedDate, 
                                                            @StatusId, 
                                                            @DepartmentId, 
                                                            @ExpiryDate, 
                                                            @IssueDate, 
                                                            @Number, 
                                                            @NumberInt, 
                                                            @ReceiptDate, 
                                                            @RestrictionId, 
                                                            @ReturnDate, 
                                                            @ReturnDeadline, 
                                                            @Series, 
                                                            @FormTypeId, 
                                                            @VehicleId, 
                                                            CURRENT_DATE, 
                                                            CURRENT_DATE
                                                     );";

        public readonly string RegistryPermitFormUpdateQuery = @"
                                                    UPDATE registry_permit_forms 
                                                        SET 
                                                            guid = @Guid, 
                                                            archive_status_id = @ArchiveStatusId, 
                                                            carrier_id = @CarrierId, 
                                                            country_id = @CountryId, 
                                                            department_id = @DepartmentId, 
                                                            expiry_date = @ExpiryDate, 
                                                            issue_date = @IssueDate, 
                                                            number = @Number, 
                                                            number_int = @NumberInt, 
                                                            receipt_date = @ReceiptDate, 
                                                            restriction_id = @RestrictionId, 
                                                            return_date = @ReturnDate, 
                                                            return_deadline = @ReturnDeadline, 
                                                            series = @Series, 
                                                            form_type_id = @FormTypeId, 
                                                            vehicle_id = @VehicleId
                                                    WHERE id = @Id;";

        public readonly string RegistryPermitFormSiteUpdateQuery = @"
                                                    UPDATE registry_permit_forms 
                                                        SET
                                                            guid = @Guid, 
                                                            deleted_date = @DeletedDate,
                                                            updated_date = @UpdatedDate,
                                                            created_date = @CreatedDate,
                                                            permit_form_received_guid = @PermitFormReceivedGuid,  
                                                            carrier_id = @CarrierId, 
                                                            country_id = @CountryId, 
                                                            invoice_date = @InvoiceDate, 
                                                            transferred_date = @TransferedDate, 
                                                            status_id = @StatusId,
                                                            department_id = @DepartmentId, 
                                                            expiry_date = @ExpiryDate, 
                                                            issue_date = @IssueDate, 
                                                            number = @Number, 
                                                            number_int = @NumberInt, 
                                                            receipt_date = @ReceiptDate, 
                                                            restriction_id = @RestrictionId, 
                                                            return_date = @ReturnDate, 
                                                            return_deadline = @ReturnDeadline, 
                                                            series = @Series, 
                                                            form_type_id = @FormTypeId, 
                                                            vehicle_id = @VehicleId
                                                    WHERE id = @Id;";

        public readonly string RegistryPermitFormDeleteQuery =
            @"DELETE FROM registry_permit_forms WHERE id = @Id;";

        public readonly string RegistryPermitFormSiteDeleteQuery =
            @"DELETE FROM registry_permit_forms WHERE id = @Id;";
    }
}
