﻿namespace AisReference.Queries.Registry
{
    public class LicenseQueries
    {

        //RegistryLicense
        public readonly string RegisterLicenseEleedSelectQuery =
            "SELECT id, uid as guid, F81199 as License, F81587 as LicJustification, F81203 as LicIssuedDate, F81205 as LicValidityEnd, " +
            "F81204 as LicValidityStart, F81585 as SuspendDateEnd, F372430 as NumSpec, F331412 as OldLicense, F81202 as CoveredTerId, " +
            "F81200 as DepartmentId, F81586 as DocReasonId, F81242 as CarrierId, F81786 as ParentId, F81208 as LicenseStateId, " +
            "F81201 as LicenseTypeId " +
            "FROM A975";

        public readonly string RegisterLicenseDefaultSelectQuery =
            "SELECT id, guid, license as License, lic_justification as LicJustification, lic_issued_date as LicIssuedDate, " +
            "lic_validity_end as LicValidityEnd, lic_validity_start as LicValidityStart, suspend_date_end as SuspendDateEnd, " +
            "num_spec as NumSpec, old_license as OldLicense, covered_ter_id as CoveredTerId, department_id as DepartmentId, " +
            "doc_reason_id as DocReasonId, carrier_id as CarrierId, parent_id as ParentId, license_state_id as LicenseStateId, " +
            "license_type_id as LicenseTypeId " +
            "FROM registry_licenses";

        public readonly string RegisterLicenseEkmSelectQuery =
            "SELECT id, guid, license as License, lic_justification as LicJustification, lic_issued_date as LicIssuedDate, " +
            "lic_validity_end as LicValidityEnd, lic_validity_start as LicValidityStart, suspend_date_end as SuspendDateEnd, " +
            "num_spec as NumSpec, old_license as OldLicense, covered_ter_id as CoveredTerId, department_id as DepartmentId, " +
            "doc_reason_id as DocReasonId, carrier_id as CarrierId, parent_id as ParentId, license_state_id as LicenseStateId, " +
            "license_type_id as LicenseTypeId " +
            "FROM registry_licenses";

        public readonly string RegisterLicenseSiteSelectQuery =
            "SELECT id, license as License, lic_justification as LicJustification, lic_issued_date as LicIssuedDate, " +
            "lic_validity_end as LicValidityEnd, lic_validity_start as LicValidityStart, suspend_date_end as SuspendDateEnd, " +
            "num_spec as NumSpec, old_license as OldLicense, covered_ter_id_id as CoveredTerId, department_id_id as DepartmentId, " +
            "carrier_id as CarrierId, license_state_id_id as LicenseStateId, " +
            "license_type_id_id as LicenseTypeId " +
            "FROM registry_licenses";

        public readonly string RegisterLicenseDefaultToSiteSelectQuery =
            "SELECT id, license as License, lic_justification as LicJustification, lic_issued_date as LicIssuedDate, " +
            "lic_validity_end as LicValidityEnd, lic_validity_start as LicValidityStart, suspend_date_end as SuspendDateEnd, " +
            "num_spec as NumSpec, old_license as OldLicense, covered_ter_id as CoveredTerId, department_id as DepartmentId, " +
            "carrier_id as CarrierId, license_state_id as LicenseStateId, " +
            "license_type_id as LicenseTypeId " +
            "FROM registry_licenses";

        public readonly string RegisterLicenseCreateQuery = @"
            INSERT INTO registry_licenses(id, guid, license, lic_justification, lic_issued_date, lic_validity_end, lic_validity_start, suspend_date_end, num_spec, old_license, covered_ter_id, department_id, doc_reason_id, carrier_id, parent_id, license_state_id, license_type_id, is_deleted )
            VALUES (@Id, @Guid, @License, @LicJustification, @LicIssuedDate, @LicValidityEnd, @LicValidityStart, @SuspendDateEnd, @NumSpec, @OldLicense, @CoveredTerId, @DepartmentId, @DocReasonId, @CarrierId, @ParentId, @LicenseStateId, @LicenseTypeId, @IsDeleted);
                                                    ";

        public readonly string RegisterLicenseCreateOnSiteQuery = @"
            INSERT INTO registry_licenses(id, license, lic_justification, lic_issued_date, lic_validity_end, lic_validity_start, suspend_date_end, num_spec, old_license, covered_ter_id_id, department_id_id, carrier_id, license_state_id_id, license_type_id_id )
            VALUES (@Id, @License, @LicJustification, @LicIssuedDate, @LicValidityEnd, @LicValidityStart, @SuspendDateEnd, @NumSpec, @OldLicense, @CoveredTerId, @DepartmentId, @CarrierId, @LicenseStateId, @LicenseTypeId);
                                                    ";

        public readonly string RegisterLicenseUpdateQuery = @"
                                                    UPDATE registry_licenses 
                                                        SET guid = @Guid,
                                                            license = @License,
                                                            lic_justification = @LicJustification,
                                                            lic_issued_date = @LicIssuedDate,
                                                            lic_validity_end = @LicValidityEnd,
                                                            lic_validity_start = @LicValidityStart,
                                                            suspend_date_end = @SuspendDateEnd,
                                                            num_spec = @NumSpec,
                                                            old_license = @OldLicense,
                                                            covered_ter_id = @CoveredTerId,
                                                            department_id_id = @DepartmentId,
                                                            doc_reason_id = @DocReasonId,
                                                            carrier_id = @CarrierId,
                                                            parent_id = @ParentId,
                                                            license_state_id = @LicenseStateId,
                                                            license_type_id = @LicenseTypeId,
                                                            is_deleted = @IsDeleted

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegisterLicenseUpdateOnSiteQuery = @"
                                                    UPDATE registry_licenses 
                                                        SET license = @License,
                                                            lic_justification = @LicJustification,
                                                            lic_issued_date = @LicIssuedDate,
                                                            lic_validity_end = @LicValidityEnd,
                                                            lic_validity_start = @LicValidityStart,
                                                            suspend_date_end = @SuspendDateEnd,
                                                            num_spec = @NumSpec,
                                                            old_license = @OldLicense,
                                                            covered_ter_id_id = @CoveredTerId,
                                                            department_id_id = @DepartmentId,
                                                            carrier_id = @CarrierId,
                                                            license_state_id_id = @LicenseStateId,
                                                            license_type_id_id = @LicenseTypeId

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegisterLicenseDeleteQuery =
            @"UPDATE registry_licenses SET is_deleted = true WHERE id = @Id;";

        public readonly string RegisterLicenseDeleteOnSiteQuery =
            @"DELETE FROM registry_licenses WHERE id = @Id;";
    }
}