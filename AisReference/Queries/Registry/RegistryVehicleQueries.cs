﻿namespace AisReference.Queries.Registry
{
    public class RegistryVehicleQueries
    {
        public readonly string RegistryVehicleEleedSelectQuery =
        "SELECT id, uid as Guid, state, F80292 as VehicleBrandId, F311350 as BrandSpelling, F311320 as IsTrailer, F80293 as Model, F392564 as Name, F81194 as RegisteredCountryId,  F81421 as Scan, F80291 as StateNumber, F81540 as VehicleKindId, F81193 as VIN, F392563 as WaterTransport, F80295 as YearRelease FROM A851";

        public readonly string RegistryVehicleSelectQuery =
            "SELECT id, guid, state, vehicle_brand_id as VehicleBrandId, brand_spelling as BrandSpelling, is_trailer as IsTrailer, model as Model, name as Name, registered_country_id as RegisteredCountryId, scan as Scan, state_number as StateNumber, vehicle_kind_id as VehicleKindId, vin as VIN, water_transport as WaterTransport, year_release as YearRelease FROM registry_vehicles";

        public readonly string RegistryVehicleSiteSelectQuery =
            "SELECT id, guid, state, brand_id as VehicleBrandId, CAST(brand_spelling AS UUID) as BrandSpelling, is_trailer as IsTrailer, model as Model, name as Name, reg_country_id as RegisteredCountryId, scan as Scan, state_number as StateNumber, vehicle_kind_id as VehicleKindId, vin as VIN, water_transport as WaterTransport, year_release as YearRelease FROM registry_vehicles";

        public readonly string RegistryVehicleDefaultToSiteSelectQuery =
           "SELECT id, guid, state, vehicle_brand_id as VehicleBrandId, brand_spelling::text as BrandSpelling, is_trailer as IsTrailer, model as Model, name as Name, registered_country_id as RegisteredCountryId, scan as Scan, state_number as StateNumber, vehicle_kind_id as VehicleKindId, vin as VIN, water_transport as WaterTransport, year_release as YearRelease FROM registry_vehicles";

        
        public readonly string RegistryVehicleCreateQuery = @"
                                                    INSERT INTO registry_vehicles(id, guid, state, vehicle_brand_id, brand_spelling, is_trailer, model, name, registered_country_id, scan, state_number, vehicle_kind_id, vin, water_transport, year_release)
                                                    VALUES (@Id, @Guid, @State, @VehicleBrandId, @BrandSpelling, @IsTrailer, @Model, @Name, @RegisteredCountryId, @Scan, @StateNumber, @VehicleKindId, @VIN, @WaterTransport, @YearRelease);
                                                  ";

        public readonly string RegistryVehicleSiteCreateQuery = @"
                                                    INSERT INTO registry_vehicles(id, guid, state, brand_id, brand_spelling, is_trailer, model, name, reg_country_id, scan, state_number, vehicle_kind_id, vin, water_transport, year_release)
                                                    VALUES (@Id, @Guid, @State, @VehicleBrandId, @BrandSpelling, @IsTrailer, @Model, @Name, @RegisteredCountryId, @Scan, @StateNumber, @VehicleKindId, @VIN, @WaterTransport, @YearRelease);
                                                  ";

        public readonly string RegistryVehicleUpdateQuery =
            @"
                                                    UPDATE registry_vehicles 
                                                        SET guid = @Guid, state = @State, vehicle_brand_id = @VehicleBrandId, brand_spelling = @BrandSpelling, is_trailer = @IsTrailer, model = @Model, name = @Name, registered_country_id = @RegisteredCountryId, scan = @Scan, state_number = @StateNumber, vehicle_kind_id = @VehicleKindId, vin = @VIN, water_transport = @WaterTransport, year_release = @YearRelease
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegistryVehicleSiteUpdateQuery =
            @"
                                                    UPDATE registry_vehicles 
                                                        SET guid = @Guid, state = @State, brand_id = @VehicleBrandId, brand_spelling = @BrandSpelling, is_trailer = @IsTrailer, model = @Model, name = @Name, reg_country_id = @RegisteredCountryId, scan = @Scan, state_number = @StateNumber, vehicle_kind_id = @VehicleKindId, vin = @VIN, water_transport = @WaterTransport, year_release = @YearRelease
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegistryVehicleDeleteQuery =
            @"UPDATE registry_vehicles SET is_deleted = CURRENT_TIMESTAMP WHERE id = @Id;";
        
        public readonly string RegistryVehicleSiteDeleteQuery =
            @"DELETE FROM registry_vehicles WHERE id = @Id;";


    }
}
