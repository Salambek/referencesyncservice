﻿namespace AisReference.Queries.Registry
{
    public class RegistryCarrierVehicleQueries
    {
        public readonly string RegistryCarrierVehicleEleedSelectQuery =
        "SELECT id, uid as Guid, ID_PARENT as ParentId, F522587 as ApplicationUID, F81374 as UsageBasisId, F81373 as VehicleId FROM A997";

        public readonly string RegistryCarrierVehicleSelectQuery =
            "SELECT id, guid, parent_id as ParentId, application_uid as ApplicationUID, usage_basis_id as UsageBasisId, vehicle_id as VehicleId FROM registry_carrier_vehicles";

        public readonly string RegistryCarrierVehicleSiteSelectQuery =
            "SELECT id, guid, carrier_id as ParentId, application_uid as ApplicationUID, usage_basis_id as UsageBasisId, vehicle_id as VehicleId FROM registry_carrier_vehicles";


        public readonly string RegistryCarrierVehicleCreateQuery = @"
                                                    INSERT INTO registry_carrier_vehicles(id, guid, parent_id, application_uid, usage_basis_id, vehicle_id)
                                                    VALUES (@Id, @Guid, @ParentId, @ApplicationUID, @UsageBasisId, @VehicleId);
                                                  ";

        public readonly string RegistryCarrierVehicleSiteCreateQuery = @"
                                                    INSERT INTO registry_carrier_vehicles(id, guid, carrier_id, application_uid, usage_basis_id, vehicle_id)
                                                    VALUES (@Id, @Guid, @ParentId, @ApplicationUID, @UsageBasisId, @VehicleId);
                                                  ";

        public readonly string RegistryCarrierVehicleUpdateQuery =
            @"
                                                    UPDATE registry_carrier_vehicles 
                                                        SET guid = @Guid, parent_id = @ParentId, application_uid = @ApplicationUID, usage_basis_id = @UsageBasisId, vehicle_id = @VehicleId
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegistryCarrierVehicleSiteUpdateQuery =
            @"
                                                    UPDATE registry_carrier_vehicles 
                                                        SET guid = @Guid, carrier_id = @ParentId, application_uid = @ApplicationUID, usage_basis_id = @UsageBasisId, vehicle_id = @VehicleId
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegistryCarrierVehicleDeleteQuery =
            @"DELETE FROM registry_carrier_vehicles WHERE id = @Id;";
    }
}
