﻿namespace AisReference.Queries.Registry
{
    public class RegistryCarrierSpecialistQueries
    {
        public readonly string RegistryCarrierSpecialistEleedSelectQuery =
        "SELECT id, uid as Guid, ID_PARENT as ParentId, F522585 as ApplicationUID, F81371 as DrivingExperience, F81370 as PositionId, F81372 as ResponsibleForLicense, F81369 as SpecialistId FROM A996";

        public readonly string RegistryCarrierSpecialistSelectQuery =
            "SELECT id, guid, parent_id as ParentId, application_uid as ApplicationUID, driving_experience as DrivingExperience, position_id as PositionId, responsible_for_license as ResponsibleForLicense, specialist_id as SpecialistId  FROM registry_carrier_specialists";

        public readonly string RegistryCarrierSpecialistSiteSelectQuery =
            "SELECT id, guid, carrier_id as ParentId, application_uid as ApplicationUID, driving_experience as DrivingExperience, position_id as PositionId, responsible_for_license as ResponsibleForLicense, specialist_id as SpecialistId  FROM registry_carrier_specialists";

        public readonly string RegistryCarrierSpecialistCreateQuery = @"
                                                    INSERT INTO registry_carrier_specialists(id, guid, parent_id, application_uid, driving_experience, position_id, responsible_for_license, specialist_id)
                                                    VALUES (@Id, @Guid, @ParentId, @ApplicationUID, @DrivingExperience, @PositionId, @ResponsibleForLicense, @SpecialistId);
                                                  ";

        public readonly string RegistryCarrierSpecialistSiteCreateQuery = @"
                                                    INSERT INTO registry_carrier_specialists(id, guid, carrier_id, application_uid, driving_experience, position_id, responsible_for_license, specialist_id)
                                                    VALUES (@Id, @Guid, @ParentId, @ApplicationUID, @DrivingExperience, @PositionId, @ResponsibleForLicense, @SpecialistId);
                                                  ";

        public readonly string RegistryCarrierSpecialistUpdateQuery = @"
                                                    UPDATE registry_carrier_specialists 
                                                        SET guid = @Guid, parent_id = @ParentId, application_uid = @ApplicationUID, driving_experience = @DrivingExperience, position_id = @PositionId, responsible_for_license = @ResponsibleForLicense, specialist_id = @SpecialistId
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegistryCarrierSpecialistSiteUpdateQuery = @"
                                                    UPDATE registry_carrier_specialists 
                                                        SET guid = @Guid, carrier_id = @ParentId, application_uid = @ApplicationUID, driving_experience = @DrivingExperience, position_id = @PositionId, responsible_for_license = @ResponsibleForLicense, specialist_id = @SpecialistId
                                                    WHERE id = @Id;
                                                  ";

        public readonly string RegistryCarrierSpecialistDeleteQuery =
           @"DELETE FROM registry_carrier_specialists  WHERE id = @Id;";
    }
}
