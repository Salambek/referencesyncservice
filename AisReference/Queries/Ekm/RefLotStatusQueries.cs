﻿namespace AisReference.Queries.Ekm
{
    public class RefLotStatusQueries
    {
        public readonly string RefLotStatusSelectQuery =
            "SELECT id, name as Name, name_ky as NameKy, name_ru as NameRu, " +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_lot_status";


        public readonly string RefLotStatusCreateQuery = @"
                                                    INSERT INTO ref_lot_status(
                                                        id, 
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";

        public readonly string RefLotStatusUpdateQuery = @"
                                                    UPDATE ref_lot_status 
                                                        SET name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefLotStatusDeleteQuery =
            @"DELETE FROM ref_lot_status WHERE id = @Id;";
    }
}
