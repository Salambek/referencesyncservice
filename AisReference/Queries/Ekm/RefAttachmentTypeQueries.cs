﻿namespace AisReference.Queries
{
    public class RefAttachmentTypeQueries
    {

        public readonly string RefAttachmentTypeSelectQuery =
            "SELECT id, is_individual AS IsIndividual, is_legal as IsLegal, name as Name, name_ky as NameKy, name_ru as NameRu," +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_attachment_types";

        public readonly string RefAttachmentTypeEkmSelectQuery =
            "SELECT id, is_individual AS IsIndividual, is_legal as IsLegal, name as Name, name_ky as NameKy, name_ru as NameRu," +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_attachment_type";

        public readonly string RefAttachmentTypeCreateQuery = @"
                                                    INSERT INTO ref_attachment_types(
                                                        id, 
                                                        is_individual,
                                                        is_legal,
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @IsIndividual,
                                                        @IsLegal,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";

        public readonly string RefAttachmentTypeUpdateQuery = @"
                                                    UPDATE ref_attachment_types 
                                                        SET is_individual = @IsIndividual,
                                                            is_legal = @IsLegal,
                                                            name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefAttachmentTypeDeleteQuery =
            @"DELETE FROM ref_attachment_types WHERE id = @Id;";
    }
}