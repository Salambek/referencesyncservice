﻿namespace AisReference.Queries.Ekm
{
    public class RefFuelTypeQueries
    {
        public readonly string RefFuelTypeSelectQuery =
            "SELECT id, name as Name, name_ky as NameKy, name_ru as NameRu, " +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_fuel_types";


        public readonly string RefFuelTypeCreateQuery = @"
                                                    INSERT INTO ref_fuel_types(
                                                        id, 
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";


        public readonly string RefFuelTypeUpdateQuery = @"
                                                    UPDATE ref_fuel_types 
                                                        SET name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";


        public readonly string RefFuelTypeDeleteQuery =
            @"DELETE FROM ref_fuel_types WHERE id = @Id;";

    }
}
