﻿namespace AisReference.Queries.Ekm
{
    public class RefApplicationStatusQueries
    {
        public readonly string RefApplicationStatusSelectQuery =
            "SELECT id, name as Name, name_ky as NameKy, name_ru as NameRu, " +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_application_status";

        public readonly string RefApplicationStatusSiteSelectQuery =
            "SELECT id, name as Name, name_ky as NameKy, name_ru as NameRu," +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_application_statuses";

        public readonly string RefApplicationStatusCreateQuery = @"
                                                    INSERT INTO ref_application_status(
                                                        id, 
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";

        public readonly string RefApplicationStatusSiteCreateQuery = @"
                                                    INSERT INTO ref_application_statuses(
                                                        id, 
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";

        public readonly string RefApplicationStatusUpdateQuery = @"
                                                    UPDATE ref_application_status 
                                                        SET name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefApplicationStatusSiteUpdateQuery = @"
                                                    UPDATE ref_application_statuses 
                                                        SET name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefApplicationStatusDeleteQuery =
            @"DELETE FROM ref_application_status WHERE id = @Id;";

        public readonly string RefApplicationStatusSiteDeleteQuery =
            @"DELETE FROM ref_application_statuses WHERE id = @Id;";
    }
}
