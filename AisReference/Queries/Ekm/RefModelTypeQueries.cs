﻿namespace AisReference.Queries
{
    public class RefModelTypeQueries
    {

        public readonly string RefModelTypeSelectQuery =
            "SELECT id, model_name AS ModelName, front_api_url as FrontApiUrl, update_with_front as UpdateWithFront, name as Name, name_ky as NameKy, name_ru as NameRu," +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_model_types";

        public readonly string RefModelTypeSiteSelectQuery =
            "SELECT id, name as Name, name_ky as NameKy, name_ru as NameRu," +
            "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
            "FROM ref_model_types";

        public readonly string RefModelTypeCreateQuery = @"
                                                    INSERT INTO ref_model_types(
                                                        id, 
                                                        model_name,
                                                        front_api_url,
                                                        update_with_front,
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @ModelName,
                                                        @FrontApiUrl,
                                                        @UpdateWithFront,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";

        public readonly string RefModelTypeSiteCreateQuery = @"
                                                    INSERT INTO ref_model_types(
                                                        id, 
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";

        public readonly string RefModelTypeUpdateQuery = @"
                                                    UPDATE ref_model_types 
                                                        SET model_name = @ModelName,
                                                            front_api_url = @FrontApiUrl,
                                                            update_with_front = @UpdateWithFront,
                                                            name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefModelTypeSiteUpdateQuery = @"
                                                    UPDATE ref_model_types 
                                                        SET name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefModelTypeDeleteQuery =
            @"DELETE FROM ref_model_types WHERE id = @Id;";
    }
}