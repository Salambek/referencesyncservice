﻿namespace AisReference.Queries.Ekm
{
    public class RefTechnicalBaseCategoryQueries
    {
        public readonly string RefTechnicalBaseCategorySelectQuery =
           "SELECT id, parent_category_id AS ParentCategoryId, is_rated as IsRated, name as Name, name_ky as NameKy, name_ru as NameRu," +
           "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
           "FROM ref_technical_base_categories";

        public readonly string RefTechnicalBaseCategorySiteSelectQuery =
           "SELECT id, parent_category_id AS ParentCategoryId,  name as Name, name_ky as NameKy, name_ru as NameRu," +
           "created_date as CreatedDate, updated_date as UpdatedDate, is_active as IsActive, deleted_date as DeletedDate " +
           "FROM ref_technical_base_categories";

        public readonly string RefTechnicalBaseCategoryCreateQuery = @"
                                                    INSERT INTO ref_technical_base_categories(
                                                        id, 
                                                        parent_category_id,
                                                        is_rated,
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @ParentCategoryId,
                                                        @IsRated,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";

        public readonly string RefTechnicalBaseCategorySiteCreateQuery = @"
                                                    INSERT INTO ref_technical_base_categories(
                                                        id, 
                                                        parent_category_id,
                                                        name,
                                                        name_ky,
                                                        name_ru,
                                                        created_date,
                                                        updated_date,
                                                        is_active,
                                                        deleted_date
                                                    )
                                                    VALUES (
                                                        @Id,
                                                        @ParentCategoryId,
                                                        @Name,
                                                        @NameKy,
                                                        @NameRu,
                                                        @CreatedDate,
                                                        @UpdatedDate,
                                                        @IsActive,
                                                        @DeletedDate
                                                    )";

        public readonly string RefTechnicalBaseCategoryUpdateQuery = @"
                                                    UPDATE ref_technical_base_categories 
                                                        SET parent_category_id = @ParentCategoryId,
                                                            is_rated = @IsRated,
                                                            name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefTechnicalBaseCategorySiteUpdateQuery = @"
                                                    UPDATE ref_technical_base_categories 
                                                        SET parent_category_id = @ParentCategoryId,
                                                            name = @Name,
                                                            name_ky = @NameKy,
                                                            name_ru = @NameRu,
                                                            created_date = @CreatedDate,
                                                            updated_date = @UpdatedDate,
                                                            is_active = @IsActive,
                                                            deleted_date = @DeletedDate

                                                    WHERE id = @Id;
                                                  ";

        public readonly string RefTechnicalBaseCategoryDeleteQuery =
            @"DELETE FROM ref_technical_base_categories WHERE id = @Id;";
    }
}
